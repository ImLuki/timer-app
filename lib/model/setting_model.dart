import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timer_app/model/sounds.dart';
import 'package:timer_app/resources/constants.dart';
import 'package:timer_app/theme/app_colors.dart';
import 'package:vibration/vibration.dart';

class SettingModel {
  static const int STANDARD_PREP_TIME = 10;

  ///
  /// Standard Colors
  ///
  late Color _standardTrainingColor;
  late Color _standardRestColor;
  late Color _standardPreparationColor;

  ///
  /// Miscellaneous
  ///
  int _standardPrepTime = 1;
  bool _skipLastRest = false;
  bool _vibrate = false;
  bool _canVibrate = false;
  bool _showLeftTime = false;

  ///
  /// Sounds
  ///
  bool _soundEnabled = true;
  int _amountCountingSounds = 3;
  Sound _trainingSound = Sound.None;
  Sound _restSound = Sound.None;
  Sound _finishSound = Sound.None;
  ShortSound _countDownSound = ShortSound.None;
  Sound _halfTimeSound = Sound.None;
  bool _halftimeSoundActivated = false;

  Future<void> init() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    this._standardTrainingColor = Color(
      pref.getInt(Constants.STANDARD_TRAINING_COLOR_KEY) ?? AppColors.secondary.value,
    );

    // color
    this._standardRestColor = Color(pref.getInt(Constants.STANDARD_REST_COLOR_KEY) ?? AppColors.restColor.value);
    this._standardPreparationColor = Color(pref.getInt(Constants.STANDARD_PREP_COLOR_KEY) ?? AppColors.prepColor.value);

    // time
    this._standardPrepTime = pref.getInt(Constants.STANDARD_PREP_DURATION_KEY) ?? STANDARD_PREP_TIME;
    this._showLeftTime = pref.getBool(Constants.SHOW_LEFT_TIME_KEY) ?? false;

    // diverse
    this._skipLastRest = pref.getBool(Constants.SKIP_LAST_REST_KEY) ?? true;

    // vibration
    _canVibrate = (await Vibration.hasVibrator() ?? false);
    _canVibrate = _canVibrate && (await Vibration.hasCustomVibrationsSupport() ?? false);

    this._vibrate = _canVibrate ? pref.getBool(Constants.VIBRATE_KEY) ?? true : false;

    // sound
    this._soundEnabled = pref.getBool(Constants.ENABLE_SOUND_KEY) ?? true;
    this._amountCountingSounds = pref.getInt(Constants.COUNTING_SOUND_KEY) ?? 3;
    this._halftimeSoundActivated = pref.getBool(Constants.HALF_TIME_SOUND_ACTIVATED_KEY) ?? false;

    Sound standardSound = SoundHandler.getStandard();
    String standardSoundString = standardSound.name;

    this._trainingSound = Sound.values.byName(
      pref.getString(Constants.STANDARD_TRAINING_SOUND_KEY) ?? standardSoundString,
    );
    this._restSound = Sound.values.byName(
      pref.getString(Constants.STANDARD_REST_SOUND_KEY) ?? standardSoundString,
    );

    this._countDownSound = ShortSound.values.byName(
      pref.getString(Constants.STANDARD_COUNTDOWN_SOUND_KEY) ?? SoundHandler.getStandardShortSound().name,
    );

    Sound standardFinalSound = SoundHandler.getStandardFinalSound();
    String standardFinalSoundString = standardFinalSound.name;
    this._finishSound = Sound.values.byName(
      pref.getString(Constants.STANDARD_FINISH_SOUND_KEY) ?? standardFinalSoundString,
    );

    this._halfTimeSound = Sound.values.byName(
      pref.getString(Constants.STANDARD_HALF_TIME_SOUND_KEY) ?? SoundHandler.getStandard().name,
    );
  }

  Color get standardPreparationColor => _standardPreparationColor;

  Color get standardRestColor => _standardRestColor;

  Color get standardTrainingColor => _standardTrainingColor;

  int get standardPrepTime => _standardPrepTime;

  bool get skipLastRest => _skipLastRest;

  bool get vibrate => _vibrate;

  bool get soundEnabled => _soundEnabled;

  int get amountCountingSounds => _amountCountingSounds;

  Sound get finishSound => _finishSound;

  Sound get restSound => _restSound;

  Sound get trainingSound => _trainingSound;

  ShortSound get countDownSound => _countDownSound;

  bool get showLeftTime => _showLeftTime;

  Sound get halfTimeSound => _halfTimeSound;

  bool get halftimeSoundActivated => _halftimeSoundActivated;

  set standardPreparationColor(Color color) {
    _standardPreparationColor = color;
    SharedPreferences.getInstance().then((pref) => pref.setInt(Constants.STANDARD_PREP_COLOR_KEY, color.value));
  }

  set standardRestColor(Color color) {
    _standardRestColor = color;
    SharedPreferences.getInstance().then((pref) => pref.setInt(Constants.STANDARD_REST_COLOR_KEY, color.value));
  }

  set standardTrainingColor(Color color) {
    _standardTrainingColor = color;
    SharedPreferences.getInstance().then((pref) => pref.setInt(Constants.STANDARD_TRAINING_COLOR_KEY, color.value));
  }

  set standardPrepTime(int value) {
    _standardPrepTime = value;
    SharedPreferences.getInstance()
        .then((pref) => pref.setInt(Constants.STANDARD_PREP_DURATION_KEY, this._standardPrepTime));
  }

  set skipLastRest(bool value) {
    _skipLastRest = value;
    SharedPreferences.getInstance().then((pref) => pref.setBool(Constants.SKIP_LAST_REST_KEY, this._skipLastRest));
  }

  bool setVibrate(bool value) {
    if (!_canVibrate) return false;
    _vibrate = value;
    SharedPreferences.getInstance().then((pref) => pref.setBool(Constants.VIBRATE_KEY, this._vibrate));
    return true;
  }

  set soundEnabled(bool value) {
    _soundEnabled = value;
    SharedPreferences.getInstance().then((pref) => pref.setBool(Constants.ENABLE_SOUND_KEY, this._soundEnabled));
  }

  set amountCountingSounds(int value) {
    _amountCountingSounds = value;
    SharedPreferences.getInstance().then((pref) => pref.setInt(Constants.COUNTING_SOUND_KEY, _amountCountingSounds));
  }

  set halftimeSoundActivated(bool value) {
    _halftimeSoundActivated = value;
    SharedPreferences.getInstance().then(
      (pref) => pref.setBool(Constants.HALF_TIME_SOUND_ACTIVATED_KEY, this._halftimeSoundActivated),
    );
  }

  set showLeftTime(bool value) {
    _showLeftTime = value;
    SharedPreferences.getInstance().then(
      (pref) => pref.setBool(Constants.SHOW_LEFT_TIME_KEY, this._showLeftTime),
    );
  }

  void setFinishSound(String value) {
    _finishSound = Sound.values.byName(value);
    SharedPreferences.getInstance()
        .then((pref) => pref.setString(Constants.STANDARD_FINISH_SOUND_KEY, _finishSound.name));
  }

  void setRestSound(String value) {
    _restSound = Sound.values.byName(value);
    SharedPreferences.getInstance().then((pref) => pref.setString(Constants.STANDARD_REST_SOUND_KEY, _restSound.name));
  }

  void setTrainingSound(String value) {
    _trainingSound = Sound.values.byName(value);
    SharedPreferences.getInstance().then(
      (pref) => pref.setString(Constants.STANDARD_TRAINING_SOUND_KEY, _trainingSound.name),
    );
  }

  void setCountDownSound(String value) {
    _countDownSound = ShortSound.values.byName(value);
    SharedPreferences.getInstance().then(
      (pref) => pref.setString(Constants.STANDARD_COUNTDOWN_SOUND_KEY, _countDownSound.name),
    );
  }

  void setHalfTimeSound(String value) {
    _halfTimeSound = Sound.values.byName(value);
    SharedPreferences.getInstance().then(
      (pref) => pref.setString(Constants.STANDARD_HALF_TIME_SOUND_KEY, _halfTimeSound.name),
    );
  }
}
