import 'package:timer_app/model/routines/abstract/time_routine.dart';
import 'package:timer_app/model/routines/parent_routines/advanced_routine.dart';
import 'package:timer_app/model/routines/parent_routines/simple_routine.dart';
import 'package:timer_app/model/routines/parent_routines/starter_routine.dart';
import 'package:timer_app/model/routines/training_routine.dart';
import 'package:timer_app/resources/constants.dart';

class RoutineFactory {
  static TimeRoutine timeRoutineFromJson(Map<String, dynamic> data) {
    String type = data[Constants.TYPE_STRING];
    switch (type) {
      case Constants.TRAINING_ROUTINE_TYPE:
        return TrainingRoutine.fromJson(data);
      default:
        throw UnimplementedError();
    }
  }

  static StarterRoutine fromMap(Map<String, dynamic> data) {
    String type = data[Constants.TYPE_STRING];
    switch (type) {
      case Constants.SIMPLE_ROUTINE_TYPE:
        return SimpleRoutine.fromJson(data);
      case Constants.ADVANCED_ROUTINE_TYPE:
        return AdvancedRoutine.fromJson(data);
      default:
        throw UnimplementedError();
    }
  }
}
