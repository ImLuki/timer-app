import 'package:timer_app/resources/constants.dart';

class Util {
  /// Calculate Time String with Format hh::mm:ss
  ///
  /// Returns String with time
  static getTimeString(int timeInSeconds) {
    int hour = timeInSeconds ~/ (Constants.SECONDS_PER_MINUTE * Constants.SECONDS_PER_MINUTE);
    int min =
        timeInSeconds % (Constants.SECONDS_PER_MINUTE * Constants.SECONDS_PER_MINUTE) ~/ Constants.SECONDS_PER_MINUTE;
    int seconds = timeInSeconds % Constants.SECONDS_PER_MINUTE;
    if (hour > 0) {
      return "${hour.toString().padLeft(2, "0")}:"
          "${min.toString().padLeft(2, "0")}:"
          "${seconds.toString().padLeft(2, "0")}";
    } else {
      return "${min.toString().padLeft(2, "0")}:${seconds.toString().padLeft(2, "0")}";
    }
  }
}
