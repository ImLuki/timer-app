import 'package:flutter/material.dart';
import 'package:timer_app/model/routines/abstract/abstract_training_routine.dart';
import 'package:timer_app/model/routines/abstract/time_routine.dart';
import 'package:timer_app/model/routines/training_routine.dart';
import 'package:timer_app/model/routines/util/routine_delegator.dart';
import 'package:timer_app/resources/constants.dart';

part 'json/sets_routine_json.dart';

part 'widgets/routine_overview/sets_routine_overview_widget.dart';

class SetsRoutine extends AbstractTrainingRoutine {
  List<TimeRoutine> children;
  int sets;

  SetsRoutine({required this.children, this.sets = 3});

  SetsRoutine.standard()
      : this(
          children: [TrainingRoutine.standard(), TrainingRoutine.rest()],
          sets: 3,
        );

  @override
  int getTimeInSeconds({bool lastSet = false}) {
    int timePerSet = this.children.fold(0, (previous, routine) => previous + routine.getTimeInSeconds(lastSet: false));
    int timePerLastSet =
        this.children.fold(0, (previous, routine) => previous + routine.getTimeInSeconds(lastSet: true));
    return timePerSet * (sets - 1) + timePerLastSet;
  }

  int getTimeRemaining({
    // sets = 2
    required int currentSet, // 1
    required int currentIndex, // 0
    bool lastSet = false,
  }) {
    if (currentSet + 1 == this.sets) {
      return this
          .children
          .getRange(currentIndex, children.length)
          .fold(0, (previous, routine) => previous + routine.getTimeInSeconds(lastSet: true));
    } else {
      int timePerCurrentSet = this
          .children
          .getRange(currentIndex, children.length)
          .fold(0, (previous, routine) => previous + routine.getTimeInSeconds(lastSet: false));

      int timePerSet = this.children.fold(
            0,
            (previous, routine) => previous + routine.getTimeInSeconds(lastSet: false),
          );

      int timePerLastSet = this.children.fold(
            0,
            (previous, routine) => previous + routine.getTimeInSeconds(lastSet: true),
          );

      return timePerSet * (this.sets - currentSet - 2) + timePerLastSet + timePerCurrentSet;
    }
  }

  factory SetsRoutine.fromJson(Map<String, dynamic> json) => _$SetsRoutineFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$SetsRoutineToJson(this);

  @override
  Widget getOverviewWidget({bool leftBorder = false, bool rightBorder = false}) =>
      _$OverviewWidget(children: children, sets: sets);

  @override
  List<Widget> getOverviewText(BuildContext context) => _$OverviewText(context, instance: this);

  SetsRoutine clone() {
    return SetsRoutine(
      children: List.generate(this.children.length, (index) => this.children[index].clone()),
      sets: this.sets,
    );
  }
}
