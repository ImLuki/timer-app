part of '../../repetition_routine.dart';

const double PREP_CONTAINER_MARGIN = 8.0;
const double PREP_CONTAINER_WIDTH = Constants.OVERVIEW_RECTANGLE_WIDTH - PREP_CONTAINER_MARGIN * 2;
const double FONT_SIZE = 13.0;
const double EMPTY_WIDTH_DASH_LINE = 6.0;
const double DASH_WIDTH = 12.0;
const int MAX_LINES = 2;

Widget _$OverviewWidget({required RepetitionRoutine instance}) {
  List<Widget> list = [];

  if (instance.preparationTime > 0) {


    list.add(Container(
      height: Constants.OVERVIEW_RECTANGLE_HEIGHT - PREP_CONTAINER_MARGIN,
      width: PREP_CONTAINER_WIDTH,
      margin: EdgeInsets.zero,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Colors.transparent,
        shape: BoxShape.rectangle,
        borderRadius: const BorderRadius.all(
          Radius.circular(Constants.OVERVIEW_RADIUS),
        ),
        border: Border.all(color: Colors.white, width: Constants.OVERVIEW_CONTAINER_BORDER_WIDTH),
      ),
      child: Text(
        Util.getTimeString(instance.preparationTime),
        style: const TextStyle(fontSize: Constants.OVERVIEW_FONT_SIZE),
      ),
    ));

    list.add(_dottedLine());
  }

  // add reps circle
  if (instance.repetitions > 1) {
    list.add(_repsCircle(reps: instance.repetitions));
    list.add(_dottedLine());
  }

  for (var element in instance.children) {
    list.add(element.getOverviewWidget());
    list.add(_dottedLine());
  }

  if (instance.children.isNotEmpty) list.removeLast();

  return Row(mainAxisAlignment: MainAxisAlignment.start, children: list);
}

List<Widget> _$OverviewText(BuildContext context, {required RepetitionRoutine instance}) {
  List<Widget> list = [];

  if (instance.preparationTime > 0) {
    list.add(
      Container(
        width: PREP_CONTAINER_WIDTH,
        alignment: Alignment.topCenter,
        child: Text(
          AppLocalizations.of(context)!.timer_app_preparation_key,
          overflow: TextOverflow.ellipsis,
          maxLines: MAX_LINES,
          textAlign: TextAlign.center,
          style: const TextStyle(fontSize: FONT_SIZE),
        ),
      ),
    );
    list.add(const SizedBox(width: Constants.OVERVIEW_LINE_WIDTH));
  }

  if (instance.repetitions > 1) {
    if (instance.preparationTime > 0) {
      list.removeLast();
      list.add(const SizedBox(width: Constants.OVERVIEW_LINE_WIDTH / 2));
    }

    list.add(
      Container(
        width: Constants.OVERVIEW_RECTANGLE_HEIGHT +
            (instance.preparationTime > 0 ? Constants.OVERVIEW_LINE_WIDTH : Constants.OVERVIEW_LINE_WIDTH / 2),
        alignment: Alignment.topCenter,
        child: Text(
          AppLocalizations.of(context)!.repetition(2),
          overflow: TextOverflow.ellipsis,
          maxLines: 2,
          textAlign: TextAlign.center,
          style: const TextStyle(fontSize: Constants.OVERVIEW_FONT_SIZE_SMALL),
        ),
      ),
    );
    list.add(const SizedBox(width: Constants.OVERVIEW_LINE_WIDTH / 2));
  }

  for (var element in instance.children) {
    list.addAll(element.getOverviewText(context));
    list.add(const SizedBox(width: Constants.OVERVIEW_LINE_WIDTH));
  }
  if (instance.children.isNotEmpty) list.removeLast();
  return list;
}

Widget _dottedLine() => const DottedLine(
      dashHeight: Constants.OVERVIEW_LINE_HEIGHT,
      totalWidth: Constants.OVERVIEW_LINE_WIDTH,
      emptyWidth: EMPTY_WIDTH_DASH_LINE,
      dashWidth: DASH_WIDTH,
      dashColor: Colors.white54,
    );

Widget _repsCircle({required int reps}) => Container(
      padding: EdgeInsets.zero,
      margin: EdgeInsets.zero,
      height: Constants.OVERVIEW_RECTANGLE_HEIGHT,
      width: Constants.OVERVIEW_RECTANGLE_HEIGHT,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Colors.transparent,
        shape: BoxShape.circle,
        border: Border.all(color: Colors.white, width: Constants.OVERVIEW_CONTAINER_BORDER_WIDTH),
      ),
      child: Text("${reps}x", style: const TextStyle(fontSize: SET_TEXT_SIZE)),
    );
