part of '../../sets_routine.dart';

const double SET_TEXT_SIZE = 20.0;

Widget _$OverviewWidget({required List<AbstractTrainingRoutine> children, required int sets}) {
  List<Widget> list = [];

  if (sets > 1) {
    list.add(_setCircle(sets: sets));
    list.add(const SizedBox(width: Constants.OVERVIEW_BOX_PADDING));
  }

  for (int i = 0; i < children.length; i++) {
    list.add(children[i].getOverviewWidget(leftBorder: i == 0 && sets <= 1, rightBorder: i == children.length - 1));
    list.add(const SizedBox(width: Constants.OVERVIEW_BOX_PADDING));
  }

  if (list.length > 1) {
    list.removeLast();
  }

  return Row(
    mainAxisAlignment: MainAxisAlignment.start,
    children: list,
  );
}

Widget _setCircle({required int sets}) => Container(
      padding: EdgeInsets.zero,
      margin: EdgeInsets.zero,
      height: Constants.OVERVIEW_RECTANGLE_HEIGHT,
      width: Constants.OVERVIEW_SETS_CONTAINER_WIDTH,
      alignment: Alignment.center,
      //color: Colors.red,
      decoration: BoxDecoration(
        color: Colors.transparent,
        shape: BoxShape.rectangle,
        borderRadius: const BorderRadius.horizontal(
          left: Radius.circular(Constants.OVERVIEW_RADIUS),
        ),
        border: Border.all(color: Colors.white, width: Constants.OVERVIEW_CONTAINER_BORDER_WIDTH),
      ),
      child: Text("${sets}x", style: const TextStyle(fontSize: SET_TEXT_SIZE)),
    );

List<Widget> _$OverviewText(BuildContext context, {required SetsRoutine instance}) {
  List<Widget> list = [];

  if (instance.sets > 1) {
    list.add(const SizedBox(width: Constants.OVERVIEW_SETS_CONTAINER_WIDTH + Constants.OVERVIEW_BOX_PADDING));
  }

  for (var element in instance.children) {
    list.addAll(element.getOverviewText(context));
    list.add(const SizedBox(width: Constants.OVERVIEW_BOX_PADDING));
  }

  if (instance.children.isNotEmpty) list.removeLast();
  return list;
}
