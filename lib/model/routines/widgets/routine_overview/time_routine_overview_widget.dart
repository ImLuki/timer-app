part of '../../abstract/time_routine.dart';

const int MAX_LINES = 2;

Widget _$OverviewWidget({required int time, required Color color, bool leftBorder = false, bool rightBorder = false}) {
  return Container(
    height: Constants.OVERVIEW_RECTANGLE_HEIGHT,
    width: Constants.OVERVIEW_RECTANGLE_WIDTH,
    margin: EdgeInsets.zero,
    alignment: Alignment.center,
    decoration: BoxDecoration(
      color: color,
      shape: BoxShape.rectangle,
      borderRadius: BorderRadius.horizontal(
        left: Radius.circular(leftBorder ? Constants.OVERVIEW_RADIUS : 0.0),
        right: Radius.circular(rightBorder ? Constants.OVERVIEW_RADIUS : 0.0),
      ),
    ),
    child: Text(
      Util.getTimeString(time),
      style: const TextStyle(fontSize: Constants.OVERVIEW_FONT_SIZE),
    ),
  );
}

List<Widget> _$OverviewText({required TimeRoutine instance}) {
  return [
    Container(
      width: Constants.OVERVIEW_RECTANGLE_WIDTH,
      alignment: Alignment.center,
      child: Text(
        instance.infoText,
        overflow: TextOverflow.ellipsis,
        maxLines: MAX_LINES,
        textAlign: TextAlign.center,
        style: const TextStyle(
          fontSize: Constants.OVERVIEW_FONT_SIZE_SMALL,
          height: 1.0,
        ),
      ),
    )
  ];
}
