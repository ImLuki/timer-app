import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:timer_app/model/data_store.dart';
import 'package:timer_app/model/routines/abstract/abstract_training_routine.dart';
import 'package:timer_app/model/routines/sets_routine.dart';
import 'package:timer_app/model/routines/util/util.dart';
import 'package:timer_app/resources/constants.dart';
import 'package:timer_app/view/util/dotted_line.dart';

part 'json/repetition_routine_json.dart';

part 'widgets/routine_overview/repetition_routine_overview_widget.dart';

class RepetitionRoutine extends AbstractTrainingRoutine {
  List<SetsRoutine> children;
  late int _preparationTime;
  int repetitions;

  RepetitionRoutine({int preparationTime = 0, required this.children, this.repetitions = 1}) {
    this._preparationTime = preparationTime;
  }

  @override
  int getTimeInSeconds({bool lastSet = false}) {
    return this.preparationTime +
        this.children.fold<int>(0, (previous, routine) => previous + routine.getTimeInSeconds()) * repetitions;
  }

  int getTimeRemaining({
    required int currentRepetition,
    required int currentRepIndex,
    required int currentSet,
    required int currentIndex,
  }) {
    int currentRepTime = 0;
    for (int i = currentRepIndex; i < children.length; i++) {
      if (i == currentRepIndex) {
        currentRepTime += children[i].getTimeRemaining(
          currentSet: currentSet,
          currentIndex: currentIndex,
        );
      } else {
        currentRepTime += children[i].getTimeRemaining(
          currentSet: 0,
          currentIndex: 0,
        );
      }
    }

    int otherRepTime = this.children.fold<int>(
          0,
          (previous, routine) =>
              previous +
              routine.getTimeRemaining(
                currentSet: 0,
                currentIndex: 0,
              ),
        );

    return currentRepTime + otherRepTime * (this.repetitions - currentRepetition - 1);
  }

  factory RepetitionRoutine.fromJson(Map<String, dynamic> json) => _$RepetitionRoutineFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$RepetitionRoutineToJson(this);

  @override
  Widget getOverviewWidget({bool leftBorder = false, bool rightBorder = false}) => _$OverviewWidget(instance: this);

  @override
  List<Widget> getOverviewText(BuildContext context) {
    return _$OverviewText(context, instance: this);
  }

  int get preparationTime {
    return this._preparationTime == -1 ? DataStore().settingModel.standardPrepTime : this._preparationTime;
  }

  set preparationTime(int value) {
    _preparationTime = value;
  }
}
