import 'package:flutter/cupertino.dart';

abstract class AbstractTrainingRoutine {
  /// Returns time of training routine in seconds
  int getTimeInSeconds({bool lastSet = false});

  Map<String, dynamic> toJson();

  Widget getOverviewWidget({bool leftBorder = false, bool rightBorder = false});

  List<Widget> getOverviewText(BuildContext context);
}
