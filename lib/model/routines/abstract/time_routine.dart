import 'package:flutter/material.dart';
import 'package:timer_app/model/data_store.dart';
import 'package:timer_app/model/routines/abstract/abstract_training_routine.dart';
import 'package:timer_app/model/routines/util/util.dart';
import 'package:timer_app/model/sounds.dart';
import 'package:timer_app/resources/constants.dart';

part '../widgets/routine_overview/time_routine_overview_widget.dart';

abstract class TimeRoutine extends AbstractTrainingRoutine {
  int time;
  String infoText;
  Color color;
  Sound sound;
  int skipInLastSet; // 1 true, 0 false, -1 default (look in current settings)
  int hasHalfWaySound; // 1 true, 0 false, -1 default (look in current settings)

  TimeRoutine({
    required this.time,
    required this.infoText,
    required this.sound,
    required this.color,
    required this.hasHalfWaySound,
    required this.skipInLastSet,
  });

  @override
  int getTimeInSeconds({bool lastSet = false, bool shouldSkipOnDefault = false}) {
    if (lastSet && this.skipInLastSet == 1) return 0;
    if (lastSet && this.skipInLastSet == -1 && DataStore().settingModel.skipLastRest && shouldSkipOnDefault) return 0;
    return time;
  }

  @override
  Widget getOverviewWidget({bool leftBorder = false, bool rightBorder = false}) => _$OverviewWidget(
        time: time,
        color: this.color,
        leftBorder: leftBorder,
        rightBorder: rightBorder,
      );

  @override
  List<Widget> getOverviewText(BuildContext context) => _$OverviewText(instance: this);

  TimeRoutine clone();
}

// for legacy support. Has actually been removed
enum TimeRoutineType { Undefined, TrainingRoutine, RestRoutine }
