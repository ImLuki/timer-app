import 'package:flutter/material.dart';
import 'package:timer_app/model/data_store.dart';
import 'package:timer_app/model/routines/abstract/time_routine.dart';
import 'package:timer_app/model/sounds.dart';
import 'package:timer_app/resources/constants.dart';
import 'package:timer_app/theme/app_colors.dart';

part 'json/training_routine_json.dart';

class TrainingRoutine extends TimeRoutine {
  static const int STANDARD_TRAINING_TIME = 30;
  static const int STANDARD_REST_TIME = 10;

  TrainingRoutine({
    required super.time,
    required super.infoText,
    required super.sound,
    required super.color,
    required super.hasHalfWaySound,
    required super.skipInLastSet,
  });

  TrainingRoutine.standard({String infoText = "", time = STANDARD_TRAINING_TIME})
      : this(
          time: time,
          infoText: infoText == "" ? "Training" : infoText,
          sound: DataStore().settingModel.trainingSound,
          color: DataStore().settingModel.standardTrainingColor,
          skipInLastSet: 0,
          hasHalfWaySound: -1,
        );

  TrainingRoutine.rest({String infoText = "", time = STANDARD_REST_TIME})
      : this(
          time: time,
          infoText: infoText == "" ? "Rest" : infoText,
          sound: DataStore().settingModel.restSound,
          color: DataStore().settingModel.standardRestColor,
          hasHalfWaySound: 0,
          skipInLastSet: 0,
        );

  factory TrainingRoutine.fromJson(Map<String, dynamic> json) => _$TrainingRoutineFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$TrainingRoutineToJson(this);

  @override
  TimeRoutine clone() {
    return TrainingRoutine(
      time: this.time,
      infoText: this.infoText,
      color: this.color,
      sound: this.sound,
      hasHalfWaySound: this.hasHalfWaySound,
      skipInLastSet: this.skipInLastSet,
    );
  }
}
