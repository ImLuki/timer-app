import 'dart:convert';

import 'package:timer_app/model/data_access_object.dart';
import 'package:timer_app/model/routines/parent_routines/starter_routine.dart';
import 'package:timer_app/model/routines/repetition_routine.dart';
import 'package:timer_app/model/routines/sets_routine.dart';
import 'package:timer_app/model/routines/training_routine.dart';
import 'package:timer_app/resources/constants.dart';

part '../json/simple_routine_json.dart';

class SimpleRoutine extends StarterRoutine {
  static const int STANDARD_SET_AMOUNT = 5;

  late SetsRoutine setsRoutine;
  late TrainingRoutine training;
  late TrainingRoutine rest;

  SimpleRoutine({
    super.id,
    int sets = STANDARD_SET_AMOUNT,
    int trainingTime = TrainingRoutine.STANDARD_TRAINING_TIME,
    int restTime = TrainingRoutine.STANDARD_REST_TIME,
    super.name,
    super.index,
    DateTime? super.dateTime,
  }) {
    this.training = TrainingRoutine.standard(time: trainingTime);
    this.rest = TrainingRoutine.rest(time: restTime);
    this.setsRoutine = SetsRoutine(sets: sets, children: [this.training, this.rest]);
    this.repetitionRoutine = RepetitionRoutine(
      children: [setsRoutine],
    );
  }

  factory SimpleRoutine.fromJson(Map<String, dynamic> json) => _$SimpleRoutineFromJson(json);

  @override
  Map<String, dynamic> toMap() => _$SimpleRoutineToMap(this);

  setTrainingTime(int trainingTime) {
    this.training.time = trainingTime;
  }

  setRestTime(int restTime) {
    this.rest.time = restTime;
  }

  setSets(int sets) {
    this.setsRoutine.sets = sets;
  }

  int getSets() {
    return this.setsRoutine.sets;
  }

  int getTrainingTime() {
    return training.getTimeInSeconds();
  }

  int getRestTime() {
    return rest.getTimeInSeconds();
  }

  @override
  SimpleRoutine clone({int? id}) {
    return SimpleRoutine(
      id: id,
      sets: getSets(),
      trainingTime: training.time,
      restTime: rest.time,
      name: name,
    );
  }
}
