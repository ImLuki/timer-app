import 'dart:convert';

import 'package:timer_app/model/data_access_object.dart';
import 'package:timer_app/model/data_store.dart';
import 'package:timer_app/model/routines/parent_routines/starter_routine.dart';
import 'package:timer_app/model/routines/repetition_routine.dart';
import 'package:timer_app/model/routines/sets_routine.dart';
import 'package:timer_app/model/routines/training_routine.dart';
import 'package:timer_app/resources/constants.dart';

part '../json/advanced_routine_json.dart';

class AdvancedRoutine extends StarterRoutine {
  AdvancedRoutine({
    super.id,
    super.name,
    super.index,
    DateTime? super.dateTime,
    RepetitionRoutine? child,
  }) {
    if (child == null) {
      this.repetitionRoutine = RepetitionRoutine(
        preparationTime: DataStore().settingModel.standardPrepTime,
        children: [
          SetsRoutine(
            children: [
              TrainingRoutine.standard(),
              TrainingRoutine.rest(),
            ],
          ),
        ],
      );
    } else {
      this.repetitionRoutine = child;
    }
  }

  factory AdvancedRoutine.fromJson(Map<String, dynamic> json) => _$AdvancedRoutineFromJson(json);

  @override
  Map<String, dynamic> toMap() => _$AdvancedRoutineToMap(this);

  @override
  AdvancedRoutine clone({int? id}) {
    return AdvancedRoutine(
      id: id,
      name: this.name,
      child: RepetitionRoutine.fromJson(this.repetitionRoutine.toJson()),
    );
  }
}
