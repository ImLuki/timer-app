import 'package:flutter/material.dart';
import 'package:timer_app/model/routines/repetition_routine.dart';
import 'package:timer_app/resources/constants.dart';

abstract class StarterRoutine {
  int? id;
  int index;
  String name;
  late DateTime dateTime;
  late RepetitionRoutine repetitionRoutine;

  StarterRoutine({this.index = 0, this.name = "", this.id, dateTime}) {
    if (dateTime == null) {
      this.dateTime = DateTime.now();
    } else {
      this.dateTime = dateTime;
    }
  }

  StarterRoutine clone();

  Map<String, dynamic> toMap();

  int getTimeInSeconds() {
    return repetitionRoutine.getTimeInSeconds();
  }

  int getTimeRemainingInSeconds({
    required bool prep,
    required int currentRepetition,
    required int currentRepIndex,
    required int sets,
    required int currentIndex,
  }) {
    if (prep) return getTimeInSeconds();
    return repetitionRoutine.getTimeRemaining(
      currentRepetition: currentRepetition,
      currentRepIndex: currentRepIndex,
      currentSet: sets,
      currentIndex: currentIndex,
    );
  }

  Widget getOverviewWidget({bool leftBorder = false, bool rightBorder = false}) =>
      this.repetitionRoutine.getOverviewWidget();

  List<Widget> getOverviewText(BuildContext context) => this.repetitionRoutine.getOverviewText(context);

  String getTotalTimeString() {
    int completeTime = getTimeInSeconds();

    int hour = completeTime ~/ (Constants.SECONDS_PER_MINUTE * Constants.SECONDS_PER_MINUTE);
    int min =
        completeTime % (Constants.SECONDS_PER_MINUTE * Constants.SECONDS_PER_MINUTE) ~/ Constants.SECONDS_PER_MINUTE;
    int seconds = completeTime % Constants.SECONDS_PER_MINUTE;

    if (hour > 0) {
      return "${hour.toString().padLeft(2, "0")}h "
          "${min.toString().padLeft(2, "0")}min "
          "${seconds.toString().padLeft(2, "0")}s";
    } else if (min > 0) {
      return "${min.toString().padLeft(2, "0")}m ${seconds.toString().padLeft(2, "0")}s";
    } else {
      return "${seconds.toString().padLeft(2, "0")}s";
    }
  }

  int get repetitions => this.repetitionRoutine.repetitions;

  int get prepTime => this.repetitionRoutine.preparationTime;

  set repetitions(int reps) => this.repetitionRoutine.repetitions = reps;

  set prepTime(int time) => this.repetitionRoutine.preparationTime = time;
}
