part of '../sets_routine.dart';

SetsRoutine _$SetsRoutineFromJson(Map<String, dynamic> json) => SetsRoutine(
      children: List.generate(json[Constants.ROUTINE_LIST_KEY].length,
          (index) => RoutineFactory.timeRoutineFromJson(json[Constants.ROUTINE_LIST_KEY][index])),
      sets: json[Constants.SETS_KEY] as int? ?? 1,
    );

Map<String, dynamic> _$SetsRoutineToJson(SetsRoutine instance) => <String, dynamic>{
      Constants.TYPE_STRING: Constants.SETS_ROUTINE_TYPE,
      Constants.SETS_KEY: instance.sets,
      Constants.ROUTINE_LIST_KEY: instance.children.map((e) => e.toJson()).toList(),
    };
