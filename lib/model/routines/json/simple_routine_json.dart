part of '../parent_routines/simple_routine.dart';

const int STANDARD_SET_AMOUNT = 5;

SimpleRoutine _$SimpleRoutineFromJson(Map<String, dynamic> json) {
  return SimpleRoutine(
    id: json[DataAccessObject.DB_ID_KEY] as int?,
    sets: jsonDecode(json[DataAccessObject.DB_TRAINING_KEY])[Constants.ROUTINES_KEY][0][Constants.SETS_KEY] as int? ??
        STANDARD_SET_AMOUNT,
    name: json[DataAccessObject.DB_NAME_KEY] as String? ?? "",
    index: json[DataAccessObject.DB_INDEX_KEY] as int? ?? 0,
    restTime: jsonDecode(json[DataAccessObject.DB_TRAINING_KEY])[Constants.ROUTINES_KEY][0][Constants.ROUTINE_LIST_KEY]
        [1][Constants.TIME_KEY],
    trainingTime: jsonDecode(json[DataAccessObject.DB_TRAINING_KEY])[Constants.ROUTINES_KEY][0]
        [Constants.ROUTINE_LIST_KEY][0][Constants.TIME_KEY],
    dateTime: json[DataAccessObject.DB_DATE_KEY] == null
        ? null
        : DateTime.parse(json[DataAccessObject.DB_DATE_KEY] as String),
  );
}

Map<String, dynamic> _$SimpleRoutineToMap(SimpleRoutine instance) => <String, dynamic>{
      DataAccessObject.DB_TYPE_KEY: Constants.SIMPLE_ROUTINE_TYPE,
      DataAccessObject.DB_ID_KEY: instance.id,
      DataAccessObject.DB_INDEX_KEY: instance.index,
      DataAccessObject.DB_NAME_KEY: instance.name,
      DataAccessObject.DB_DATE_KEY: instance.dateTime.toIso8601String(),
      DataAccessObject.DB_TRAINING_KEY: jsonEncode(instance.repetitionRoutine.toJson()),
    };
