part of '../training_routine.dart';

TrainingRoutine _$TrainingRoutineFromJson(Map<String, dynamic> json) {
  return TrainingRoutine(
    time: json[Constants.TIME_KEY] as int,
    infoText: json[Constants.INFO_TEXT_KEY] as String? ?? "",
    sound: _getSoundFromJson(json),
    color: _getColorFromJson(json),
    skipInLastSet: json[Constants.SKIP_LAST_SET_KEY] as int? ?? 0,
    hasHalfWaySound: json[Constants.HAS_HALF_WAY_SOUND_KEY] as int? ?? 0,
  );
}

Sound _getSoundFromJson(Map<String, dynamic> json) {
  if (json[Constants.SOUND_KEY] != null) return Sound.values[json[Constants.SOUND_KEY] as int];

  int? type = json[Constants.TIME_ROUTINE_TYPE_KEY] as int?;
  if (type == null) {
    return Sound.Beep2x;
  } else {
    switch (TimeRoutineType.values.elementAt(type)) {
      case TimeRoutineType.RestRoutine:
        return DataStore().settingModel.restSound;
      case TimeRoutineType.TrainingRoutine:
      case TimeRoutineType.Undefined:
        return DataStore().settingModel.trainingSound;
    }
  }
}

Color _getColorFromJson(Map<String, dynamic> json) {
  if (json[Constants.COLOR_KEY] != null) return Color(int.parse(json[Constants.COLOR_KEY])).withOpacity(1.0);

  int? type = json[Constants.TIME_ROUTINE_TYPE_KEY] as int?;
  if (type == null) {
    return AppColors.primary;
  } else {
    switch (TimeRoutineType.values.elementAt(type)) {
      case TimeRoutineType.RestRoutine:
        return DataStore().settingModel.standardRestColor;
      case TimeRoutineType.TrainingRoutine:
      case TimeRoutineType.Undefined:
        return DataStore().settingModel.standardTrainingColor;
    }
  }
}

Map<String, dynamic> _$TrainingRoutineToJson(TrainingRoutine instance) => <String, dynamic>{
      Constants.TYPE_STRING: Constants.TRAINING_ROUTINE_TYPE,
      Constants.TIME_KEY: instance.time,
      Constants.INFO_TEXT_KEY: instance.infoText,
      Constants.SOUND_KEY: instance.sound.index,
      Constants.COLOR_KEY: instance.color.value.toString(),
      Constants.HAS_HALF_WAY_SOUND_KEY: instance.hasHalfWaySound,
      Constants.SKIP_LAST_SET_KEY: instance.skipInLastSet
    };
