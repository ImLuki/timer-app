part of '../repetition_routine.dart';

RepetitionRoutine _$RepetitionRoutineFromJson(Map<String, dynamic> json) => RepetitionRoutine(
      preparationTime: json[Constants.PREPARATION_TIME_KEY] as int? ?? 0,
      children: List.generate(json[Constants.ROUTINES_KEY].length,
          (index) => SetsRoutine.fromJson(json[Constants.ROUTINES_KEY][index])),
      repetitions: json[Constants.REPETITIONS_KEY] as int? ?? 1,
    );

Map<String, dynamic> _$RepetitionRoutineToJson(RepetitionRoutine instance) => <String, dynamic>{
      Constants.TYPE_STRING: Constants.REPETITION_ROUTINE_TYPE,
      Constants.REPETITIONS_KEY: instance.repetitions,
      Constants.PREPARATION_TIME_KEY: instance.preparationTime,
      Constants.ROUTINES_KEY: instance.children.map((e) => e.toJson()).toList(),
    };
