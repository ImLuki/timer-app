part of '../parent_routines/advanced_routine.dart';

const int STANDARD_SET_AMOUNT = 5;

AdvancedRoutine _$AdvancedRoutineFromJson(Map<String, dynamic> json) => AdvancedRoutine(
      id: json[DataAccessObject.DB_ID_KEY] as int?,
      name: json[DataAccessObject.DB_NAME_KEY] as String? ?? "",
      index: json[DataAccessObject.DB_INDEX_KEY] as int? ?? 0,
      dateTime: json[DataAccessObject.DB_DATE_KEY] == null
          ? null
          : DateTime.parse(json[DataAccessObject.DB_DATE_KEY] as String),
      child: RepetitionRoutine.fromJson(
        jsonDecode(json[DataAccessObject.DB_TRAINING_KEY]),
      ),
    );

Map<String, dynamic> _$AdvancedRoutineToMap(AdvancedRoutine instance) => <String, dynamic>{
      DataAccessObject.DB_TYPE_KEY: Constants.ADVANCED_ROUTINE_TYPE,
      DataAccessObject.DB_INDEX_KEY: instance.index,
      DataAccessObject.DB_ID_KEY: instance.id,
      DataAccessObject.DB_NAME_KEY: instance.name,
      DataAccessObject.DB_DATE_KEY: instance.dateTime.toIso8601String(),
      DataAccessObject.DB_TRAINING_KEY: jsonEncode(instance.repetitionRoutine.toJson()),
    };
