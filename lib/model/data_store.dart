import 'package:timer_app/model/routine_repository.dart';
import 'package:timer_app/model/setting_model.dart';
import 'package:timer_app/model/sounds.dart';

class DataStore {
  DataStore._privateConstructor();

  static final DataStore _instance = DataStore._privateConstructor();

  factory DataStore() => _instance;

  final RoutineRepository routineRepository = RoutineRepository();
  final SettingModel settingModel = SettingModel();
  final SoundHandler soundHandler = SoundHandler();

  Future<void> init() async {

    await settingModel.init();
    await routineRepository.init();

    // Waiting does take long, but otherwise sounds can not be played
    await soundHandler.init();
  }
}
