import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:timer_app/resources/constants.dart';

class DataWriter {
  final String content;
  final String fileName;
  final String folder;

  DataWriter({required this.content, required this.fileName, this.folder = Constants.ROUTINES_FOLDER});

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    final Directory dir = Directory(join(directory.path, this.folder));
    dir.create();
    return dir.path;
  }

  Future<File> get localFile async {
    final path = await _localPath;
    return File(join(path, "$fileName${Constants.JSON_ENDING}"));
  }

  Future<File> write() async {
    final file = await localFile;
    file.createSync();
    // Write the file
    return file.writeAsString(this.content);
  }

  Future<void> delete() async {
    final file = await localFile;
    await file.delete();
  }
}
