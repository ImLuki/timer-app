import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:timer_app/resources/constants.dart';

class DataHandler {
  Future<bool> deleteRoutine(String name) async {
    final path = await _localPath;
    return await this._deleteFile(File(join(path, "$name${Constants.JSON_ENDING}")));
  }

  Future<bool> _deleteFile(File file) async {
    try {
      if (await file.exists()) {
        await file.delete();
        return true;
      }
    } catch (_) {}
    return false;
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    final Directory dir = Directory(join(directory.path, Constants.ROUTINES_FOLDER));
    dir.create();
    return dir.path;
  }
}
