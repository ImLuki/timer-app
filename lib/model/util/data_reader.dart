import 'dart:io';

class DataReader {
  final String path;

  DataReader({required this.path});

  Future<String> getJsonFromFile() async {
    String jsonString = await _readFile(this.path);
    return jsonString;
  }

  Future<String> _readFile(String filePath) async {
    try {
      return await File(filePath).readAsString();
    } catch (e) {
      return "";
    }
  }
}
