import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:timer_app/model/routines/parent_routines/starter_routine.dart';
import 'package:timer_app/model/routines/util/routine_delegator.dart';

class DataAccessObject {
  static const String DATA_BASE_NAME = "timer_app_database";
  static const String DB_ROUTINE_TABLE_KEY = "routines";
  static const String DB_DATE_KEY = "date";
  static const String DB_NAME_KEY = "name";
  static const String DB_INDEX_KEY = "sort_index";
  static const String DB_TRAINING_KEY = "training";
  static const String DB_TYPE_KEY = "type";
  static const String DB_ID_KEY = "routine_id";

  late final Database _dataBase;

  Future<void> init() async {
    String databasesPath = await getDatabasesPath();
    String path = join(databasesPath, '$DATA_BASE_NAME.db');

    _dataBase = await openDatabase(path, version: 1, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute("CREATE TABLE $DB_ROUTINE_TABLE_KEY ("
        "$DB_ID_KEY INTEGER PRIMARY KEY AUTOINCREMENT, "
        "$DB_NAME_KEY TEXT, "
        "$DB_INDEX_KEY INT, "
        "$DB_DATE_KEY TEXT, "
        "$DB_TYPE_KEY TEXT, "
        "$DB_TRAINING_KEY TEXT)");
  }

  /// This method insert a [starterRoutine] to the database
  /// and returns the id of the last inserted row.
  Future<int> addRoutine(StarterRoutine starterRoutine) async {
    return await _dataBase.insert(DB_ROUTINE_TABLE_KEY, starterRoutine.toMap());
  }

  Future<void> updateRoutine(StarterRoutine starterRoutine) async {
    await _dataBase.update(
      DB_ROUTINE_TABLE_KEY,
      starterRoutine.toMap(),
      where: "$DB_ID_KEY = ?",
      whereArgs: [starterRoutine.id],
    );
  }

  void removeRoutine(StarterRoutine starterRoutine) {
    _dataBase.delete(
      DB_ROUTINE_TABLE_KEY,
      where: "$DB_ID_KEY = ?",
      whereArgs: [starterRoutine.id],
    );
  }

  Future<List<StarterRoutine>> getAllRoutines() async {
    List<Map<String, Object?>> result = await _dataBase.query(DB_ROUTINE_TABLE_KEY);
    return List.generate(result.length, (index) => RoutineFactory.fromMap(result[index]));
  }

  Future<void> updateIndices(Iterable<StarterRoutine> starterRoutine) async {
    Batch batch = _dataBase.batch();
    for (var element in starterRoutine) {
      batch.update(
        DB_ROUTINE_TABLE_KEY,
        {DB_INDEX_KEY: element.index},
        where: "$DB_ID_KEY = ?",
        whereArgs: [element.id],
      );
    }
    await batch.commit();
  }
}
