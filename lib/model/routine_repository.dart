import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timer_app/model/data_access_object.dart';
import 'package:timer_app/model/routines/parent_routines/advanced_routine.dart';
import 'package:timer_app/model/routines/parent_routines/simple_routine.dart';
import 'package:timer_app/model/routines/parent_routines/starter_routine.dart';
import 'package:timer_app/model/routines/repetition_routine.dart';
import 'package:timer_app/model/routines/sets_routine.dart';
import 'package:timer_app/model/routines/training_routine.dart';
import 'package:timer_app/model/sounds.dart';
import 'package:timer_app/theme/app_colors.dart';

class RoutineRepository with ChangeNotifier {
  bool initialized = false;

  late List<StarterRoutine> _routines;
  final DataAccessObject _dataAccessObject = DataAccessObject();

  Future<void> init() async {
    await _dataAccessObject.init();
    this._routines = await _dataAccessObject.getAllRoutines();

    initialized = true;
  }

  void addStandardRoutinesIfFirstStart(BuildContext context) async {
    if (!initialized) {
      await _dataAccessObject.init();
      initialized = true;
    }

    // add standard Routines
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool firstTime = prefs.getBool('first_time') ?? true; //check if app starts first time
    if (firstTime && this._routines.isEmpty) {
      await _addStandardRoutines(context); //ignore: use_build_context_synchronously
      this._routines = await _dataAccessObject.getAllRoutines();

      notifyListeners();
    }
    prefs.setBool('first_time', false);
  }

  Future<void> addRoutine(StarterRoutine starterRoutine) async {
    if (this._routines.any((element) => element.id == starterRoutine.id)) {
      _routines.removeWhere((element) => element.id == starterRoutine.id);
      _routines.add(starterRoutine);
      await this._dataAccessObject.updateRoutine(starterRoutine);
    } else {
      for (var elem in this._routines) {
        elem.index++;
      }
      this._routines.add(starterRoutine);
      int res = await this._dataAccessObject.addRoutine(starterRoutine);
      starterRoutine.id = res;
      this.updateIndices();
    }
    notifyListeners();
  }

  void removeRoutine(StarterRoutine starterRoutine) {
    this._routines.remove(starterRoutine);
    notifyListeners();
    this._dataAccessObject.removeRoutine(starterRoutine);
  }

  void updateIndices() {
    _dataAccessObject.updateIndices(this._routines);
  }

  Iterable<StarterRoutine> getAll() => this._routines;

  Future<void> _addStandardRoutines(BuildContext context) async {
    final AdvancedRoutine advancedRoutine = AdvancedRoutine(
      name: AppLocalizations.of(context)!.timer_app_training_hiit_key,
      child: RepetitionRoutine(
        preparationTime: 5,
        children: [
          SetsRoutine(
            sets: 1,
            children: [
              TrainingRoutine(
                time: 180,
                infoText: AppLocalizations.of(context)!.timer_app_warm_up_key,
                sound: Sound.Harp2x,
                color: AppColors.prepColor,
                hasHalfWaySound: 0,
                skipInLastSet: 0,
              ),
            ],
          ),
          SetsRoutine(
            sets: 8,
            children: [
              TrainingRoutine(
                time: 20,
                infoText: AppLocalizations.of(context)!.timer_app_training_key,
                sound: Sound.Harp2x,
                color: AppColors.secondary,
                hasHalfWaySound: 0,
                skipInLastSet: 0,
              ),
              TrainingRoutine.rest()
            ],
          ),
          SetsRoutine(
            sets: 1,
            children: [
              TrainingRoutine(
                time: 90,
                infoText: AppLocalizations.of(context)!.timer_app_cool_down_key,
                sound: Sound.Harp2x,
                color: AppColors.prepColor,
                hasHalfWaySound: 0,
                skipInLastSet: 0,
              ),
            ],
          ),
        ],
      ),
    );

    SimpleRoutine simpleRoutine = SimpleRoutine(
      name: AppLocalizations.of(context)!.timer_app_training_one_key,
    );

    await addRoutine(advancedRoutine);
    await addRoutine(simpleRoutine);
  }
}
