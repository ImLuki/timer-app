import 'package:just_audio/just_audio.dart';
import 'package:timer_app/model/tts_handler.dart';

enum Sound {
  None,
  Beep1x,
  Beep2x,
  Alarm1x,
  Alarm2x,
  Harp1x,
  Harp2x,
  Piano1x,
  Piano2x,
  Synth1x,
  Synth2x,
  Final1,
  Final2,
  Final3,
  TextToSpeech,
}

enum ShortSound {
  None,
  Beep1x,
  Alarm1x,
  Harp1x,
  Piano1x,
  Synth1x,
  TextToSpeech,
}

class SoundHandler {
  static Sound getStandard() => Sound.Beep2x;

  static Sound getStandardFinalSound() => Sound.Final1;

  static ShortSound getStandardShortSound() => ShortSound.Beep1x;

  final Map<String, AudioPlayer> sounds = {};
  final TTSHandler _ttsHandler = TTSHandler();
  double _volume = 1.0;

  Future<void> init() async {
    for (Sound sound in Sound.values) {
      if (sound == Sound.None || sound == Sound.TextToSpeech) continue;

      final AudioPlayer player = AudioPlayer(handleAudioSessionActivation: false);
      await player.setAsset("assets/sounds/${sound.name}.mp3");
      await player.setVolume(_volume);
      sounds[sound.name] = player;
    }

    for (ShortSound sound in ShortSound.values) {
      if (sound == ShortSound.None || sound == ShortSound.TextToSpeech) continue;
      if (!sounds.containsKey(sound.name)) {
        final AudioPlayer player = AudioPlayer(handleAudioSessionActivation: false);
        await player.setAsset("assets/sounds/${sound.name}.mp3");
        await player.setVolume(_volume);
        sounds[sound.name] = player;
      }
    }
    await _ttsHandler.init(_volume);
  }

  void playSound(Sound sound, String text) async {
    if (sound == Sound.None) return;
    if (sound == Sound.TextToSpeech) _ttsHandler.speak(text);
    await sounds[sound.name]?.seek(const Duration(milliseconds: 0));
    await sounds[sound.name]?.play();
  }

  void playShortSound(ShortSound sound, String text) async {
    if (sound == ShortSound.None) return;
    if (sound == ShortSound.TextToSpeech) _ttsHandler.speak(text);
    await sounds[sound.name]?.seek(const Duration(milliseconds: 0));
    await sounds[sound.name]?.play();
  }

  void stopSound() async {
    await _ttsHandler.stop();
  }

  double get volume => _volume;

  set volume(double value) {
    _volume = value;

    _ttsHandler.setVolume(_volume);
    for (Sound sound in Sound.values) {
      sounds[sound.name]?.setVolume(_volume);
    }
    for (ShortSound sound in ShortSound.values) {
      sounds[sound.name]?.setVolume(_volume);
    }
  }
}
