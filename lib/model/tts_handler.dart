import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter_tts/flutter_tts.dart';

class TTSHandler {
  late FlutterTts flutterTts;

  Future<void> init(double volume) async {
    try {
      await _initTts();
      String localeName = Platform.localeName;
      bool res = await flutterTts.isLanguageAvailable(localeName);
      if (res) {
        await flutterTts.setLanguage(localeName);
      }
      await setVolume(volume);
    } on Exception catch (e) {
      debugPrint(e.toString());
    }
  }

  Future<void> _initTts() async {
    flutterTts = FlutterTts();

    _getDefaultEngine();

    flutterTts.setErrorHandler((msg) {
      debugPrint("error: $msg");
    });

    if (Platform.isIOS) {
      await flutterTts.setSharedInstance(true);
      await flutterTts.setIosAudioCategory(IosTextToSpeechAudioCategory.ambient,
          [
            IosTextToSpeechAudioCategoryOptions.allowBluetooth,
            IosTextToSpeechAudioCategoryOptions.allowBluetoothA2DP,
            IosTextToSpeechAudioCategoryOptions.mixWithOthers,
          ],
          IosTextToSpeechAudioMode.voicePrompt
      );
    }
  }

  Future<void> setVolume(double volume) async {
    await flutterTts.setVolume(volume);
  }

  Future speak(String text) async {
    await flutterTts.speak(text);
  }

  Future stop() async {
    await flutterTts.stop();
  }

  Future _getDefaultEngine() async {
    var engine = await flutterTts.getDefaultEngine;
    if (engine != null) {
      debugPrint(engine);
    }
  }
}
