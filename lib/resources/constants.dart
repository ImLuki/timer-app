abstract class Constants {
  ///
  /// Routines
  ///
  static const String TYPE_STRING = "type";
  static const String SIMPLE_ROUTINE_TYPE = "SIMPLE_ROUTINE";
  static const String ADVANCED_ROUTINE_TYPE = "ADVANCED_ROUTINE";
  static const String REPETITION_ROUTINE_TYPE = "REPETITION_ROUTINE";
  static const String SETS_ROUTINE_TYPE = "SETS_ROUTINE";
  static const String TRAINING_ROUTINE_TYPE = "TRAINING_ROUTINE";
  static const String PREPARATION_TIME_KEY = "preparationTime";
  static const String ROUTINES_KEY = "routines";
  static const String REPETITIONS_KEY = "repetitions";
  static const String ROUTINE_LIST_KEY = "routineList";
  static const String SETS_KEY = "sets";
  static const String TIME_KEY = "time";
  static const String INFO_TEXT_KEY = "infoText";
  static const String COLOR_KEY = "color";
  static const String SOUND_KEY = "sound";
  static const String HAS_HALF_WAY_SOUND_KEY = "hasHalfWaySound";
  static const String SKIP_LAST_SET_KEY = "skipInLastSet";
  static const String TIME_ROUTINE_TYPE_KEY = "time_routine_type";

  ///
  /// SharedPreferences keys
  ///
  static const String STANDARD_TRAINING_COLOR_KEY = "standard_training_color";
  static const String STANDARD_REST_COLOR_KEY = "standard_rest_color";
  static const String STANDARD_PREP_COLOR_KEY = "standard_prep_color";
  static const String STANDARD_PREP_DURATION_KEY = "standard_prep_duration";
  static const String SKIP_LAST_REST_KEY = "skip_last_rest_key";
  static const String VIBRATE_KEY = "vibrate_key";
  static const String NOTIFICATION_ENABLE_KEY = "notification_enabled_key";
  static const String ENABLE_SOUND_KEY = "enable_sound_key";
  static const String COUNTING_SOUND_KEY = "counting_sound_key";
  static const String STANDARD_TRAINING_SOUND_KEY = "standard_training_sound";
  static const String STANDARD_REST_SOUND_KEY = "standard_rest_sound";
  static const String STANDARD_FINISH_SOUND_KEY = "standard_finish_sound";
  static const String STANDARD_COUNTDOWN_SOUND_KEY = "standard_countdown_sound";
  static const String STANDARD_HALF_TIME_SOUND_KEY = "standard_half_time_sound";
  static const String HALF_TIME_SOUND_ACTIVATED_KEY = "half_time_sound_activated";
  static const String SHOW_LEFT_TIME_KEY = "show_left_time";

  static const String DATE_FORMAT = "yyyy-MM-dd HH:MM";
  static const String ROUTINES_FOLDER = "routines";
  static const String JSON_ENDING = ".json";

  ///
  /// Regex
  ///
  static const String REGEX_REPS = "^([1-9][0-9]{0,1})";
  static const String REGEX_ONE = "^([0-9]{0,1}[0-9]{0,1})";
  static const String REGEX_TWO = "^([0-5][0-9]{0,1}|[1-9])";
  static const String COLON_SIGN = " : ";

  ///
  /// Overview constants
  ///
  static const double OVERVIEW_SETS_CONTAINER_WIDTH = 44.0;
  static const double OVERVIEW_LINE_HEIGHT = 4.0;
  static const double OVERVIEW_LINE_WIDTH = 36;
  static const double OVERVIEW_BOX_PADDING = 4.0;
  static const double OVERVIEW_TEXT_PADDING = 8.0;
  static const double OVERVIEW_RECTANGLE_WIDTH = 90.0;
  static const double OVERVIEW_RECTANGLE_HEIGHT = 50.0;
  static const double OVERVIEW_RADIUS = 16.0;
  static const double OVERVIEW_FONT_SIZE = 20.0;
  static const double OVERVIEW_FONT_SIZE_SMALL = 13.0;
  static const double OVERVIEW_CONTAINER_BORDER_WIDTH = 1.5;

  ///
  /// Standard values
  ///
  static const double STANDARD_PADDING = 20.0;
  static const double BOTTOM_PADDING = 60.0;
  static const double STANDARD_BORDER_RADIUS = 14.0;

  ///
  /// Time constants
  ///
  static const int SECONDS_PER_MINUTE = 60;
  static const int TIME_MAX_VALUE = 5999;
  static const int TIME_MIN_VALUE = 1;

  ///
  /// Vibration
  ///
  static const int VIBRATION_DURATION = 200;
  static const int VIBRATION_AMPLITUDE = 96;

}
