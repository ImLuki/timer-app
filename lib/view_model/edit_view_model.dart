import 'package:flutter/cupertino.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:timer_app/model/data_store.dart';
import 'package:timer_app/model/routine_repository.dart';
import 'package:timer_app/model/routines/parent_routines/advanced_routine.dart';
import 'package:timer_app/model/routines/parent_routines/simple_routine.dart';
import 'package:timer_app/model/routines/sets_routine.dart';
import 'package:timer_app/resources/constants.dart';

class EditViewModel {
  late SimpleRoutine simpleRoutine;
  late AdvancedRoutine advancedRoutine;
  final RoutineRepository _routineRepository = DataStore().routineRepository;
  bool _advanced = false;
  bool hasBeenChanged = false;

  EditViewModel({
    required bool advanced,
    SimpleRoutine? simpleRoutine,
    AdvancedRoutine? advancedRoutine,
  }) {
    this._advanced = advanced;
    this.simpleRoutine = simpleRoutine?.clone(id: simpleRoutine.id) ?? SimpleRoutine();
    this.advancedRoutine = advancedRoutine?.clone(id: advancedRoutine.id) ?? AdvancedRoutine();
    if (advanced) hasBeenChanged = true;
  }

  Future<bool> saveRoutine(BuildContext context) async {
    if (this.advanced) {
      if (this.advancedRoutine.repetitionRoutine.children.isEmpty) return false;

      if (advancedRoutine.name == "") {
        advancedRoutine.name ==
            "${AppLocalizations.of(context)!.saved} "
                "${DateFormat(Constants.DATE_FORMAT).format(advancedRoutine.dateTime)}";
      }

      await _routineRepository.addRoutine(this.advancedRoutine);
    } else {
      if (simpleRoutine.name == "") {
        simpleRoutine.name ==
            "${AppLocalizations.of(context)!.saved} "
                "${DateFormat(Constants.DATE_FORMAT).format(simpleRoutine.dateTime)}";
      }
      await _routineRepository.addRoutine(this.simpleRoutine);
    }
    return true;
  }

  void addSet() {
    if (this.advanced) {
      advancedRoutine.repetitionRoutine.children.add(SetsRoutine.standard());
    } else {}
  }

  void deleteSet(SetsRoutine setsRoutine) {
    if (this.advanced) {
      advancedRoutine.repetitionRoutine.children.remove(setsRoutine);
    } else {}
  }

  void duplicate(SetsRoutine setsRoutine) {
    if (this.advanced) {
      advancedRoutine.repetitionRoutine.children.insert(
        advancedRoutine.repetitionRoutine.children.indexOf(setsRoutine) + 1,
        setsRoutine.clone(),
      );
    }
  }

  bool get advanced => _advanced;

  set advanced(bool value) {
    this.hasBeenChanged = true;
    _advanced = value;
  }
}
