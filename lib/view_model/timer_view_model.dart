import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_foreground_task/flutter_foreground_task.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:timer_app/model/data_store.dart';
import 'package:timer_app/model/routines/abstract/time_routine.dart';
import 'package:timer_app/model/routines/parent_routines/starter_routine.dart';
import 'package:timer_app/model/routines/repetition_routine.dart';
import 'package:timer_app/model/routines/sets_routine.dart';
import 'package:timer_app/model/routines/util/util.dart';
import 'package:timer_app/resources/constants.dart';
import 'package:vibration/vibration.dart';
import 'package:wakelock_plus/wakelock_plus.dart';

enum TimerState { PREPARATION, RUNNING, PAUSED, FINISHED, CANCELED }

class TimerViewModel {
  static const String TIMER_HANDLER_STATE_KEY = "timer_handler_state_key";

  final StarterRoutine _routine;
  final ValueNotifier<int> _totalTimeRemaining = ValueNotifier<int>(0);

  Function? onNewRoutine;
  Function? onFinished;

  int _currentIndex = 0;
  int _currentRepIndex = 0;
  int _set = 0;
  int _repetition = 0;
  TimerState _timerState = TimerState.PREPARATION;
  int timeRemaining = 0;
  Timer? timer;
  bool isInBackground = false;
  int trainingsTime = 0;

  TimerViewModel(this._routine, {this.onNewRoutine, this.onFinished}) {
    _currentRepIndex = 0;
    _currentIndex = 0;
    _set = _routine.repetitionRoutine.children.first.sets;
    _repetition = _routine.repetitionRoutine.repetitions;
    _timerState = _routine.repetitionRoutine.preparationTime > 0 ? TimerState.PREPARATION : TimerState.RUNNING;
    timeRemaining = _routine.repetitionRoutine.preparationTime > 0
        ? _routine.repetitionRoutine.preparationTime
        : currentRoutine.time;

    _totalTimeRemaining.value = _routine.getTimeInSeconds();
    WakelockPlus.enable();
  }

  void start(BuildContext context) {
    trainingsTime = 0;
    _createTimer(context);
  }

  void jumpToNext(BuildContext context) {
    _next(context);
    if (isRunning()) _createTimer(context);
  }

  void _next(BuildContext context) {
    if (_calculateNextRoutine()) {
      _finish(context);
    } else {
      timeRemaining = currentRoutine.time;
      _totalTimeRemaining.value = _routine.getTimeRemainingInSeconds(
        prep: _timerState == TimerState.PREPARATION,
        currentRepetition: repetitionRoutine.repetitions - repetition,
        currentRepIndex: _currentRepIndex,
        sets: setRoutine.sets - _set,
        currentIndex: _currentIndex,
      );
      onNewRoutine?.call();
      _playSound();
      _vibrate();
      _updateNotification(context);
    }
  }

  /// Calculate indices for next training
  ///
  /// returns true if routine finished, false otherwise
  bool _calculateNextRoutine() {
    if (_timerState == TimerState.PREPARATION) {
      _timerState = TimerState.RUNNING;
      _currentIndex = -1;
    }

    _currentIndex++;
    if (_currentIndex >= setRoutine.children.length) {
      _set--;
      if (_set <= 0) {
        _currentRepIndex++;
        if (_currentRepIndex >= _routine.repetitionRoutine.children.length) {
          _repetition--;
          if (_repetition <= 0) {
            return true;
          }
          this._currentRepIndex = 0;
        }
        this._set = setRoutine.sets;
      }
      this._currentIndex = 0;
    }

    // slip exercises which are skipped in last set
    if (this.currentRoutine.getTimeInSeconds(lastSet: isLastSet(), shouldSkipOnDefault: shouldSkipOnDefault()) <= 0) {
      return _calculateNextRoutine();
    }

    return false;
  }

  /// Calculate indices for previous training step
  void _calculatePrevRoutine() {
    if (_timerState == TimerState.PREPARATION) {
      return;
    }

    // if first exercise
    if (_currentIndex <= 0 &&
        _set >= setRoutine.sets &&
        _currentRepIndex <= 0 &&
        _repetition >= _routine.repetitionRoutine.repetitions) {
      return;
    }

    if (_currentIndex <= 0) {
      if (_set >= setRoutine.sets) {
        if (_currentRepIndex <= 0) {
          if (_repetition >= _routine.repetitionRoutine.repetitions) {
            return;
          }
          _repetition++;
          this._currentRepIndex = _routine.repetitionRoutine.children.length - 1;
          this._set = 1;
          this._currentIndex = setRoutine.children.length - 1;
          return;
        }
        _currentRepIndex--;
        this._set = 1;
        this._currentIndex = setRoutine.children.length - 1;
        return;
      }
      _set++;
      this._currentIndex = setRoutine.children.length - 1;
      return;
    }
    _currentIndex--;
  }

  void togglePlayPause(BuildContext context, {Duration? explicitTime}) {
    if (_timerState == TimerState.PAUSED) {
      play(context, explicitTime: explicitTime);
    } else {
      _pause();
    }
  }

  void _pause() {
    _timerState = TimerState.PAUSED;
    timer?.cancel();
  }

  Future<void> play(BuildContext context, {Duration? explicitTime}) async {
    _timerState = TimerState.RUNNING;
    if (explicitTime == null) {
      _createTimer(context);
    } else {
      int milliseconds = explicitTime.inMilliseconds - (explicitTime.inSeconds * 1000);
      Future.delayed(Duration(milliseconds: milliseconds)).then((_) {
        if (_timerState != TimerState.RUNNING) return;
        timeRemaining--;
        _totalTimeRemaining.value--;
        trainingsTime++;
        if (context.mounted) _createTimer(context);
      });
    }
  }

  void cancel() {
    _timerState = TimerState.CANCELED;
    DataStore().soundHandler.stopSound();
    timer?.cancel();
    WakelockPlus.disable();
  }

  Future<void> moveToBackgroundTask(BuildContext context) async {
    debugPrint("Moved task to background");
    isInBackground = true;
    _updateNotification(context);

    /// Do not cancel timer, because this will run in foreground service
    /// Not necessary anymore because of foreground-service
    //SharedPreferences prefs = await SharedPreferences.getInstance();
    //prefs.setString(TIMER_HANDLER_STATE_KEY, DateTime.now().toIso8601String());
  }

  Future<void> restoreState() async {
    isInBackground = false;

    debugPrint("Moved task to foreground");
  }

  void prev(BuildContext context, int seconds) {
    if (timeRemaining >= currentRoutine.time - 1) {
      _calculatePrevRoutine();

      // slip exercises which are skipped in last set
      if (this.currentRoutine.getTimeInSeconds(lastSet: isLastSet(), shouldSkipOnDefault: shouldSkipOnDefault()) <= 0) {
        _calculatePrevRoutine();
      }
    }

    timeRemaining = currentRoutine.time;
    _totalTimeRemaining.value = _routine.getTimeRemainingInSeconds(
            prep: _timerState == TimerState.PREPARATION,
            currentRepetition: repetitionRoutine.repetitions - repetition,
            currentRepIndex: _currentRepIndex,
            sets: setRoutine.sets - _set,
            currentIndex: _currentIndex) +
        currentRoutine.time -
        timeRemaining;

    if (_timerState != TimerState.PAUSED) {
      _timerState = TimerState.RUNNING;
      _playSound();
      _vibrate();
      _createTimer(context);
    } else {
      // update notification if PAUSED, otherwise 'updateTimer()' will do that
      _updateNotification(context);
    }
  }

  void _createTimer(BuildContext context) {
    timer?.cancel();

    //play sound
    _playCountDownSound();

    //update Notification
    _updateNotification(context);

    timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      timeRemaining--;
      _totalTimeRemaining.value--;
      trainingsTime++;

      //play sound
      _playCountDownSound();

      // play Halftime sound
      _playHalfTimeSound(context);

      //update Notification
      _updateNotification(context);

      if (timeRemaining <= 0) {
        _next(context);
      }
    });
  }

  void _updateNotification(BuildContext context) {
    if (isInBackground) {
      FlutterForegroundTask.updateService(
        notificationTitle: getNotificationTitle(context),
        notificationText: getNotificationBody(context),
      );
    }
  }

  void _playCountDownSound() {
    if (!DataStore().settingModel.soundEnabled) return;
    if (timeRemaining <= 0) return;
    if (timeRemaining <= DataStore().settingModel.amountCountingSounds) {
      DataStore().soundHandler.playShortSound(DataStore().settingModel.countDownSound, timeRemaining.toString());
    }
  }

  void _playHalfTimeSound(BuildContext context) {
    if (!DataStore().settingModel.soundEnabled) return;

    if (currentRoutine.hasHalfWaySound == 0) return;
    if (currentRoutine.hasHalfWaySound == -1 && !DataStore().settingModel.halftimeSoundActivated) return;

    if (timeRemaining == currentRoutine.time ~/ 2) {
      DataStore().soundHandler.playSound(
            DataStore().settingModel.halfTimeSound,
            AppLocalizations.of(context)!.half_time_tts,
          );
    }

    if (timeRemaining <= 0) return;
    if (timeRemaining <= DataStore().settingModel.amountCountingSounds) {}
  }

  void _playSound() async {
    if (DataStore().settingModel.soundEnabled) {
      DataStore().soundHandler.playSound(currentRoutine.sound, currentRoutine.infoText);
    }
  }

  void _vibrate() async {
    if (DataStore().settingModel.vibrate) {
      Vibration.vibrate(
        duration: Constants.VIBRATION_DURATION,
        amplitude: Constants.VIBRATION_AMPLITUDE,
      );
    }
  }

  void _finish(BuildContext context) {
    _timerState = TimerState.FINISHED;
    timer?.cancel();
    onFinished?.call();

    // vibration
    if (DataStore().settingModel.vibrate) {
      Vibration.vibrate(
        pattern: [0, 200, 200, 200, 200, 200],
        amplitude: Constants.VIBRATION_AMPLITUDE,
      );
    }

    // sounds
    if (DataStore().settingModel.soundEnabled) {
      DataStore().soundHandler.playSound(
            DataStore().settingModel.finishSound,
            AppLocalizations.of(context)!.finished,
          );
    }

    // notifications
    if (isInBackground) _updateNotification(context);
  }

  RepetitionRoutine get repetitionRoutine => _routine.repetitionRoutine;

  SetsRoutine get setRoutine => repetitionRoutine.children[_currentRepIndex];

  TimeRoutine get currentRoutine => setRoutine.children[_currentIndex];

  int get set => _set;

  int get repetition => _repetition;

  StarterRoutine get routine => _routine;

  ValueNotifier<int> get totalTimeRemaining => _totalTimeRemaining;

  bool isLastSet() => this._set <= 1;

  bool shouldSkipOnDefault() =>
      this._set <= 1 &&
      this.setRoutine.sets > 1 &&
      this.setRoutine.children.length > 1 &&
      _currentIndex == this.setRoutine.children.length - 1;

  bool isPreparation() => _timerState == TimerState.PREPARATION;

  bool isRunning() => _timerState == TimerState.RUNNING || _timerState == TimerState.PREPARATION;

  int getCurrentRoutineDuration() => isPreparation() ? repetitionRoutine.preparationTime : currentRoutine.time;

  String getNotificationTitle(BuildContext context) {
    switch (_timerState) {
      case TimerState.PREPARATION:
        return AppLocalizations.of(context)!.timer_app_preparation_key;
      case TimerState.RUNNING:
        return currentRoutine.infoText;
      case TimerState.PAUSED:
        return "${AppLocalizations.of(context)!.paused} - ${currentRoutine.infoText}";
      case TimerState.FINISHED:
        return AppLocalizations.of(context)!.finished;
      case TimerState.CANCELED:
        return AppLocalizations.of(context)!.training_canceled;
    }
  }

  String getNotificationBody(BuildContext context) {
    switch (_timerState) {
      case TimerState.PREPARATION:
      case TimerState.RUNNING:
      case TimerState.PAUSED:
        return _getBody(context, timeRemaining);
      case TimerState.FINISHED:
      case TimerState.CANCELED:
        return "${AppLocalizations.of(context)!.total_training_time} ${routine.getTotalTimeString()}";
    }
  }

  String _getBody(BuildContext context, int time) {
    return "${AppLocalizations.of(context)!.time_remaining}: ${Util.getTimeString(time)}";
  }
}
