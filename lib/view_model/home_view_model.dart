import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:share_plus/share_plus.dart';
import 'package:sorted_list/sorted_list.dart';
import 'package:timer_app/model/data_access_object.dart';
import 'package:timer_app/model/data_store.dart';
import 'package:timer_app/model/routine_repository.dart';
import 'package:timer_app/model/routines/parent_routines/starter_routine.dart';
import 'package:timer_app/model/util/data_writer.dart';

class HomeViewModel with ChangeNotifier {
  final ScrollController scrollController = ScrollController();

  final SortedList<StarterRoutine> _routines = SortedList((a, b) => a.index - b.index);
  final RoutineRepository _routineRepository = DataStore().routineRepository;

  late Function() _listener;

  List<StarterRoutine> _filteredRoutines = [];
  final TextEditingController textController = TextEditingController();

  HomeViewModel() {
    this._routines.clear();
    this._routines.addAll(_routineRepository.getAll());
    this._filteredRoutines = _routines;
    this._listener = () {
      this._routines.clear();
      this._routines.addAll(_routineRepository.getAll());
      if (this.textController.text == "") {
        this._filteredRoutines = _routines;
      } else {
        this._updateFilteredRoutines();
      }

      notifyListeners();
    };
    this._routineRepository.addListener(this._listener);
  }

  void scrollToTop() {
    scrollController.animateTo(0, duration: const Duration(milliseconds: 500), curve: Curves.ease);
  }

  void deleteRoutine({required StarterRoutine routine}) {
    _routineRepository.removeRoutine(routine);
  }

  List<StarterRoutine> get filteredRoutines => _filteredRoutines;

  void onSearchTextChanged() {
    this._updateFilteredRoutines();
    notifyListeners();
  }

  void _updateFilteredRoutines() {
    this._filteredRoutines = _routines.where((routine) {
      return routine.name.toLowerCase().contains(this.textController.text.toLowerCase());
    }).toList();
  }

  dynamic duplicate({required StarterRoutine routine}) {
    StarterRoutine newRoutine = routine.clone();
    newRoutine.name += " (copy)";
    return newRoutine;
  }

  void shareFile(StarterRoutine starterRoutine) async {
    Map map = starterRoutine.toMap();
    map.remove(DataAccessObject.DB_ID_KEY);
    DataWriter dataWriter = DataWriter(
      content: jsonEncode(map),
      fileName: "${starterRoutine.name}-interval-timer-routine",
    );
    File file = await dataWriter.localFile;
    await dataWriter.write();
    await Share.shareXFiles([XFile(file.path)]);
    dataWriter.delete();
  }

  void onReorder(int oldIndex, int newIndex) {
    final StarterRoutine item = _filteredRoutines[oldIndex];
    if (newIndex >= this._routines.length) {
      item.index = this._routines.length;
    } else {
      this._routines.getRange(newIndex, this._routines.length - 1).forEach((element) => element.index++);
      item.index = newIndex;
    }

    this._routines.sort((a, b) => a.index - b.index);
    for (int i = 0; i < this._routines.length; i++) {
      this._routines[i].index = i;
    } //update indices
    this._filteredRoutines = this._routines;

    this._routineRepository.updateIndices();

    notifyListeners();
  }

  bool isSearching() {
    return textController.text != "";
  }

  String get searchText => textController.text;
}
