import 'package:flutter/material.dart';

abstract class AppColors {
  static const Color primary = Color.fromRGBO(86, 86, 86, 1.0);

  //static const Color background = Colors.white;

  static const Color secondary = Color.fromRGBO(249, 170, 51, 1.0);
  static const Color restColor = Colors.blueAccent;
  static const Color prepColor = Colors.lightGreen;

  static const List<Color> defaultColors = [
    AppColors.secondary,
    Colors.red,
    Colors.pink,
    Colors.purple,
    Colors.deepPurple,
    Colors.indigo,
    Colors.blueAccent,
    Colors.lightBlue,
    Colors.cyan,
    Colors.teal,
    Colors.green,
    Colors.lightGreen,
    Colors.lime,
    Colors.yellow,
    secondary,
    Colors.orange,
    Colors.deepOrange,
    Colors.brown,
    Colors.grey,
    Colors.blueGrey,
  ];
}
