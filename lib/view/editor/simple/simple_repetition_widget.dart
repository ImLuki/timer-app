import 'package:flutter/material.dart';
import 'package:timer_app/model/routines/sets_routine.dart';
import 'package:timer_app/resources/constants.dart';
import 'package:timer_app/theme/app_colors.dart';
import 'package:timer_app/view/util/number_picker.dart';

class SimpleRepetitionWidget extends StatelessWidget {
  const SimpleRepetitionWidget({
    super.key,
    this.setsRoutine,
    required this.onChanged,
    required this.widgetName,
  });

  static const double MARGIN = 25.0;
  static const double VERTICAL_PADDING = 10.0;
  static const double HORIZONTAL_PADDING = 50.0;
  static const double FONT_SIZE = 18.0;
  static const double WORD_SPACING = 5.0;
  static const double LETTER_SPACING = 3.0;

  final String widgetName;

  final SetsRoutine? setsRoutine;
  final Function(int) onChanged;

  @override
  Widget build(BuildContext context) {
    return _getContainer();
  }

  Widget _getContainer() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: VERTICAL_PADDING, horizontal: HORIZONTAL_PADDING),
      margin: const EdgeInsets.only(
        left: MARGIN,
        right: MARGIN,
        bottom: MARGIN,
      ),
      decoration: const BoxDecoration(
        color: AppColors.primary,
        borderRadius: BorderRadius.all(
          Radius.circular(Constants.STANDARD_BORDER_RADIUS),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            widgetName,
            style: const TextStyle(
              fontSize: FONT_SIZE,
              color: Colors.white60,
              wordSpacing: WORD_SPACING,
              letterSpacing: LETTER_SPACING,
            ),
          ),
          ...[getContent()],
        ],
      ),
    );
  }

  Widget getContent() {
    return NumberPicker(
      initValue: this.setsRoutine!.sets,
      onChange: this.onChanged,
      padding: 10.0,
      numberTextFieldWidth: 120.0,
    );
  }
}
