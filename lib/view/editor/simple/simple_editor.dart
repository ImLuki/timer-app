import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:timer_app/resources/constants.dart';
import 'package:timer_app/theme/app_colors.dart';
import 'package:timer_app/view/editor/simple/simple_repetition_widget.dart';
import 'package:timer_app/view/editor/simple/simple_training_widget.dart';
import 'package:timer_app/view/editor/total_time_widget.dart';
import 'package:timer_app/view/timer_screen/timer_screen.dart';
import 'package:timer_app/view/util/custom_page_route.dart';
import 'package:timer_app/view_model/edit_view_model.dart';

class SimpleEditor extends StatefulWidget {
  final EditViewModel editViewModel;

  const SimpleEditor({super.key, required this.editViewModel});

  @override
  _SimpleEditorState createState() => _SimpleEditorState();
}

class _SimpleEditorState extends State<SimpleEditor> {
  static const double TEXT_FIELD_PADDING_HORIZONTAL = 50.0;
  static const double TEXT_FIELD_PADDING_VERTICAL = 10.0;

  final TextEditingController _nameEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();

    _nameEditingController.text = widget.editViewModel.simpleRoutine.name;
  }

  @override
  void dispose() {
    _nameEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          const SizedBox(height: Constants.STANDARD_PADDING),
          _getTextField(),
          const SizedBox(height: Constants.STANDARD_PADDING),
          TotalTimeWidget(completeTime: widget.editViewModel.simpleRoutine.getTimeInSeconds()),
          const SizedBox(height: Constants.STANDARD_PADDING * 2),
          _getWorkoutMaker(),
          const SizedBox(height: 24.0),
          _quickstartWidget(),
          const SizedBox(height: 25.0),
        ],
      ),
    );
  }

  Widget _getWorkoutMaker() {
    return Column(
      children: [
        SimpleRepetitionWidget(
          widgetName: AppLocalizations.of(context)!.set(2).toUpperCase(),
          setsRoutine: widget.editViewModel.simpleRoutine.setsRoutine,
          onChanged: (newValue) => setState(() {
            widget.editViewModel.hasBeenChanged = true;
            widget.editViewModel.simpleRoutine.setSets(newValue);
          }),
        ),
        SimpleTrainingWidget(
          timeRoutine: widget.editViewModel.simpleRoutine.training,
          widgetName: AppLocalizations.of(context)!.timer_app_training_key.toUpperCase(),
          timeMinValue: 0,
          onChanged: (val) => setState(() {
            widget.editViewModel.hasBeenChanged = true;
            widget.editViewModel.simpleRoutine.setTrainingTime(val);
          }),
        ),
        SimpleTrainingWidget(
          timeRoutine: widget.editViewModel.simpleRoutine.rest,
          widgetName: AppLocalizations.of(context)!.timer_app_rest_key.toUpperCase(),
          timeMinValue: 0,
          onChanged: (val) => setState(() {
            widget.editViewModel.hasBeenChanged = true;
            widget.editViewModel.simpleRoutine.setRestTime(val);
          }),
        ),
      ],
    );
  }

  Widget _getTextField() {
    return Container(
      padding: const EdgeInsets.symmetric(
        vertical: TEXT_FIELD_PADDING_VERTICAL,
        horizontal: TEXT_FIELD_PADDING_HORIZONTAL,
      ),
      child: TextField(
        keyboardType: TextInputType.name,
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: AppColors.secondary),
          ),
          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: AppColors.secondary),
          ),
          labelText: AppLocalizations.of(context)!.name,
          floatingLabelStyle: const TextStyle(color: AppColors.secondary),
          alignLabelWithHint: true,
        ),
        cursorColor: Colors.white,
        controller: _nameEditingController,
        onChanged: (value) => setState(() {
          widget.editViewModel.hasBeenChanged = true;
          widget.editViewModel.simpleRoutine.name = value.trim();
        }),
      ),
    );
  }

  Widget _quickstartWidget() {
    return Padding(
      padding: const EdgeInsets.only(
        left: 35.0,
        right: 35.0,
      ),
      child: InkWell(
        splashColor: Colors.yellow.withOpacity(0.4),
        highlightColor: Colors.yellow.withOpacity(0.4),
        hoverColor: Colors.yellow.withOpacity(0.4),
        borderRadius: const BorderRadius.all(
          Radius.circular(Constants.STANDARD_BORDER_RADIUS),
        ),
        onTap: _quickstart,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 35.0),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.yellow),
            borderRadius: const BorderRadius.all(
              Radius.circular(Constants.STANDARD_BORDER_RADIUS),
            ),
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              const Icon(Icons.flash_on, color: Colors.yellow),
              const SizedBox(width: 8.0),
              Flexible(
                child: Text(
                  AppLocalizations.of(context)!.quickstart,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 19.0,
                    color: Colors.white.withOpacity(0.85),
                    wordSpacing: 5.0,
                    letterSpacing: 3.0,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _quickstart() {
    Navigator.push(
      context,
      CustomPageRoute.build(
        builder: (BuildContext context) => TimerScreen(routine: widget.editViewModel.simpleRoutine),
      ),
    );
  }
}
