import 'package:flutter/material.dart';
import 'package:timer_app/model/routines/abstract/time_routine.dart';
import 'package:timer_app/resources/constants.dart';
import 'package:timer_app/view/editor/simple/simple_repetition_widget.dart';
import 'package:timer_app/view/util/time_picker.dart';

class SimpleTrainingWidget extends SimpleRepetitionWidget {
  final TimeRoutine timeRoutine;
  final int timeMaxValue;
  final int timeMinValue;

  const SimpleTrainingWidget({
    super.key,
    required this.timeRoutine,
    required super.onChanged,
    required super.widgetName,
    this.timeMaxValue = Constants.TIME_MAX_VALUE,
    this.timeMinValue = Constants.TIME_MIN_VALUE,
  });

  @override
  Widget getContent() {
    return TimePicker(
      initValue: this.timeRoutine.getTimeInSeconds(),
      onChange: this.onChanged,
      padding: 10.0,
      numberTextFieldWidth: 50.0,
      timeMaxValue: timeMaxValue,
      timeMinValue: timeMinValue,
    );
  }
}
