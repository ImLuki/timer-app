import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:timer_app/model/routines/sets_routine.dart';
import 'package:timer_app/resources/constants.dart';
import 'package:timer_app/theme/app_colors.dart';
import 'package:timer_app/view/dialogs/confirm_dialog.dart';
import 'package:timer_app/view/editor/advanced/advanced_repetition_header.dart';
import 'package:timer_app/view/editor/advanced/advanced_set_container.dart';
import 'package:timer_app/view_model/edit_view_model.dart';

class AdvancedEditor extends StatefulWidget {
  final EditViewModel editViewModel;

  const AdvancedEditor({super.key, required this.editViewModel});

  @override
  _AdvancedEditorState createState() => _AdvancedEditorState();
}

class _AdvancedEditorState extends State<AdvancedEditor> {
  static const int SNACK_BAR_DURATION = 1500;
  static const double VERTICAL_MARGIN = 12.0;
  static const double HORIZONTAL_MARGIN = 8.0;
  static const double VERTICAL_PADDING = 10.0;
  static const double BUTTON_PADDING = 8.0;
  static const double ICON_SIZE = 32.0;

  int keyExtension = 0; // used to update widgets on duplicate

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          AdvancedRepetitionHeader(advancedRoutine: widget.editViewModel.advancedRoutine),
          ...List.generate(
            widget.editViewModel.advancedRoutine.repetitionRoutine.children.length,
            (index) {
              return AdvancedSetContainer(
                key: Key("$index $keyExtension"),
                setsRoutine: widget.editViewModel.advancedRoutine.repetitionRoutine.children[index],
                index: index + 1,
                totalSets: widget.editViewModel.advancedRoutine.repetitionRoutine.children.length,
                onUpdate: () => setState(() {}),
                onDelete: () => deleteSet(widget.editViewModel.advancedRoutine.repetitionRoutine.children[index]),
                onDuplicate: () => duplicate(widget.editViewModel.advancedRoutine.repetitionRoutine.children[index]),
                moveDown: () => moveDown(index),
                moveUp: () => moveUp(index),
              );
            },
          ),
          _addSetWidget(),
          const SizedBox(height: Constants.BOTTOM_PADDING),
        ],
      ),
    );
  }

  Widget _addSetWidget() {
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.only(
        left: VERTICAL_MARGIN,
        right: VERTICAL_MARGIN,
        bottom: HORIZONTAL_MARGIN,
      ),
      padding: const EdgeInsets.symmetric(horizontal: Constants.STANDARD_PADDING, vertical: VERTICAL_PADDING),
      decoration: const BoxDecoration(
        color: AppColors.primary,
        borderRadius: BorderRadius.all(
          Radius.circular(Constants.STANDARD_BORDER_RADIUS),
        ),
      ),
      child: ElevatedButton(
        onPressed: () => setState(() => widget.editViewModel.addSet()),
        style: ElevatedButton.styleFrom(
          shape: const CircleBorder(),
          padding: const EdgeInsets.all(BUTTON_PADDING),
          backgroundColor: Colors.white,
        ),
        child: const Icon(Icons.add, size: ICON_SIZE),
      ),
    );
  }

  void deleteSet(SetsRoutine setsRoutine) async {
    bool? result = await ConfirmDialog().deleteConfirm(context);

    if (mounted) {
      ScaffoldMessenger.of(context).removeCurrentSnackBar();
    }
    if (result != null && result) {
      setState(() {
        widget.editViewModel.deleteSet(setsRoutine);
      });
      if (mounted) {
        ScaffoldMessenger.of(context)
          ..removeCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              content: Text(AppLocalizations.of(context)!.deleted_entry),
              duration: const Duration(milliseconds: SNACK_BAR_DURATION),
              behavior: SnackBarBehavior.floating,
            ),
          );
      }
    } else {
      if (mounted) {
        ScaffoldMessenger.of(context)
          ..removeCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              content: Text(AppLocalizations.of(context)!.delete_canceled),
              duration: const Duration(milliseconds: SNACK_BAR_DURATION),
              behavior: SnackBarBehavior.floating,
            ),
          );
      }
    }
  }

  void duplicate(SetsRoutine setsRoutine) {
    setState(() {
      this.keyExtension++;
      widget.editViewModel.duplicate(setsRoutine);
    });
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Text(AppLocalizations.of(context)!.duplicated_entry),
          duration: const Duration(milliseconds: SNACK_BAR_DURATION),
          behavior: SnackBarBehavior.floating,
        ),
      );
  }

  void moveUp(index) {
    if (index == 0) return;
    widget.editViewModel.advancedRoutine.repetitionRoutine.children.insert(
      index - 1,
      widget.editViewModel.advancedRoutine.repetitionRoutine.children.removeAt(index),
    );
    setState(() => this.keyExtension++);
  }

  void moveDown(index) {
    if (index >= widget.editViewModel.advancedRoutine.repetitionRoutine.children.length - 1) return;

    widget.editViewModel.advancedRoutine.repetitionRoutine.children.insert(
      index + 1,
      widget.editViewModel.advancedRoutine.repetitionRoutine.children.removeAt(index),
    );
    setState(() => this.keyExtension++);
  }
}
