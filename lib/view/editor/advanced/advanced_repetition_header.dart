import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:timer_app/model/routines/parent_routines/advanced_routine.dart';
import 'package:timer_app/model/routines/util/util.dart';
import 'package:timer_app/theme/app_colors.dart';
import 'package:timer_app/view/editor/total_time_widget.dart';
import 'package:timer_app/view/util/number_picker.dart';
import 'package:timer_app/view/util/time_picker.dart';

class AdvancedRepetitionHeader extends StatefulWidget {
  const AdvancedRepetitionHeader({super.key, required this.advancedRoutine});

  final AdvancedRoutine advancedRoutine;

  @override
  _AdvancedRepetitionHeaderState createState() => _AdvancedRepetitionHeaderState(advancedRoutine);
}

class _AdvancedRepetitionHeaderState extends State<AdvancedRepetitionHeader> {
  static const double verticalMargin = 30.0;
  static const double topMargin = 14.0;
  static const double bottomMargin = 20.0;
  static const double elementPadding = 12.0;

  static const double timeWidgetFontSize = 32.0;
  static const double timeWidgetHeadingFontSize = 16.0;
  static const double timeWidgetColonFontSize = 26.0;
  static const double timeWidgetColonShift = 4.0;

  static const double textFieldFontSize = 14.0;
  static const double textFieldContentPadding = 12.0;
  static const double textFieldBorderWidth = 2.0;

  final TextEditingController _nameEditingController = TextEditingController();
  final AdvancedRoutine _advancedRoutine;

  _AdvancedRepetitionHeaderState(this._advancedRoutine);

  @override
  void initState() {
    super.initState();

    this._nameEditingController.text = _advancedRoutine.name;
  }

  @override
  void dispose() {
    this._nameEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(
        top: topMargin,
        bottom: bottomMargin,
        left: verticalMargin,
        right: verticalMargin,
      ),
      child: Column(
        children: [
          TotalTimeWidget(
            completeTime: _advancedRoutine.getTimeInSeconds(),
            timeFontSize: timeWidgetFontSize,
            headingFontSize: timeWidgetHeadingFontSize,
            colonFontSize: timeWidgetColonFontSize,
            colonShift: timeWidgetColonShift,
          ),
          const SizedBox(height: elementPadding),
          _getTextField(context),
          const SizedBox(height: 20.0),
          _repsAndPrepTime(),
        ],
      ),
    );
  }

  Widget _getTextField(BuildContext context) {
    return TextField(
      style: const TextStyle(fontSize: textFieldFontSize),
      keyboardType: TextInputType.name,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.symmetric(
          vertical: textFieldContentPadding / 2,
          horizontal: textFieldContentPadding,
        ),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(width: textFieldBorderWidth, color: AppColors.secondary),
        ),
        enabledBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: AppColors.secondary),
        ),
        labelText: AppLocalizations.of(context)!.name,
        floatingLabelStyle: const TextStyle(color: AppColors.secondary),
        alignLabelWithHint: true,
      ),
      cursorColor: Colors.white,
      controller: _nameEditingController,
      onChanged: (value) => _advancedRoutine.name = value.trim(),
    );
  }

  Widget _repsAndPrepTime() {
    return IntrinsicHeight(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _repetitionsWidget(),
          const SizedBox(width: 12.0),
          _prepWidget(),
        ],
      ),
    );
  }

  Widget _widgetContainer({required String title, required String text, required Function()? onTap}) {
    return Expanded(
      child: InkWell(
        borderRadius: const BorderRadius.all(Radius.circular(20)),
        onTap: onTap,
        child: Container(
          padding: const EdgeInsets.all(12.0),
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.white70,
            ),
            borderRadius: const BorderRadius.all(Radius.circular(20)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                title,
                overflow: TextOverflow.clip,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 16.0,
                  color: Colors.white60,
                  letterSpacing: 1.5,
                ),
              ),
              const SizedBox(height: 6.0),
              Text(
                text,
                style: const TextStyle(
                  fontSize: 22.0,
                  color: Colors.white,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> showEditorDialog({required BuildContext context, required String title, required Widget body}) {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: Center(child: Text(title)),
          contentPadding: const EdgeInsets.all(16.0),
          children: [
            body,
            const SizedBox(height: 14.0),
            _okButton(context),
          ],
        );
      },
    );
  }

  Widget _okButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40.0),
      child: TextButton(
        style: TextButton.styleFrom(
          foregroundColor: Theme.of(context).colorScheme.secondary,
        ),
        onPressed: () => Navigator.pop(context),
        child: Text(AppLocalizations.of(context)!.ok),
      ),
    );
  }

  Widget _repetitionsWidget() {
    return _widgetContainer(
      title: "${AppLocalizations.of(context)!.repetition(2)}:",
      text: _advancedRoutine.repetitions.toString(),
      onTap: () => showEditorDialog(
        context: context,
        title: AppLocalizations.of(context)!.repetition(2),
        body: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          child: NumberPicker(
            initValue: _advancedRoutine.repetitions,
            onChange: (val) => setState(() => _advancedRoutine.repetitions = val),
            padding: 10.0,
            numberTextFieldWidth: 120.0,
          ),
        ),
      ),
    );
  }

  Widget _prepWidget() {
    return _widgetContainer(
      title: "${AppLocalizations.of(context)!.prep_time}:",
      text: Util.getTimeString(_advancedRoutine.prepTime),
      onTap: () => showEditorDialog(
        context: context,
        title: AppLocalizations.of(context)!.prep_time,
        body: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          child: TimePicker(
            initValue: _advancedRoutine.prepTime,
            onChange: (val) => setState(() => _advancedRoutine.prepTime = val),
            padding: 10.0,
            numberTextFieldWidth: 50.0,
            timeMinValue: 0,
          ),
        ),
      ),
    );
  }
}
