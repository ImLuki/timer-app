import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:timer_app/model/data_store.dart';
import 'package:timer_app/model/routines/abstract/time_routine.dart';
import 'package:timer_app/model/sounds.dart';
import 'package:timer_app/resources/constants.dart';
import 'package:timer_app/view/dialogs/sound_picker_dialog.dart';
import 'package:timer_app/view/util/color_picker.dart';
import 'package:timer_app/view/util/time_picker.dart';

class AdvancedTrainingRoutineWidget extends StatefulWidget {
  const AdvancedTrainingRoutineWidget({
    super.key,
    required this.trainingRoutine,
    this.topBorderRadius = false,
    this.bottomBorderRadius = false,
    this.onUpdate,
    required this.onDelete,
    required this.onDuplicate,
  });

  final TimeRoutine trainingRoutine;
  final bool topBorderRadius;
  final bool bottomBorderRadius;
  final Function? onUpdate;
  final Function onDelete;
  final Function onDuplicate;

  @override
  _AdvancedTrainingRoutineWidgetState createState() => _AdvancedTrainingRoutineWidgetState();
}

class _AdvancedTrainingRoutineWidgetState extends State<AdvancedTrainingRoutineWidget> {
  static const double DELETE_ICON_SIZE = 28.0;
  static const double CONTAINER_PADDING_HORIZONTAL = 18.0;
  static const double CONTAINER_PADDING_TOP = 12.0;
  static const double ELEMENT_PADDING = 8.0;
  static const double TEXT_FIELD_PADDING = 6.0;
  static const double FONT_SIZE = 16.0;

  final TextEditingController _nameEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();
    this._nameEditingController.text = widget.trainingRoutine.infoText;
  }

  @override
  void dispose() {
    this._nameEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(
        left: Constants.STANDARD_PADDING,
        right: Constants.STANDARD_PADDING,
      ),
      padding: const EdgeInsets.only(
        left: CONTAINER_PADDING_HORIZONTAL,
        right: CONTAINER_PADDING_HORIZONTAL,
        top: CONTAINER_PADDING_TOP,
      ),
      alignment: Alignment.center,
      width: double.infinity,
      decoration: BoxDecoration(
        color: widget.trainingRoutine.color,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(widget.topBorderRadius ? Constants.STANDARD_BORDER_RADIUS : 0.0),
          topRight: Radius.circular(widget.topBorderRadius ? Constants.STANDARD_BORDER_RADIUS : 0.0),
          bottomRight: Radius.circular(widget.bottomBorderRadius ? Constants.STANDARD_BORDER_RADIUS : 0.0),
          bottomLeft: Radius.circular(widget.bottomBorderRadius ? Constants.STANDARD_BORDER_RADIUS : 0.0),
        ),
      ),
      child: _body(),
    );
  }

  Widget _body() {
    return Column(
      children: [
        _nameTextField(),
        const SizedBox(height: ELEMENT_PADDING),
        TimePicker(
          initValue: widget.trainingRoutine.time,
          onChange: (val) {
            setState(() {
              widget.trainingRoutine.time = val;
            });
            widget.onUpdate?.call();
          },
          padding: 5.0,
          numberTextFieldWidth: 50.0,
          iconBoxSize: 22.0,
          iconSize: 20.0,
          fontSize: 22.0,
        ),
        Theme(
          data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
          child: ExpansionTile(
            iconColor: Colors.white,
            collapsedIconColor: Colors.white,
            tilePadding: EdgeInsets.zero,
            title: _settings(),
            children: _expandedSettings(),
          ),
        ),
      ],
    );
  }

  Widget _nameTextField() {
    return TextField(
      maxLines: 1,
      decoration: const InputDecoration(
        contentPadding: EdgeInsets.all(TEXT_FIELD_PADDING),
        isDense: true,
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
        ),
      ),
      controller: _nameEditingController,
      onChanged: (value) => widget.trainingRoutine.infoText = value.trim(),
    );
  }

  Widget _settings() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        const SizedBox(width: ELEMENT_PADDING),
        IconButton(
          onPressed: () => widget.onDelete(),
          icon: const Icon(Icons.delete_outline, size: DELETE_ICON_SIZE),
        ),
        const SizedBox(width: ELEMENT_PADDING),
        IconButton(
          onPressed: () => widget.onDuplicate(),
          icon: const Icon(Icons.copy),
        ),
      ],
    );
  }

  List<Widget> _expandedSettings() {
    return [
      _colorPicker(),
      _changeSound(),
      _skipOnLastSetOption(),
      _hasHalfTimeSoundOption(),
      const SizedBox(height: Constants.STANDARD_PADDING),
    ];
  }

  Widget _colorPicker() {
    return Row(
      children: [
        Text(
          AppLocalizations.of(context)!.change_color,
          style: const TextStyle(fontSize: FONT_SIZE),
        ),
        const Spacer(),
        IconButton(
          onPressed: () => _onColorPick(
            currentColor: widget.trainingRoutine.color,
            onColorChanged: (color) {
              setState(() => widget.trainingRoutine.color = color);
            },
          ),
          icon: const Icon(Icons.colorize, size: 24.0),
        ),
      ],
    );
  }

  Widget _skipOnLastSetOption() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: Text(
            AppLocalizations.of(context)!.skip_on_last_set,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(fontSize: FONT_SIZE),
          ),
        ),
        widget.trainingRoutine.skipInLastSet == -1
            ? _defaultButton(onChanged: () => _onSkipInLastSetChanged(1))
            : Switch(
                inactiveTrackColor: Colors.grey,
                activeColor: Colors.tealAccent,
                value: widget.trainingRoutine.skipInLastSet != 0,
                onChanged: (val) => _onSkipInLastSetChanged(val ? 1 : 0),
              ),
      ],
    );
  }

  Widget _hasHalfTimeSoundOption() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: Text(
            AppLocalizations.of(context)!.half_time_sound,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(fontSize: FONT_SIZE),
          ),
        ),
        widget.trainingRoutine.hasHalfWaySound == -1
            ? _defaultButton(onChanged: () => _onHalfTimeSoundChanged(1))
            : Switch(
                inactiveTrackColor: Colors.grey,
                activeColor: Colors.tealAccent,
                value: widget.trainingRoutine.hasHalfWaySound != 0,
                onChanged: (val) => _onHalfTimeSoundChanged(val ? 1 : 0),
              ),
      ],
    );
  }

  Widget _defaultButton({required Function()? onChanged}) {
    return IconButton(
      onPressed: onChanged,
      icon: Container(
        height: 28,
        padding: const EdgeInsets.symmetric(horizontal: 6.0),
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(
            Radius.circular(12.0),
          ),
          border: Border.all(
            color: Colors.white,
            width: 1.5,
          ),
        ),
        child: Align(
          alignment: Alignment.center,
          child: Text(
            AppLocalizations.of(context)!.default_word,
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }

  void _onSkipInLastSetChanged(int val) {
    setState(() {
      widget.trainingRoutine.skipInLastSet = val;
    });
    widget.onUpdate?.call();
  }

  void _onHalfTimeSoundChanged(int val) {
    setState(() {
      widget.trainingRoutine.hasHalfWaySound = val;
    });
    widget.onUpdate?.call();
  }

  Widget _changeSound() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: Text(
            AppLocalizations.of(context)!.change_sound,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(fontSize: FONT_SIZE),
          ),
        ),
        Material(
          color: Colors.transparent,
          child: InkWell(
            borderRadius: const BorderRadius.all(Radius.circular(20.0)),
            child: Padding(
              padding: const EdgeInsets.all(4.0).copyWith(left: 12.0),
              child: Row(
                children: [
                  Text(
                    widget.trainingRoutine.sound.name,
                    style: const TextStyle(fontSize: 16),
                  ),
                  const Icon(Icons.arrow_drop_down, color: Colors.white70)
                ],
              ),
            ),
            onTap: () => SoundPickerDialog(
              items: Sound.values.map((e) => e.name).toList(),
              currentValue: widget.trainingRoutine.sound.name,
              title: AppLocalizations.of(context)!.change_sound,
              onChanged: (val) => setState(
                () => widget.trainingRoutine.sound = Sound.values.byName(val),
              ),
              onPlay: (sound) => DataStore().soundHandler.playSound(
                    Sound.values.byName(sound),
                    AppLocalizations.of(context)!.tts_test_sound,
                  ),
            ).showScrollPickerDialog(context),
          ),
        ),
      ],
    );
  }

  void _onColorPick({required Color currentColor, required ValueChanged<Color> onColorChanged}) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (context) {
        return CustomColorPicker(
          currentColor: currentColor,
          onColorChanged: onColorChanged,
          colors: [
            DataStore().settingModel.standardTrainingColor,
            DataStore().settingModel.standardRestColor,
            DataStore().settingModel.standardPreparationColor,
          ]
            ..removeWhere((element) => element.value == currentColor.value)
            ..insert(0, currentColor),
        );
      },
    ).then((_) {
      setState(() {});
    });
  }
}
