import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:timer_app/model/routines/abstract/time_routine.dart';
import 'package:timer_app/model/routines/sets_routine.dart';
import 'package:timer_app/model/routines/training_routine.dart';
import 'package:timer_app/resources/constants.dart';
import 'package:timer_app/theme/app_colors.dart';
import 'package:timer_app/view/dialogs/confirm_dialog.dart';
import 'package:timer_app/view/editor/advanced/advanced_training_routine_widget.dart';
import 'package:timer_app/view/util/number_picker.dart';

class AdvancedSetContainer extends StatefulWidget {
  const AdvancedSetContainer({
    super.key,
    required this.setsRoutine,
    required this.onDelete,
    required this.index,
    required this.totalSets,
    this.onUpdate,
    required this.onDuplicate,
    required this.moveUp,
    required this.moveDown,
  });

  final SetsRoutine setsRoutine;
  final int index;
  final int totalSets;
  final Function? onUpdate;
  final Function onDelete;
  final Function onDuplicate;
  final Function moveUp;
  final Function moveDown;

  @override
  _AdvancedSetContainerState createState() => _AdvancedSetContainerState();
}

class _AdvancedSetContainerState extends State<AdvancedSetContainer> {
  static const double PADDING = 12.0;
  static const int SNACK_BAR_DURATION = 1500;
  static const double ICON_SIZE = 32.0;
  static const double ICON_PADDING = 8.0;

  static const VERTICAL_MARGIN = 12.0;
  static const BOTTOM_MARGIN = 16.0;
  static const HORIZONTAL_PADDING = 10.0;

  static const double SETS_FONT_SIZE = 16.0;
  static const double SETS_LETTER_SPACING = 3.0;

  int keyExtension = 0; // used to update widgets on duplicate

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: VERTICAL_MARGIN, right: VERTICAL_MARGIN, bottom: BOTTOM_MARGIN),
      padding: const EdgeInsets.only(top: HORIZONTAL_PADDING, bottom: HORIZONTAL_PADDING),
      decoration: const BoxDecoration(
        color: AppColors.primary,
        borderRadius: BorderRadius.all(
          Radius.circular(Constants.STANDARD_BORDER_RADIUS),
        ),
      ),
      child: Column(
        children: [
          Stack(
            children: [
              Align(
                alignment: Alignment.centerRight,
                child: _menu(),
              ),
              _setEditingWidget(),
            ],
          ),
          const SizedBox(height: PADDING),
          ...List.generate(
            widget.setsRoutine.children.length,
            (index) => AdvancedTrainingRoutineWidget(
              key: Key("$index $keyExtension"),
              trainingRoutine: widget.setsRoutine.children[index],
              topBorderRadius: index == 0,
              bottomBorderRadius: index == widget.setsRoutine.children.length - 1,
              onUpdate: widget.onUpdate,
              onDelete: () => deleteRoutine(widget.setsRoutine.children[index]),
              onDuplicate: () => duplicate(widget.setsRoutine.children[index]),
            ),
          ),
          const SizedBox(height: PADDING),
          ElevatedButton(
            onPressed: _addRoutine,
            style: ElevatedButton.styleFrom(
              shape: const CircleBorder(),
              backgroundColor: Colors.white,
              padding: const EdgeInsets.all(ICON_PADDING),
            ),
            child: const Icon(Icons.add, size: ICON_SIZE),
          ),
        ],
      ),
    );
  }

  Widget _menu() {
    return PopupMenuButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      itemBuilder: (BuildContext context) {
        return [
          PopupMenuItem<Function>(
            value: widget.onDelete,
            child: Text(AppLocalizations.of(context)!.delete),
          ),
          PopupMenuItem<Function>(
            value: widget.onDuplicate,
            child: Text(AppLocalizations.of(context)!.duplicate),
          ),
          PopupMenuItem<Function>(
            value: widget.moveUp,
            child: Text(AppLocalizations.of(context)!.move_up),
          ),
          PopupMenuItem<Function>(
            value: widget.moveDown,
            child: Text(AppLocalizations.of(context)!.move_down),
          ),
        ];
      },
      icon: const Icon(Icons.more_vert),
      onSelected: (Function func) {
        func();
        setState(() {});
      },
    );
  }

  Widget _setEditingWidget() {
    return Stack(
      children: [
        Column(
          children: [
            Text(
              AppLocalizations.of(context)!.set(2).toUpperCase(),
              style: const TextStyle(
                fontSize: SETS_FONT_SIZE,
                color: Colors.white60,
                letterSpacing: SETS_LETTER_SPACING,
              ),
            ),
            NumberPicker(
              initValue: widget.setsRoutine.sets,
              onChange: (val) {
                setState(() => widget.setsRoutine.sets = val);
                widget.onUpdate?.call();
              },
              padding: 5.0,
              numberTextFieldWidth: 80.0,
              fontSize: 24.0,
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(top: 4.0, left: 12.0),
          child: Text(
            "${widget.index} / ${widget.totalSets}",
            style: const TextStyle(
              fontSize: SETS_FONT_SIZE,
              color: Colors.white60,
              letterSpacing: 1.05,
            ),
          ),
        ),
      ],
    );
  }

  void _addRoutine() {
    setState(() {
      widget.setsRoutine.children.add(widget.setsRoutine.children.length % 2 == 0
          ? TrainingRoutine.standard(infoText: AppLocalizations.of(context)!.new_entry)
          : TrainingRoutine.rest(infoText: AppLocalizations.of(context)!.new_entry));
    });
    widget.onUpdate?.call();
  }

  void deleteRoutine(TimeRoutine routine) async {
    keyExtension++;
    bool? result = await ConfirmDialog().deleteConfirm(context, name: routine.infoText);

    if (mounted) {
      ScaffoldMessenger.of(context).removeCurrentSnackBar();
    }

    if (result != null && result) {
      setState(() {
        widget.setsRoutine.children.remove(routine);
      });
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(AppLocalizations.of(context)!.deleted_entry),
            duration: const Duration(milliseconds: SNACK_BAR_DURATION),
            behavior: SnackBarBehavior.floating,
          ),
        );
      }
    } else {
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(AppLocalizations.of(context)!.delete_canceled),
            duration: const Duration(milliseconds: SNACK_BAR_DURATION),
            behavior: SnackBarBehavior.floating,
          ),
        );
      }
    }
  }

  void duplicate(TimeRoutine timeRoutine) {
    setState(() {
      this.keyExtension++;
      widget.setsRoutine.children.insert(
        widget.setsRoutine.children.indexOf(timeRoutine) + 1,
        timeRoutine.clone(),
      );
    });
    widget.onUpdate?.call();
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Text(AppLocalizations.of(context)!.duplicated_entry),
          duration: const Duration(milliseconds: SNACK_BAR_DURATION),
        ),
      );
  }
}
