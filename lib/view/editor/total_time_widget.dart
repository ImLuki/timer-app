import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:timer_app/resources/constants.dart';
import 'package:timer_app/theme/app_colors.dart';

class TotalTimeWidget extends StatelessWidget {
  const TotalTimeWidget({
    super.key,
    required this.completeTime,
    this.timeFontSize = STANDARD_TIME_FONT_SIZE,
    this.colonFontSize = STANDARD_COLON_FONT_SIZE,
    this.headingFontSize = STANDARD_HEADING_FONT_SIZE,
    this.colonShift = STANDARD_COLON_SHIFT,
  });

  static const double STANDARD_TIME_FONT_SIZE = 42.0;
  static const double STANDARD_COLON_FONT_SIZE = 32.0;
  static const double STANDARD_HEADING_FONT_SIZE = 18.0;
  static const double STANDARD_TIME_SPACING = 8.0;
  static const double STANDARD_COLON_SHIFT = 6.0;
  static const double WORD_SPACING = 5.0;
  static const double LETTER_SPACING = 3.0;

  final int completeTime;
  final double timeFontSize;
  final double colonFontSize;
  final double headingFontSize;
  final double colonShift;

  @override
  Widget build(BuildContext context) {
    return _getTimeWidget(context);
  }

  Widget _getTimeWidget(BuildContext context) {
    int hour = completeTime ~/ (Constants.SECONDS_PER_MINUTE * Constants.SECONDS_PER_MINUTE);
    int min =
        completeTime % (Constants.SECONDS_PER_MINUTE * Constants.SECONDS_PER_MINUTE) ~/ Constants.SECONDS_PER_MINUTE;
    int seconds = completeTime % Constants.SECONDS_PER_MINUTE;

    return Column(
      children: [
        Text(
          AppLocalizations.of(context)!.timeInTotal.toUpperCase(),
          style: TextStyle(
            fontSize: headingFontSize,
            color: Colors.white54,
            wordSpacing: WORD_SPACING,
            letterSpacing: LETTER_SPACING,
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _getTimeNumber(hour, "h"),
            const SizedBox(width: STANDARD_TIME_SPACING),
            _getColonWidget(),
            const SizedBox(width: STANDARD_TIME_SPACING),
            _getTimeNumber(min, "m"),
            const SizedBox(width: STANDARD_TIME_SPACING),
            _getColonWidget(),
            const SizedBox(width: STANDARD_TIME_SPACING),
            _getTimeNumber(seconds, "s"),
          ],
        )
      ],
    );
  }

  Widget _getTimeNumber(int time, String shortcut) {
    return Column(
      children: [
        Text(
          time.toString().padLeft(2, "0"),
          style: TextStyle(color: AppColors.secondary, fontSize: timeFontSize),
        ),
        Text(
          shortcut,
          style: const TextStyle(color: Colors.white54),
        ),
      ],
    );
  }

  Widget _getColonWidget() {
    return Column(
      children: [
        SizedBox(
          height: colonShift,
        ),
        Text(
          Constants.COLON_SIGN,
          style: TextStyle(color: Colors.white, fontSize: colonFontSize),
        ),
      ],
    );
  }
}
