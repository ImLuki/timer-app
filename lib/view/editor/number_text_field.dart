import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class NumberTextField extends StatelessWidget {
  const NumberTextField({
    super.key,
    required this.controller,
    required this.width,
    required this.regex,
    required this.onChangePreCalc,
    this.fontSize = STANDARD_FONT_SIZE,
    this.alignment = TextAlign.center,
    this.height = STANDARD_HEIGHT,
  });

  static const double STANDARD_HEIGHT = 42.0;
  static const double STANDARD_FONT_SIZE = 28.0;

  final TextEditingController? controller;
  final double width;
  final double height;
  final String regex;
  final Function(String) onChangePreCalc;
  final double fontSize;
  final TextAlign alignment;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: this.height,
      width: this.width,
      child: Center(
        child: TextField(
          keyboardType: TextInputType.number,
          inputFormatters: [
            FilteringTextInputFormatter.digitsOnly,
            FilteringTextInputFormatter.allow(RegExp(regex)),
            LengthLimitingTextInputFormatter(3),
          ],
          enableInteractiveSelection: false,
          cursorColor: Colors.white,
          maxLines: 1,
          decoration: const InputDecoration(border: InputBorder.none, isDense: true, contentPadding: EdgeInsets.zero),
          textAlignVertical: TextAlignVertical.center,
          onChanged: (newValue) => {onChangePreCalc(newValue)},
          onEditingComplete: () => FocusManager.instance.primaryFocus?.unfocus(),
          controller: controller,
          textAlign: alignment,
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: this.fontSize,
          ),
        ),
      ),
    );
  }
}
