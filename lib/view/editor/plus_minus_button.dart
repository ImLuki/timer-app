import 'dart:async';

import 'package:flutter/material.dart';

class PlusMinusButton extends StatefulWidget {
  const PlusMinusButton({
    super.key,
    required this.onChangeValue,
    required this.changeValue,
    required this.icon,
    this.iconBoxSize = ICON_BOX_SIZE,
    this.iconSize = ICON_SIZE,
  });

  static const double ICON_BOX_SIZE = 24.0;
  static const double ICON_SIZE = 24.0;

  final Function(int) onChangeValue;
  final int changeValue;
  final IconData icon;
  final double iconBoxSize;
  final double iconSize;

  @override
  State<PlusMinusButton> createState() => _PlusMinusButtonState();
}

class _PlusMinusButtonState extends State<PlusMinusButton> {
  Timer? _timer;

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Colors.transparent,
      hoverColor: Colors.transparent,
      focusColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: () => widget.onChangeValue(widget.changeValue),
      onTapDown: (TapDownDetails details) {
        _timer = Timer.periodic(const Duration(milliseconds: 100), (t) {
          widget.onChangeValue(widget.changeValue);
        });
      },
      onTapUp: (TapUpDetails details) {
        _timer?.cancel();
      },
      onTapCancel: () {
        _timer?.cancel();
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(
              Radius.circular(4.0),
            ),
          ),
          child: SizedBox(
            width: widget.iconBoxSize,
            height: widget.iconBoxSize,
            child: Icon(
              widget.icon,
              color: Colors.black,
              size: widget.iconSize,
            ),
          ),
        ),
      ),
    );
  }
}
