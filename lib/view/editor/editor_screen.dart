import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:timer_app/theme/app_colors.dart';
import 'package:timer_app/view/editor/advanced/advanced_editor.dart';
import 'package:timer_app/view/editor/simple/simple_editor.dart';
import 'package:timer_app/view/util/custom_on_will_pop_scope.dart';
import 'package:timer_app/view_model/edit_view_model.dart';

class EditorScreen extends StatefulWidget {
  const EditorScreen({super.key, required this.editViewModel});

  final EditViewModel editViewModel;

  @override
  _EditorScreenState createState() => _EditorScreenState(editViewModel);
}

class _EditorScreenState extends State<EditorScreen> {
  static const iconSize = 32.0;
  static const buttonPadding = 8.0;
  static const borderSideWidth = 0.8;
  static const fontSize = 12.0;
  static const letterSpacing = 1.0;

  bool _floatingActionButtonVisibility = true;
  late final EditViewModel _editViewModel;

  StreamSubscription<bool>? _keyboardSubscription;

  _EditorScreenState(this._editViewModel);

  @override
  void initState() {
    super.initState();

    KeyboardVisibilityController keyboardVisibilityController = KeyboardVisibilityController();
    this._floatingActionButtonVisibility = !keyboardVisibilityController.isVisible;

    // Subscribe
    _keyboardSubscription = keyboardVisibilityController.onChange.listen((bool visible) {
      if (!visible) FocusManager.instance.primaryFocus?.unfocus();
      setState(() {
        this._floatingActionButtonVisibility = !visible;
      });
    });
  }

  @override
  void dispose() {
    _keyboardSubscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomWillPopScope(
      onWillPop: () async {
        if (_editViewModel.hasBeenChanged) {
          _leaveNotSavedWarning();
          return false;
        } else {
          Navigator.pop(context);
          return false;
        }
      },
      child: GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Scaffold(
          floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
          floatingActionButton: Visibility(
            visible: this._floatingActionButtonVisibility,
            child: FloatingActionButton(
              heroTag: null,
              backgroundColor: Theme.of(context).colorScheme.secondary,
              onPressed: () => _save(context),
              child: const Icon(
                Icons.save,
                color: Colors.black,
                size: iconSize,
              ),
            ),
          ),
          bottomNavigationBar: Container(
            decoration: const BoxDecoration(
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Colors.white10,
                  blurRadius: 10,
                ),
              ],
            ),
            child: BottomAppBar(
              color: AppColors.primary,
              elevation: 8.0,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  _getLeftButton(),
                  _getRightButton(),
                ],
              ),
            ),
          ),
          body: SafeArea(
            child: _editViewModel.advanced
                ? AdvancedEditor(editViewModel: _editViewModel)
                : SimpleEditor(editViewModel: _editViewModel),
          ),
        ),
      ),
    );
  }

  Widget _getRightButton() {
    return Padding(
      padding: const EdgeInsets.all(2.0).copyWith(right: buttonPadding),
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          foregroundColor: Colors.white,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
          side: BorderSide(
            color: _editViewModel.advanced ? Colors.white : Colors.transparent,
            width: borderSideWidth,
          ),
        ),
        onPressed: () => setState(() => _editViewModel.advanced = true),
        child: _getBottomButtonText(AppLocalizations.of(context)!.advanced.toUpperCase()),
      ),
    );
  }

  Widget _getLeftButton() {
    return Padding(
      padding: const EdgeInsets.all(2.0).copyWith(left: buttonPadding),
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          foregroundColor: Colors.white,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
          side: BorderSide(
            color: _editViewModel.advanced ? Colors.transparent : Colors.white,
            width: borderSideWidth,
          ),
        ),
        onPressed: () {
          setState(() {
            this._editViewModel.advanced = false;
          });
        },
        child: _getBottomButtonText(AppLocalizations.of(context)!.simple.toUpperCase()),
      ),
    );
  }

  Widget _getBottomButtonText(String text) {
    return Text(
      text,
      style: const TextStyle(
        fontSize: fontSize,
        letterSpacing: letterSpacing,
        color: Colors.white,
      ),
    );
  }

  void _save(BuildContext context) {
    _editViewModel.saveRoutine(context).then((bool value) {
      if (value && context.mounted) {
        Navigator.pop(context, value);
        FocusScope.of(context).unfocus();
      } else {
        if (context.mounted) {
          ScaffoldMessenger.of(context)
            ..removeCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Text(AppLocalizations.of(context)!.advanced_routine_without_children),
                behavior: SnackBarBehavior.floating,
              ),
            );
        }
      }
    });
  }

  Future<void> _leaveNotSavedWarning() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            AppLocalizations.of(context)!.want_save,
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                AppLocalizations.of(context)!.not_save,
                style: const TextStyle(color: AppColors.secondary),
              ),
              onPressed: () {
                Navigator.pop(context);
                Navigator.pop(context);
              },
            ),
            TextButton(
              child: Text(
                AppLocalizations.of(context)!.save,
                style: const TextStyle(color: AppColors.secondary),
              ),
              onPressed: () {
                _save(context);
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }
}
