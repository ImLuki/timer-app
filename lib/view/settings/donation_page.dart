import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:timer_app/resources/constants.dart';

class DonationPage extends StatefulWidget {
  const DonationPage({super.key});

  @override
  _DonationPageState createState() => _DonationPageState();
}

class _DonationPageState extends State<DonationPage> {
  final InAppPurchase _iap = InAppPurchase.instance;

  final List<String> _productIds = [
    "1_euro_donation",
    "2_euro_donation",
    "5_euro_donation",
    "10_euro_donation",
    "20_euro_donation",
    "50_euro_donation"
  ];
  final List<String> _productNames = ["1€", "2€", "5€", "10€", "20€", "50€"];

  late StreamSubscription _subscription;

  bool _isAvailable = false;
  List<ProductDetails> _products = [];

  @override
  void initState() {
    super.initState();
    _initialize();
  }

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }

  Future<void> _initialize() async {
    // Check availability of InApp Purchases
    _isAvailable = await _iap.isAvailable();
    debugPrint("isAvailable: $_isAvailable");
    if (_isAvailable) {
      await _getUserProducts();

      _subscription = _iap.purchaseStream.listen((data) => setState(() {}));
    }
  }

  void _buyProduct(ProductDetails prod) {
    final PurchaseParam purchaseParam = PurchaseParam(productDetails: prod);
    _iap.buyConsumable(purchaseParam: purchaseParam, autoConsume: true);
  }

  Future<void> _getUserProducts() async {
    ProductDetailsResponse response = await _iap.queryProductDetails(_productIds.toSet());

    setState(() {
      _products = response.productDetails;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 48.0,
      ),
      body: SafeArea(
        child: _content(),
      ),
    );
  }

  Widget _content() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: [
        Flexible(
          child: Padding(
            padding: const EdgeInsets.all(60),
            child: Image.asset(
              "assets/images/donation.png",
              fit: BoxFit.cover,
            ),
          ),
        ),
        ClipPath(
          clipper: CustomClipPath(),
          child: Container(
            decoration: const BoxDecoration(
              color: Color(0xff1f2d36),
            ),
            height: max(MediaQuery.of(context).size.height / 2, 350),
            child: SingleChildScrollView(
              child: _donationContent(),
            ),
          ),
        ),
      ],
    );
  }

  Widget _donationContent() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(
        Constants.STANDARD_PADDING,
        60.0,
        Constants.STANDARD_PADDING,
        Constants.STANDARD_PADDING,
      ),
      child: Column(
        children: [
          Text(
            AppLocalizations.of(context)!.settings_donation_support,
            style: Theme.of(context).textTheme.displaySmall!.copyWith(color: Colors.white),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 10),
          Text(
            AppLocalizations.of(context)!.settings_donation_support_info,
            overflow: TextOverflow.clip,
            style: Theme.of(context).textTheme.bodyLarge!.copyWith(color: Colors.white, fontStyle: FontStyle.italic),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 32),
          //const Spacer(),
          _createDonateButtons(),
          //const Spacer(),
        ],
      ),
    );
  }

  Widget _createDonateButtons() {
    return Wrap(
      alignment: WrapAlignment.center,
      runSpacing: 24,
      spacing: 20,
      children: List.generate(
        _productIds.length,
            (index) => TextButton(
          onPressed: () async {
            try {
              _buyProduct(_products.firstWhere((element) => element.id == _productIds[index]));
            } catch (e, stacktrace) {
              debugPrint(stacktrace.toString());
              debugPrint(e.toString());
              _showWarningScreen(context);
            }
          },
          style: TextButton.styleFrom(
            padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 32.0),
            backgroundColor: Colors.grey[300],
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(48.0),
            ),
          ),
          child: Text(
            _productNames[index],
            style: const TextStyle(fontSize: 18),
          ),
        ),
      ),
    );
  }

  Future<void> _showWarningScreen(BuildContext context) async {
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          scrollable: true,
          contentPadding: const EdgeInsets.fromLTRB(24.0, 20.0, 24.0, 24.0),
          titleTextStyle: Theme.of(context).textTheme.titleMedium,
          title: Text(AppLocalizations.of(context)!.settings_donation_donate_warning),
          content: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Icon(Icons.warning_amber_rounded, color: Colors.orange, size: 48),
              const SizedBox(width: 14.0),
              Flexible(
                child: Text(
                  AppLocalizations.of(context)!.settings_donation_donate_warning_info,
                  style: const TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
                  overflow: TextOverflow.clip,
                ),
              ),
            ],
          ),
          contentTextStyle: Theme.of(context).textTheme.bodyMedium?.copyWith(fontWeight: FontWeight.w400),
          actions: <Widget>[
            TextButton(
              onPressed: () => {FocusScope.of(context).unfocus(), Navigator.pop(context)},
              child: Text(
                AppLocalizations.of(context)!.back,
                style: TextStyle(color: Theme.of(context).colorScheme.secondary),
              ),
            ),
          ],
        );
      },
    );
  }
}

class CustomClipPath extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    double curveHeight = 50.0;
    Offset controlPoint = Offset(size.width / 2, 0);
    Offset endPoint = Offset(size.width, curveHeight);

    Path path = Path();
    path
      ..moveTo(0, curveHeight)
      ..quadraticBezierTo(controlPoint.dx, controlPoint.dy, endPoint.dx, endPoint.dy)
      ..lineTo(size.width, size.height)
      ..lineTo(0, size.height)
      ..close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
