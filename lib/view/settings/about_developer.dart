import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:open_store/open_store.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:timer_app/view/settings/donation_page.dart';
import 'package:timer_app/view/util/custom_page_route.dart';
import 'package:url_launcher/url_launcher_string.dart';

class AboutPage extends StatelessWidget {
  final String versionNumber;
  final String buildNumber;

  const AboutPage({super.key, required this.versionNumber, required this.buildNumber});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // toolbarHeight: 48.0,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: _body(context),
        ),
      ),
    );
  }

  Widget _body(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 20.0),
        _appName(context),
        const SizedBox(height: 30.0),
        if (Platform.isAndroid) _donationWidget(context),
        _feedBack(context),
        _developer(context),
        _credits(context),
        const SizedBox(height: 60),
      ],
    );
  }

  Widget _appName(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: 120,
          height: 120,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(32.0),
            child: Image.asset('assets/icon/ios_icon.png'),
          ),
        ),
        const SizedBox(height: 20.0),
        Center(
          child: Text(
            AppLocalizations.of(context)!.title,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.displaySmall?.copyWith(
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                  letterSpacing: -1.0,
                ),
          ),
        ),
        Text(
          "${AppLocalizations.of(context)!.screen_about_version}: $versionNumber ($buildNumber)",
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.bodyMedium,
        ),
      ],
    );
  }

  Widget _container(Widget containerBody) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 32.0),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.white54),
        borderRadius: const BorderRadius.all(Radius.circular(32)),
      ),
      child: containerBody,
    );
  }

  Widget _containerBody({
    required BuildContext context,
    required List<Widget> innerText,
    Widget? primaryButton,
    Widget? secondaryButton,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ...innerText,
        if (primaryButton != null || secondaryButton != null) const SizedBox(height: 20.0),
        if (secondaryButton != null) secondaryButton,
        if (primaryButton != null) primaryButton,
      ],
    );
  }

  List<Widget> _containerBodyInnerText({
    required BuildContext context,
    required String title,
    required String text,
  }) {
    return [
      Text(
        title,
        overflow: TextOverflow.clip,
        style: Theme.of(context).textTheme.titleLarge?.copyWith(fontWeight: FontWeight.w500),
      ),
      const SizedBox(height: 12.0),
      Text(
        text,
        style: Theme.of(context).textTheme.bodyLarge?.copyWith(height: 1.3),
        overflow: TextOverflow.clip,
      ),
    ];
  }

  Widget _primaryButton({
    required BuildContext context,
    required String text,
    required IconData icon,
    required VoidCallback? onPressed,
  }) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: ElevatedButton(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.red.shade400,
          shadowColor: Colors.white30,
          padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 16.0),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Text(
                text,
                style: const TextStyle(color: Colors.white),
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              ),
            ),
            const SizedBox(width: 12.0),
            Icon(icon, color: Colors.white),
          ],
        ),
      ),
    );
  }

  Widget _secondaryButton({
    required BuildContext context,
    required String text,
    required IconData icon,
    required VoidCallback? onPressed,
  }) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: TextButton(
          onPressed: onPressed,
          style: TextButton.styleFrom(
            backgroundColor: Colors.red.shade400,
            shadowColor: Colors.white30,
            padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 16.0),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Flexible(
                child: Text(
                  text,
                  style: const TextStyle(color: Colors.white),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                ),
              ),
              const SizedBox(width: 12.0),
              Icon(icon, color: Colors.white),
            ],
          ),
        ),
      ),
    );
  }

  Widget _feedBack(BuildContext context) {
    return _container(
      _containerBody(
        context: context,
        innerText: _containerBodyInnerText(
          context: context,
          title: AppLocalizations.of(context)!.screen_about_feedback,
          text: AppLocalizations.of(context)!.screen_about_feedback_info,
        ),
        secondaryButton: _secondaryButton(
          context: context,
          text: AppLocalizations.of(context)!.screen_about_feedback_button_contact,
          icon: Icons.mail_outline,
          onPressed: _sendFeedBack,
        ),
        primaryButton: _primaryButton(
          context: context,
          text: AppLocalizations.of(context)!.screen_about_feedback_button_rate,
          icon: Icons.star_outline_sharp,
          onPressed: () => OpenStore.instance.open(
            androidAppBundleId: 'timerapp.timer_app', // Android app bundle package name
          ),
        ),
      ),
    );
  }

  Widget _developer(BuildContext context) {
    return _container(
      _containerBody(
        context: context,
        innerText: [
          Text(
            AppLocalizations.of(context)!.screen_about_dev_developed,
            style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                  height: 1.3,
                  fontSize: 14,
                  fontStyle: FontStyle.italic,
                ),
            overflow: TextOverflow.clip,
          ),
          const SizedBox(height: 16.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Image.asset(
                  "assets/images/profile.png",
                  width: 70.0,
                  height: 70.0,
                  fit: BoxFit.cover,
                ),
              ),
              const SizedBox(width: 14.0),
              Flexible(
                child: AutoSizeText(
                  AppLocalizations.of(context)!.screen_about_dev_name,
                  overflow: TextOverflow.clip,
                  maxLines: 2,
                  style: Theme.of(context).textTheme.titleLarge?.copyWith(
                        fontWeight: FontWeight.w500,
                        fontSize: 32,
                        height: 0.95,
                      ),
                ),
              ),
            ],
          ),
          const SizedBox(height: 16.0),
          Text(
            AppLocalizations.of(context)!.screen_about_dev_info,
            style: Theme.of(context).textTheme.bodyLarge?.copyWith(height: 1.3),
            overflow: TextOverflow.clip,
            maxLines: 10,
          ),
        ],
        primaryButton: _primaryButton(
          context: context,
          text: AppLocalizations.of(context)!.screen_about_dev_button_other_apps,
          icon: Icons.open_in_new,
          onPressed: _launchDeveloperPage,
        ),
      ),
    );
  }

  Widget _donationWidget(BuildContext context) {
    return _container(
      _containerBody(
        context: context,
        innerText: _containerBodyInnerText(
          context: context,
          title: AppLocalizations.of(context)!.screen_about_support,
          text: AppLocalizations.of(context)!.screen_about_support_info,
        ),
        primaryButton: _primaryButton(
          context: context,
          text: AppLocalizations.of(context)!.screen_about_support_button,
          icon: Icons.favorite,
          onPressed: () => _donate(context),
        ),
      ),
    );
  }

  static Future<void> _sendFeedBack({String? feedback = "Hey Lukas,\n"}) async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      final Email email = Email(
        body: "device: ${androidInfo.model}\n"
            "app_version: ${packageInfo.version} (${packageInfo.buildNumber})\n"
            "sdk_version: ${androidInfo.version.sdkInt}\n\n\n"
            "$feedback",
        recipients: ['freudenmann.business+interval-timer@gmail.com'],
        isHTML: false,
      );
      await FlutterEmailSender.send(email);
    } else if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      final Email email = Email(
        body: "device: ${iosInfo.model}\n"
            "app_version: ${packageInfo.version} (${packageInfo.buildNumber})\n"
            "system_version: ${iosInfo.systemVersion}\n\n\n"
            "Hey Lukas,\n",
        recipients: ['freudenmann.business+interval-timer@gmail.com'],
        isHTML: false,
      );
      await FlutterEmailSender.send(email);
    }
  }

  _launchDeveloperPage() async {
    const url = 'https://play.google.com/store/apps/developer?id=Lukas+Freudenmann';
    if (await canLaunchUrlString(url)) {
      await launchUrlString(url, mode: LaunchMode.externalApplication);
    } else {
      throw 'Could not launch $url';
    }
  }

  void _donate(BuildContext context) {
    Navigator.push(
      context,
      CustomPageRoute.build(builder: (context) => const DonationPage()),
    );
  }

  Widget _credits(BuildContext context) {
    return _container(
      _containerBody(
        context: context,
        innerText: [
          Text(
            AppLocalizations.of(context)!.screen_about_credits,
            overflow: TextOverflow.clip,
            style: Theme.of(context).textTheme.titleLarge?.copyWith(fontWeight: FontWeight.w500),
          ),
          const SizedBox(height: 12.0),
          SelectableText.rich(
            TextSpan(
              children: [
                _normalText(context: context, text: "${AppLocalizations.of(context)!.screen_about_credits_alex}\n\n"),
                _normalText(context: context, text: "- "),
                _hyperLinkText(
                  context: context,
                  text: "Alexander Salameh",
                  url: "https://alexander-salameh.de/index.php/de/",
                ),
                _normalText(context: context, text: "\n\n\n"),
                _normalText(
                  context: context,
                  text: "${AppLocalizations.of(context)!.screen_about_credits_flaticon} (",
                ),
                _hyperLinkText(context: context, text: "www.flaticon.com", url: "https://www.flaticon.com/"),
                _normalText(context: context, text: "):\n\n"),
                _normalText(context: context, text: "- "),
                _hyperLinkText(
                  context: context,
                  text: "Icon Pack: Spiritual Activities\n",
                  url: "https://www.flaticon.com/packs/spiritual-activities-3",
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  TextSpan _normalText({required BuildContext context, required String text}) {
    return TextSpan(
      style: Theme.of(context).textTheme.bodyLarge?.copyWith(height: 1.3),
      text: text,
    );
  }

  TextSpan _hyperLinkText({required BuildContext context, required String text, required String url}) {
    return TextSpan(
      style: Theme.of(context).textTheme.bodyLarge?.copyWith(
            height: 1.3,
            color: Theme.of(context).colorScheme.secondary,
          ),
      text: text,
      recognizer: TapGestureRecognizer()
        ..onTap = () async {
          if (await canLaunchUrlString(url)) {
            await launchUrlString(url, mode: LaunchMode.externalApplication);
          }
        },
    );
  }
}
