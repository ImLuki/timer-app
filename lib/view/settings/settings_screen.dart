import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:fluttericon/font_awesome5_icons.dart';
import 'package:open_store/open_store.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:share_plus/share_plus.dart';
import 'package:timer_app/model/data_store.dart';
import 'package:timer_app/model/setting_model.dart';
import 'package:timer_app/model/sounds.dart';
import 'package:timer_app/theme/app_colors.dart';
import 'package:timer_app/view/dialogs/export_file_dialog.dart';
import 'package:timer_app/view/dialogs/import_file_dialog.dart';
import 'package:timer_app/view/dialogs/sound_picker_dialog.dart';
import 'package:timer_app/view/dialogs/timer_picker_dialog.dart';
import 'package:timer_app/view/settings/about_developer.dart';
import 'package:timer_app/view/settings/donation_page.dart';
import 'package:timer_app/view/util/color_picker.dart';
import 'package:timer_app/view/util/custom_page_route.dart';
import 'package:timer_app/view/util/number_picker.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({super.key});

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  final TextEditingController _prepTimeController = TextEditingController();
  final SettingModel _settingModel = DataStore().settingModel;

  String versionNumber = "";
  String buildNumber = "";

  @override
  void initState() {
    super.initState();
    this._prepTimeController.text = _settingModel.standardPrepTime.toString();

    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      setState(() {
        versionNumber = packageInfo.version;
        buildNumber = packageInfo.buildNumber;
      });
    });
  }

  @override
  void dispose() {
    this._prepTimeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 80.0,
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        surfaceTintColor: Colors.white,
        centerTitle: true,
        title: Text(
          AppLocalizations.of(context)!.settings.toUpperCase(),
          textAlign: TextAlign.start,
          style: Theme.of(context).textTheme.displaySmall?.copyWith(
                letterSpacing: -1.0,
              ),
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: _createSettings(),
        ),
      ),
    );
  }

  Widget _createSettings() {
    return Column(
      children: [
        const SizedBox(height: 8.0),
        ..._colorSettings(),
        ..._soundSettings(),
        ..._backup(),
        ..._miscellaneousSettings(),
        ..._otherSettings(),
        ..._appVersion(),
        const SizedBox(height: 50)
      ],
    );
  }

  Widget _createSettingWidget({
    required String title,
    required String info,
    required IconData icon,
    Widget? setting,
    void Function()? onTapFunction,
    IconData? secondIcon,
  }) {
    Widget container = Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Stack(
            children: [
              SizedBox(
                height: 30.0,
                width: 30.0,
                child: secondIcon != null
                    ? Align(
                        alignment: Alignment.bottomRight,
                        child: Icon(secondIcon, color: Colors.white, size: 12.0),
                      )
                    : const SizedBox(),
              ),
              RotatedBox(
                quarterTurns: icon == Icons.fitness_center ? 1 : 0,
                child: Icon(icon, color: Colors.white),
              ),
            ],
          ),
          const SizedBox(width: 20.0),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 6.0),
                  child: Text(
                    title,
                    style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                          fontWeight: FontWeight.normal,
                          height: 0.9,
                          fontSize: 20.0,
                        ),
                  ),
                ),
                Text(
                  info,
                  maxLines: 10,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                        height: 1.2,
                        color: Colors.white.withOpacity(0.8),
                      ),
                ),
              ],
            ),
          ),
          //Spacer(),
          SizedBox(width: setting != null ? 12.0 : 0.0),
          if (setting != null) setting,
        ],
      ),
    );
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12.0),
      child: onTapFunction != null
          ? InkWell(
              onTap: onTapFunction,
              borderRadius: const BorderRadius.all(
                Radius.circular(24.0),
              ),
              child: container,
            )
          : container,
    );
  }

  Widget _header({required String text, VoidCallback? onReset}) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.only(
        left: 24.0,
        right: 20.0,
        top: 20.0,
        bottom: 10.0,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            child: Text(
              text.toUpperCase(),
              style: Theme.of(context).textTheme.titleMedium?.copyWith(
                    fontWeight: FontWeight.normal,
                    letterSpacing: 1.5,
                    color: Theme.of(context).colorScheme.secondary,
                  ),
              overflow: TextOverflow.visible,
              maxLines: 1,
            ),
          ),
          if (onReset != null)
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: IconButton(
                padding: EdgeInsets.zero,
                tooltip: AppLocalizations.of(context)!.reset_defaults,
                onPressed: onReset,
                icon: const Icon(Icons.refresh),
              ),
            ),
        ],
      ),
    );
  }

  List<Widget> _colorSettings() {
    return [
      _header(
          text: AppLocalizations.of(context)!.standard_colors.toUpperCase(),
          onReset: () => setState(() {
                _settingModel.standardTrainingColor = AppColors.secondary;
                _settingModel.standardRestColor = AppColors.restColor;
                _settingModel.standardPreparationColor = AppColors.prepColor;
              })),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.timer_app_preparation_key,
        info: AppLocalizations.of(context)!.settings_colors_help_prep,
        icon: Icons.timer_outlined,
        secondIcon: Icons.palette_outlined,
        setting: _colorSettingsBox(color: _settingModel.standardPreparationColor),
        onTapFunction: () => _onColorPick(
          currentColor: _settingModel.standardPreparationColor,
          onColorChanged: (color) => _settingModel.standardPreparationColor = color,
        ),
      ),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.timer_app_training_key,
        info: AppLocalizations.of(context)!.settings_colors_help_training,
        icon: Icons.fitness_center,
        secondIcon: Icons.palette_outlined,
        setting: _colorSettingsBox(color: _settingModel.standardTrainingColor),
        onTapFunction: () => _onColorPick(
          currentColor: _settingModel.standardTrainingColor,
          onColorChanged: (color) => _settingModel.standardTrainingColor = color,
        ),
      ),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.timer_app_rest_key,
        info: AppLocalizations.of(context)!.settings_colors_help_rest,
        icon: Icons.restore,
        secondIcon: Icons.palette_outlined,
        setting: _colorSettingsBox(color: _settingModel.standardRestColor),
        onTapFunction: () => _onColorPick(
          currentColor: _settingModel.standardRestColor,
          onColorChanged: (color) => _settingModel.standardRestColor = color,
        ),
      ),
    ];
  }

  List<Widget> _soundSettings() {
    return [
      _header(text: AppLocalizations.of(context)!.sounds),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.enable_sounds,
        info: AppLocalizations.of(context)!.enable_sounds_info,
        icon: _settingModel.soundEnabled ? Icons.volume_up : Icons.volume_off,
        setting: Switch(
          activeColor: Colors.tealAccent,
          value: _settingModel.soundEnabled,
          onChanged: (val) => setState(() => _settingModel.soundEnabled = val),
        ),
      ),
      if (_settingModel.soundEnabled) ...[
        _createSettingWidget(
          title: AppLocalizations.of(context)!.amount_counting_sounds,
          info: AppLocalizations.of(context)!.amount_counting_sounds_info,
          icon: Icons.timelapse,
          onTapFunction: () => _showNumberPickerDialog(
            initValue: _settingModel.amountCountingSounds,
            title: AppLocalizations.of(context)!.amount_counting_sounds,
            onChanged: (int value) {
              _settingModel.amountCountingSounds = value;
              setState(() {});
            },
          ),
        ),
        _soundPickerWidget(
          title: AppLocalizations.of(context)!.countdown_sound,
          info: AppLocalizations.of(context)!.countdown_sound_info,
          icon: Icons.library_music,
          values: ShortSound.values,
          onChange: (val) => setState(() => _settingModel.setCountDownSound(val)),
          onPlay: (sound) => DataStore().soundHandler.playShortSound(
                ShortSound.values.byName(sound),
                AppLocalizations.of(context)!.tts_test_sound_short,
              ),
          value: _settingModel.countDownSound.name,
        ),
        _soundPickerWidget(
          title: AppLocalizations.of(context)!.training_sound,
          info: AppLocalizations.of(context)!.training_sound_info,
          icon: Icons.fitness_center,
          values: Sound.values,
          onChange: (val) => setState(() => _settingModel.setTrainingSound(val)),
          onPlay: (sound) => DataStore().soundHandler.playSound(
                Sound.values.byName(sound),
                AppLocalizations.of(context)!.tts_test_sound,
              ),
          value: _settingModel.trainingSound.name,
        ),
        _soundPickerWidget(
          title: AppLocalizations.of(context)!.rest_sound,
          info: AppLocalizations.of(context)!.rest_sound_info,
          icon: Icons.restore,
          values: Sound.values,
          onChange: (val) => setState(() => _settingModel.setRestSound(val)),
          onPlay: (sound) => DataStore().soundHandler.playSound(
                Sound.values.byName(sound),
                AppLocalizations.of(context)!.tts_test_sound,
              ),
          value: _settingModel.restSound.name,
        ),
        _soundPickerWidget(
          title: AppLocalizations.of(context)!.finish_sound,
          info: AppLocalizations.of(context)!.finish_sound_info,
          icon: Icons.sports_score,
          values: Sound.values,
          onChange: (val) => setState(() => _settingModel.setFinishSound(val)),
          onPlay: (sound) => DataStore().soundHandler.playSound(
                Sound.values.byName(sound),
                AppLocalizations.of(context)!.tts_test_sound,
              ),
          value: _settingModel.finishSound.name,
        ),
        _createSettingWidget(
          title: AppLocalizations.of(context)!.half_time_sound_activated,
          info: AppLocalizations.of(context)!.half_time_sound_activated_info,
          icon: Icons.sync_alt,
          setting: Switch(
            activeColor: Colors.tealAccent,
            value: _settingModel.halftimeSoundActivated,
            onChanged: (val) => setState(() => _settingModel.halftimeSoundActivated = val),
          ),
        ),
        if (_settingModel.halftimeSoundActivated) ...[
          _soundPickerWidget(
            title: AppLocalizations.of(context)!.half_time_sound,
            info: AppLocalizations.of(context)!.half_time_sound_info,
            icon: Icons.sync_alt,
            values: Sound.values,
            onChange: (val) => setState(() => _settingModel.setHalfTimeSound(val)),
            onPlay: (sound) => DataStore().soundHandler.playSound(
                  Sound.values.byName(sound),
                  AppLocalizations.of(context)!.tts_test_sound,
                ),
            value: _settingModel.halfTimeSound.name,
          ),
        ]
      ],
    ];
  }

  List<Widget> _backup() {
    return [
      _header(text: AppLocalizations.of(context)!.backup),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.export_training_routine,
        info: AppLocalizations.of(context)!.export_training_routine_info,
        icon: Icons.file_upload,
        onTapFunction: () => ExportFileDialog().showExportDialog(context),
      ),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.import_backup_file,
        info: AppLocalizations.of(context)!.import_backup_file_info,
        icon: FontAwesome5.file_import,
        onTapFunction: () => ImportFileDialog().showLoadFileDialog(context),
      ),
    ];
  }

  List<Widget> _miscellaneousSettings() {
    return [
      _header(text: AppLocalizations.of(context)!.miscellaneous),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_prep_time,
        info: AppLocalizations.of(context)!.settings_prep_time_help,
        icon: Icons.timer_outlined,
        onTapFunction: () => TimerPickerDialog(
          currentValue: _settingModel.standardPrepTime,
          onChanged: (val) => setState(() => _settingModel.standardPrepTime = val),
          title: AppLocalizations.of(context)!.settings_prep_time,
        ).showScrollPickerDialog(context),
      ),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_skip_last_rest,
        info: AppLocalizations.of(context)!.settings_skip_last_rest_help,
        icon: Icons.skip_next,
        setting: Switch(
          activeColor: Colors.tealAccent,
          value: _settingModel.skipLastRest,
          onChanged: (val) => setState(() => _settingModel.skipLastRest = val),
        ),
      ),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_show_left_time,
        info: AppLocalizations.of(context)!.settings_show_left_time_info,
        icon: _settingModel.showLeftTime ? Icons.hourglass_bottom : Icons.hourglass_disabled,
        setting: Switch(
          activeColor: Colors.tealAccent,
          value: _settingModel.showLeftTime,
          onChanged: (val) => setState(() => _settingModel.showLeftTime = val),
        ),
      ),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.vibration,
        info: AppLocalizations.of(context)!.vibration_info,
        icon: _settingModel.vibrate ? Icons.vibration : Icons.mobile_off_outlined,
        setting: Switch(
            activeColor: Colors.tealAccent,
            value: _settingModel.vibrate,
            onChanged: (val) {
              setState(() {
                bool res = _settingModel.setVibrate(val);

                if (!res) {
                  ScaffoldMessenger.of(context)
                    ..removeCurrentSnackBar()
                    ..showSnackBar(
                      SnackBar(
                        content: Text(AppLocalizations.of(context)!.vibration_snack_bar_info),
                        behavior: SnackBarBehavior.floating,
                      ),
                    );
                }
              });
            }),
      ),
    ];
  }

  List<Widget> _otherSettings() {
    return [
      _header(text: AppLocalizations.of(context)!.others),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.screen_settings_about,
        info: AppLocalizations.of(context)!.screen_settings_about_info,
        icon: Icons.person_outline,
        onTapFunction: _pushAboutPage,
      ),
      if (Platform.isAndroid)
        _createSettingWidget(
          title: AppLocalizations.of(context)!.settings_donation,
          info: AppLocalizations.of(context)!.settings_donation_support_info,
          onTapFunction: _donate,
          icon: Icons.volunteer_activism_outlined,
        ),
      if (Platform.isAndroid)
        _createSettingWidget(
          title: AppLocalizations.of(context)!.share,
          info: AppLocalizations.of(context)!.share_help,
          onTapFunction: () => Share.share(
            AppLocalizations.of(context)!
                .settings_share_text("https://play.google.com/store/apps/details?id=timerapp.timer_app"),
          ),
          icon: Icons.share,
        ),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.rate,
        info: AppLocalizations.of(context)!.rate_help,
        onTapFunction: () => OpenStore.instance.open(
          androidAppBundleId: 'timerapp.timer_app', // Android app bundle package name
        ),
        icon: Icons.star_border,
      ),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.feedback,
        info: AppLocalizations.of(context)!.feedback_help,
        onTapFunction: _sendFeedBack,
        icon: Icons.local_post_office_outlined,
      ),
      const SizedBox(height: 20),
    ];
  }

  List<Widget> _appVersion() {
    return [
      const SizedBox(height: 30),
      Text(
        AppLocalizations.of(context)!.app_version,
        style: Theme.of(context).textTheme.bodyLarge?.copyWith(
              fontWeight: FontWeight.normal,
              height: 0.9,
              fontSize: 18.0,
            ),
      ),
      Text(
        "$versionNumber ($buildNumber)",
        style: Theme.of(context).textTheme.bodyMedium?.copyWith(
              color: Colors.white70,
              fontWeight: FontWeight.normal,
            ),
      ),
    ];
  }

  Widget _colorSettingsBox({required Color color}) {
    return InkWell(
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10.0),
        height: 40.0,
        width: 40.0,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: color,
        ),
      ),
    );
  }

  Widget _soundPickerWidget({
    required String title,
    required String info,
    required IconData icon,
    required List<Enum> values,
    required Function(String) onChange,
    required Function(String)? onPlay,
    required String value,
  }) {
    return _createSettingWidget(
      title: title,
      info: info,
      icon: icon,
      secondIcon: Icons.notifications_outlined,
      onTapFunction: () => SoundPickerDialog(
        items: values.map((Enum e) => e.name).toList(),
        currentValue: value,
        title: AppLocalizations.of(context)!.change_sound,
        onChanged: onChange,
        onPlay: onPlay,
      ).showScrollPickerDialog(context),
    );
  }

  void _pushAboutPage() {
    Navigator.of(context).push(
      CustomPageRoute.build(
        builder: (BuildContext context) => AboutPage(versionNumber: versionNumber, buildNumber: buildNumber),
      ),
    );
  }

  void _onColorPick({required Color currentColor, required ValueChanged<Color> onColorChanged}) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (context) {
        return CustomColorPicker(
          currentColor: currentColor,
          onColorChanged: onColorChanged,
          colors: [
            DataStore().settingModel.standardTrainingColor,
            DataStore().settingModel.standardRestColor,
            DataStore().settingModel.standardPreparationColor,
          ]
            ..removeWhere((element) => element.value == currentColor.value)
            ..insert(0, currentColor),
        );
      },
    ).then((_) {
      setState(() {});
    });
  }

  void _showNumberPickerDialog({
    required int initValue,
    required String title,
    required ValueChanged<int> onChanged,
  }) {
    showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: Center(child: Text(title)),
          contentPadding: const EdgeInsets.all(16.0),
          children: [
            const SizedBox(height: 20.0),
            NumberPicker(
              initValue: initValue,
              onChange: onChanged,
              padding: 10.0,
              numberTextFieldWidth: 120.0,
            ),
            const SizedBox(height: 20.0),
            _okButton(context),
          ],
        );
      },
    );
  }

  Widget _okButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40.0),
      child: TextButton(
        style: TextButton.styleFrom(
          foregroundColor: Theme.of(context).colorScheme.secondary,
        ),
        child: Text(AppLocalizations.of(context)!.ok),
        onPressed: () => Navigator.pop(context),
      ),
    );
  }

  static Future<void> _sendFeedBack({String? feedback = "Hey Lukas,\n"}) async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      final Email email = Email(
        body: "device: ${androidInfo.model}\n"
            "app_version: ${packageInfo.version} (${packageInfo.buildNumber})\n"
            "sdk_version: ${androidInfo.version.sdkInt}\n\n\n"
            "$feedback",
        recipients: ['freudenmann.business+interval-timer@gmail.com'],
        isHTML: false,
      );
      await FlutterEmailSender.send(email);
    } else if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      final Email email = Email(
        body: "device: ${iosInfo.model}\n"
            "app_version: ${packageInfo.version} (${packageInfo.buildNumber})\n"
            "system_version: ${iosInfo.systemVersion}\n\n\n"
            "Hey Lukas,\n",
        recipients: ['freudenmann.business+interval-timer@gmail.com'],
        isHTML: false,
      );
      await FlutterEmailSender.send(email);
    }
  }

  void _donate() {
    Navigator.push(
      context,
      CustomPageRoute.build(builder: (context) => const DonationPage()),
    );
  }
}
