import 'package:flutter/material.dart';
import 'package:timer_app/model/data_store.dart';

class VolumeSlider extends StatefulWidget {
  final Color color;

  const VolumeSlider({super.key, required this.color});

  @override
  _VolumeSliderState createState() => _VolumeSliderState();
}

class _VolumeSliderState extends State<VolumeSlider> {
  static const double ICON_SIZE = 28.0;
  static const double PADDING = 6.0;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        _icon(),
        const SizedBox(width: PADDING),
        Expanded(child: _slider()),
      ],
    );
  }

  Widget _icon() {
    return Icon(
      _iconData(),
      size: ICON_SIZE,
    );
  }

  IconData _iconData() {
    if (DataStore().soundHandler.volume == 0.0) return Icons.volume_off;
    if (DataStore().soundHandler.volume < 0.33) return Icons.volume_mute;
    if (DataStore().soundHandler.volume < 0.66) return Icons.volume_down;
    return Icons.volume_up;
  }

  Widget _slider() {
    return Slider(
      value: DataStore().soundHandler.volume,
      onChanged: (val) => setState(() {
        DataStore().soundHandler.volume = val;
      }),
      onChangeEnd: (val) => {},
      activeColor: widget.color,
      inactiveColor: Colors.black54,
    );
  }
}
