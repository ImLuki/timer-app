import 'dart:math';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:timer_app/model/routines/util/util.dart';

class TotalTimeRemainingWidget extends StatefulWidget {
  final double fontSize;
  final ValueNotifier<int> totalTimeRemaining;

  const TotalTimeRemainingWidget({super.key, required this.fontSize, required this.totalTimeRemaining});

  @override
  State<TotalTimeRemainingWidget> createState() => _TotalTimeRemainingWidgetState();
}

class _TotalTimeRemainingWidgetState extends State<TotalTimeRemainingWidget> {

  late final Function() valueListener;

  @override
  void initState() {
    super.initState();
    valueListener =  () => setState(() {});
    widget.totalTimeRemaining.addListener(valueListener);
  }

  @override
  void dispose() {
    widget.totalTimeRemaining.removeListener(valueListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Flexible(
      flex: 2,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          AutoSizeText(
            Util.getTimeString(max(0, widget.totalTimeRemaining.value)),
            style: TextStyle(height: 1.0, fontSize: widget.fontSize),
            maxFontSize: widget.fontSize,
            minFontSize: 14,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            stepGranularity: 1,
          ),
          Text(AppLocalizations.of(context)!.total_time),
        ],
      ),
    );
  }


}
