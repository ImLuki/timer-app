import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:timer_app/model/data_store.dart';
import 'package:timer_app/view/timer_screen/total_time_remaining_widget.dart';

// ignore: must_be_immutable
class SetsAndReps extends StatelessWidget {
  SetsAndReps({
    super.key,
    required this.repetitions,
    required this.sets,
    required this.totalTimeRemaining,
    required this.isPreparation,
    this.totalRepetitions = 1,
    this.totalSets = 1,
  });

  static const double MARGIN = 18.0;
  static const double LINE_CONTAINER_HEIGHT = 26.0;
  static const double LINE_CONTAINER_WIDTH = 2.0;
  static const double CONTAINER_WIDTH = 80;
  static const double FONT_SIZE = 34.0;

  final ValueNotifier<int> totalTimeRemaining;

  int repetitions;
  int totalRepetitions;
  int sets;
  int totalSets;
  bool isPreparation;

  @override
  Widget build(BuildContext context) {
    List<Widget> elements = [];

    if (isPreparation && DataStore().settingModel.showLeftTime) return _buildElements([_timeWidget(context)]);

    if (totalRepetitions > 1) elements.add(_reps(context));
    if (DataStore().settingModel.showLeftTime) elements.add(_timeWidget(context));
    if (totalSets > 1 || totalRepetitions > 1) elements.add(_sets(context));

    return _buildElements(elements);
  }

  Widget _buildElements(List<Widget> elements) {
    if (elements.isEmpty) return Container();

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: List<Widget>.generate(elements.length * 2 - 1, (index) {
        if (index.isEven) {
          return elements[index ~/ 2];
        } else {
          return _dividerWidget();
        }
      }),
    );
  }

  Widget _timeWidget(BuildContext context) {
    return TotalTimeRemainingWidget(fontSize: FONT_SIZE, totalTimeRemaining: totalTimeRemaining);
  }

  Widget _sets(BuildContext context) {
    return Expanded(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: totalRepetitions > 1 || DataStore().settingModel.showLeftTime
            ? CrossAxisAlignment.start
            : CrossAxisAlignment.center,
        children: [
          Text(
            "$sets/$totalSets",
            style: const TextStyle(height: 1.0, fontSize: FONT_SIZE),
            maxLines: 1,
          ),
          Text(AppLocalizations.of(context)!.set(sets))
        ],
      ),
    );
  }

  Widget _reps(BuildContext context) {
    return Expanded(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Text(
            "$repetitions/$totalRepetitions",
            style: const TextStyle(fontSize: FONT_SIZE, height: 1.0),
            maxLines: 1,
          ),
          Text(AppLocalizations.of(context)!.repetition_short(repetitions)),
        ],
      ),
    );
  }

  Widget _dividerWidget() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: MARGIN),
      height: LINE_CONTAINER_HEIGHT,
      width: LINE_CONTAINER_WIDTH,
      color: Colors.white,
    );
  }
}
