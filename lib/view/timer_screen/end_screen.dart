import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:timer_app/resources/constants.dart';
import 'package:timer_app/theme/app_colors.dart';

class EndScreen extends StatefulWidget {
  const EndScreen({super.key, required this.trainingTime});

  final int trainingTime;

  @override
  _EndScreenState createState() => _EndScreenState();
}

class _EndScreenState extends State<EndScreen> {
  static const double TIME_FONT_SIZE = 42.0;
  static const double TOTAL_TIME_FONT_SIZE = 18.0;
  static const double FONT_SIZE = 72.0;
  static const double TIME_SPACING = 8.0;
  static const double COLON_SHIFT = 6.0;
  static const double COLON_FONT_SIZE = 32.0;
  static const double STANDARD_PADDING = 20.0;
  static const double TOP_PADDING = 20.0;
  static const double BUTTON_HEIGHT = 90.0;
  static const double BUTTON_WIDTH = 140.0;
  static const double BUTTON_BORDER_RADIUS = 28.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(STANDARD_PADDING).copyWith(top: TOP_PADDING),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _infoText(),
                    _totalTime(),
                  ],
                ),
              ),
              _button(),
              const SizedBox(height: STANDARD_PADDING * 2),
            ],
          ),
        ),
      ),
    );
  }

  Widget _infoText() {
    return Center(
      child: Text(
        AppLocalizations.of(context)!.finished,
        style: const TextStyle(fontSize: FONT_SIZE),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _totalTime() {
    int completeTime = widget.trainingTime;

    int hour = completeTime ~/ (Constants.SECONDS_PER_MINUTE * Constants.SECONDS_PER_MINUTE);
    int min =
        completeTime % (Constants.SECONDS_PER_MINUTE * Constants.SECONDS_PER_MINUTE) ~/ Constants.SECONDS_PER_MINUTE;
    int seconds = completeTime % Constants.SECONDS_PER_MINUTE;

    return Column(
      children: [
        Text(
          AppLocalizations.of(context)!.timeInTotal.toUpperCase(),
          style: const TextStyle(
            fontSize: TOTAL_TIME_FONT_SIZE,
            color: Colors.white54,
            wordSpacing: 5,
            letterSpacing: 3,
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _getTimeNumber(hour, "h"),
            const SizedBox(width: TIME_SPACING),
            _getColonWidget(),
            const SizedBox(width: TIME_SPACING),
            _getTimeNumber(min, "m"),
            const SizedBox(width: TIME_SPACING),
            _getColonWidget(),
            const SizedBox(width: TIME_SPACING),
            _getTimeNumber(seconds, "s"),
          ],
        )
      ],
    );
  }

  Widget _button() {
    return Center(
      child: MaterialButton(
        onPressed: finish,
        height: BUTTON_HEIGHT,
        minWidth: BUTTON_WIDTH,
        color: Theme.of(context).colorScheme.secondary,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(BUTTON_BORDER_RADIUS),
        ),
        child: Text(AppLocalizations.of(context)!.back),
      ),
    );
  }

  void finish() {
    Navigator.pop(context);
  }

  Widget _getTimeNumber(int time, String shortcut) {
    return Column(
      children: [
        Text(
          time.toString().padLeft(2, "0"),
          style: const TextStyle(color: AppColors.secondary, fontSize: TIME_FONT_SIZE),
        ),
        Text(
          shortcut,
          style: const TextStyle(color: Colors.white54),
        )
      ],
    );
  }

  Widget _getColonWidget() {
    return const Column(
      children: [
        SizedBox(height: COLON_SHIFT),
        Text(
          ":",
          style: TextStyle(color: Colors.white, fontSize: COLON_FONT_SIZE),
        ),
      ],
    );
  }
}
