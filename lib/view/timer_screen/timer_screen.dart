import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_foreground_task/flutter_foreground_task.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:timer_app/model/data_store.dart';
import 'package:timer_app/model/routines/parent_routines/starter_routine.dart';
import 'package:timer_app/view/timer_screen/countdown_timer.dart';
import 'package:timer_app/view/timer_screen/end_screen.dart';
import 'package:timer_app/view/timer_screen/sets_and_reps.dart';
import 'package:timer_app/view/timer_screen/volume_slider.dart';
import 'package:timer_app/view/util/custom_on_will_pop_scope.dart';
import 'package:timer_app/view/util/custom_page_route.dart';
import 'package:timer_app/view_model/task_handler.dart';
import 'package:timer_app/view_model/timer_view_model.dart';

import 'circular_countdown_timer.dart';

class TimerScreen extends StatefulWidget {
  const TimerScreen({
    super.key,
    required this.routine,
  });

  final StarterRoutine routine;

  @override
  _TimerScreenState createState() => _TimerScreenState();
}

class _TimerScreenState extends State<TimerScreen> with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  static const int DOUBLE_TAP_DURATION = 3;

  DateTime? currentBackPressTime;
  late CountDownController _countDownController;
  late AnimationController _animationController;
  late TimerViewModel _timerViewModel;

  @override
  void initState() {
    super.initState();
    _initService();
    WidgetsBinding.instance.addObserver(this);
    _countDownController = CountDownController();
    _animationController = AnimationController(duration: const Duration(milliseconds: 450), vsync: this);
    _timerViewModel = TimerViewModel(widget.routine, onNewRoutine: _next, onFinished: () => _finish(context));

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _timerViewModel.start(context);
      _startService();
    });
    FlutterForegroundTask.addTaskDataCallback(_onReceiveTaskData);
  }

  void _initService() {
    FlutterForegroundTask.init(
      androidNotificationOptions: AndroidNotificationOptions(
        channelId: 'foreground_service',
        channelName: 'Foreground Service Notification',
        channelDescription: 'This notification appears when the foreground service is running.',
        channelImportance: NotificationChannelImportance.LOW,
        priority: NotificationPriority.LOW,
        playSound: false,
        enableVibration: false,

      ),
      iosNotificationOptions: const IOSNotificationOptions(
        showNotification: true,
        playSound: false,
      ),
      foregroundTaskOptions: ForegroundTaskOptions(
        eventAction: ForegroundTaskEventAction.repeat(500),
        autoRunOnBoot: false,
        autoRunOnMyPackageReplaced: true,
        allowWakeLock: true,
        allowWifiLock: true,
      ),
    );
  }

  Future<ServiceRequestResult> _startService() async {
    String title = _timerViewModel.getNotificationTitle(context);
    String contentText = _timerViewModel.getNotificationBody(context);
    if (await FlutterForegroundTask.isRunningService) {
      return FlutterForegroundTask.restartService();
    } else {
      return FlutterForegroundTask.startService(
        serviceId: 256,
        notificationTitle: title,
        notificationText: contentText,
        notificationIcon: const NotificationIconData(
          name: 'notification_icon',
          resPrefix: ResourcePrefix.ic,
          resType: ResourceType.mipmap,
        ),
        callback: startCallback,
      );
    }
  }

  Future<ServiceRequestResult> _stopService() async {
    return FlutterForegroundTask.stopService();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    FlutterForegroundTask.removeTaskDataCallback(_onReceiveTaskData);
    _stopService();
    super.dispose();
  }

  void _onReceiveTaskData(Object data) {
    if (data is Map<String, dynamic>) {
      final dynamic timestampMillis = data["timestampMillis"];
      if (timestampMillis != null) {
        final DateTime timestamp = DateTime.fromMillisecondsSinceEpoch(timestampMillis, isUtc: true);
        debugPrint('timestamp: ${timestamp.toString()}');
      }
    }
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    super.didChangeAppLifecycleState(state);
    switch (state) {
      case AppLifecycleState.resumed:
        await _timerViewModel.restoreState();
        if (_timerViewModel.isRunning()) {
          setState(() {
            _countDownController.startFromTime(
              initialDuration: _timerViewModel.getCurrentRoutineDuration() - _timerViewModel.timeRemaining,
              duration: _timerViewModel.getCurrentRoutineDuration(),
            );
          });
        }
        break;
      case AppLifecycleState.inactive:
        _countDownController.pause();
        _timerViewModel.moveToBackgroundTask(context);
        break;
      case AppLifecycleState.paused:
        break;
      case AppLifecycleState.detached:
        break;
      case AppLifecycleState.hidden:
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return WithForegroundTask(
      child: Scaffold(
        body: CustomWillPopScope(
          onWillPop: _onWillPop,
          child: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(20.0).copyWith(top: 6.0),
              child: _content(),
            ),
          ),
        ),
      ),
    );
  }

  Widget _content() => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          VolumeSlider(color: _color(context: context)),
          const SizedBox(height: 20.0),
          SizedBox(
            height: 76,
            child: SetsAndReps(
              repetitions: _timerViewModel.repetition,
              sets: _timerViewModel.set,
              totalRepetitions: _timerViewModel.repetitionRoutine.repetitions,
              totalSets: _timerViewModel.setRoutine.sets,
              totalTimeRemaining: _timerViewModel.totalTimeRemaining,
              isPreparation: _timerViewModel.isPreparation(),
            ),
          ),
          const SizedBox(height: 20.0),
          Expanded(
            flex: 2,
            child: Container(
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.width * 0.7,
              child: CountDownTimer(
                //onComplete: _next,
                duration: _timerViewModel.isPreparation()
                    ? _timerViewModel.repetitionRoutine.preparationTime
                    : _timerViewModel.currentRoutine.getTimeInSeconds(lastSet: _timerViewModel.isLastSet()),
                countDownController: this._countDownController,
                color: _color(context: context),
              ),
            ),
          ),
          _infoText(
            text: _timerViewModel.isPreparation()
                ? AppLocalizations.of(context)!.timer_app_preparation_key
                : _timerViewModel.currentRoutine.infoText,
          ),
          const SizedBox.shrink(),
          Visibility(
            visible: !_timerViewModel.isPreparation(),
            maintainSize: true,
            maintainAnimation: true,
            maintainState: true,
            maintainSemantics: true,
            child: _buttons(),
          ),
          const SizedBox(height: 6.0),
        ],
      );

  Widget _infoText({required String text}) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(top: 30.0, bottom: 20.0),
        child: Container(
          alignment: Alignment.center,
          child: AutoSizeText(
            text,
            style: const TextStyle(fontSize: 42),
            textAlign: TextAlign.center,
            minFontSize: 18,
            maxLines: 5,
            overflow: TextOverflow.ellipsis,
          ),
        ),
      ),
    );
  }

  Widget _buttons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _sideButtons(icon: const Icon(Icons.skip_previous), onPressed: _prev),
        const SizedBox(width: 40.0),
        Flexible(
          child: MaterialButton(
            onPressed: _togglePlayPause,
            height: 90.0,
            minWidth: 140.0,
            color: _color(context: context),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(28.0),
            ),
            child: AnimatedIcon(
              icon: AnimatedIcons.pause_play,
              progress: _animationController,
            ),
          ),
        ),
        const SizedBox(width: 40.0),
        _sideButtons(
          icon: const Icon(Icons.skip_next),
          onPressed: () => _timerViewModel.jumpToNext(context),
        ),
      ],
    );
  }

  Widget _sideButtons({required Icon icon, void Function()? onPressed}) {
    return MaterialButton(
      onPressed: onPressed,
      height: 60.0,
      minWidth: 60.0,
      color: Colors.white30,
      shape: const CircleBorder(),
      child: icon,
    );
  }

  void _togglePlayPause() {
    setState(() {
      _timerViewModel.togglePlayPause(context, explicitTime: _countDownController.getExplicitTime());
      _timerViewModel.isRunning() ? _countDownController.resume() : _countDownController.pause();
      _timerViewModel.isRunning() ? _animationController.reverse() : _animationController.forward();
    });
  }

  void _next() {
    setState(() {
      _countDownController.restart(duration: _timerViewModel.timeRemaining);
      if (!_timerViewModel.isRunning()) _countDownController.pause();
    });
  }

  void _finish(BuildContext context) {
    Navigator.pushReplacement(
      context,
      CustomPageRoute.build(
        builder: (BuildContext context) => EndScreen(trainingTime: _timerViewModel.trainingsTime),
      ),
    );
  }

  void _prev() {
    setState(() {
      _timerViewModel.prev(context, _timerViewModel.currentRoutine.getTimeInSeconds());
      _countDownController.restart(duration: _timerViewModel.currentRoutine.getTimeInSeconds());
      if (!_timerViewModel.isRunning()) _countDownController.pause();
    });
  }

  Color _color({required BuildContext context}) {
    if (_timerViewModel.isPreparation()) return DataStore().settingModel.standardPreparationColor;
    return _timerViewModel.currentRoutine.color;
  }

  Future<bool> _onWillPop() async {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(this.currentBackPressTime!) > const Duration(seconds: DOUBLE_TAP_DURATION)) {
      currentBackPressTime = now;
      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            content: Text(AppLocalizations.of(context)!.press_again_to_leave),
            duration: const Duration(seconds: DOUBLE_TAP_DURATION),
            behavior: SnackBarBehavior.floating,
          ),
        );
      return false;
    }
    if (mounted) ScaffoldMessenger.of(context).removeCurrentSnackBar();
    _timerViewModel.cancel();
    if (mounted) Navigator.pop(context);
    return false;
  }
}
