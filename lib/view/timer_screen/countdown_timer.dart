import 'package:flutter/material.dart';

import 'circular_countdown_timer.dart';

class CountDownTimer extends StatelessWidget {
  final int duration;
  final Function()? onComplete;
  final CountDownController countDownController;
  final Color color;

  static const double SIZE_MULTIPLIER = 0.76;
  static const double FONT_SIZE = 72.0;
  static const double CIRCLE_STROKE_WIDTH = 20.0;

  const CountDownTimer({
    super.key,
    required this.duration,
    required this.countDownController,
    this.onComplete,
    required this.color,
  });

  @override
  Widget build(BuildContext context) {
    return CircularCountDownTimer(
      duration: this.duration,
      initialDuration: 0,
      controller: this.countDownController,
      width: MediaQuery.of(context).size.width * SIZE_MULTIPLIER,
      height: MediaQuery.of(context).size.width * SIZE_MULTIPLIER,
      ringColor: Colors.black54,
      fillColor: this.color,
      backgroundColor: Colors.transparent,
      strokeWidth: CIRCLE_STROKE_WIDTH,
      strokeCap: StrokeCap.round,
      textStyle: const TextStyle(
        fontSize: FONT_SIZE,
        color: Colors.white,
        fontWeight: FontWeight.normal,
      ),
      textFormat: CountdownTextFormat.DYNAMIC,
      isReverse: true,
      isReverseAnimation: false,
      isTimerTextShown: true,
      autoStart: true,
      onStart: () {},
      onComplete: this.onComplete,
    );
  }
}
