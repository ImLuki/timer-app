import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:timer_app/model/routines/parent_routines/advanced_routine.dart';
import 'package:timer_app/model/routines/parent_routines/simple_routine.dart';
import 'package:timer_app/model/routines/parent_routines/starter_routine.dart';
import 'package:timer_app/resources/constants.dart';
import 'package:timer_app/theme/app_colors.dart';
import 'package:timer_app/view/dialogs/confirm_dialog.dart';
import 'package:timer_app/view/editor/editor_screen.dart';
import 'package:timer_app/view/timer_screen/timer_screen.dart';
import 'package:timer_app/view/util/custom_page_route.dart';
import 'package:timer_app/view_model/edit_view_model.dart';
import 'package:timer_app/view_model/home_view_model.dart';

class RoutineView extends StatefulWidget {
  const RoutineView({super.key, required this.homeViewModel});

  final HomeViewModel homeViewModel;

  @override
  _RoutineViewState createState() => _RoutineViewState();
}

class _RoutineViewState extends State<RoutineView> {
  static const double mediumPadding = 16.0;
  static const double smallPadding = 10.0;
  static const double timeLinePadding = 6.0;
  static const double playArrowSize = 75.0;
  static const double routineContainerHeight = 212.0;
  static const double routineTitleSize = 18.0;
  static const double floatingSearchBarHeight = 48.0;
  static const double actionIconPadding = 0.0;
  static const double actionIconSplashRadius = 28.0;
  static const double actionIconSize = 20.0;
  static const int snackBarDuration = 1500;

  late Function() _listener;

  @override
  void initState() {
    super.initState();
    widget.homeViewModel.textController.text = "";
    _listener = () => setState(() {});
    widget.homeViewModel.addListener(this._listener);
  }

  @override
  void dispose() {
    super.dispose();
    widget.homeViewModel.removeListener(this._listener);
  }

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      controller: widget.homeViewModel.scrollController,
      child: ReorderableListView.builder(
        buildDefaultDragHandles: widget.homeViewModel.textController.text == "",
        scrollController: widget.homeViewModel.scrollController,
        padding: const EdgeInsets.only(
          left: mediumPadding,
          right: mediumPadding,
          top: Constants.STANDARD_PADDING / 2 + floatingSearchBarHeight,
          bottom: Constants.STANDARD_PADDING * 3,
        ),
        itemCount: widget.homeViewModel.filteredRoutines.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: Constants.STANDARD_PADDING / 2),
            key: Key('$index'),
            child: _getRoutineContainer(
              index: index,
              context: context,
              routine: widget.homeViewModel.filteredRoutines[index],
            ),
          );
        },
        onReorder: (int oldIndex, int newIndex) {
          widget.homeViewModel.onReorder(oldIndex, newIndex);
        },
      ),
    );
  }

  Widget _getRoutineContainer({required BuildContext context, required StarterRoutine routine, required int index}) {
    return SizedBox(
      height: routineContainerHeight,
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: AppColors.secondary),
          borderRadius: const BorderRadius.all(
            Radius.circular(Constants.STANDARD_BORDER_RADIUS),
          ),
        ),
        child: Row(
          children: [
            _getStartButton(routine: routine),
            _getTrainingOverview(routine: routine, index: index),
          ],
        ),
      ),
    );
  }

  Widget _getStartButton({required StarterRoutine routine}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        InkWell(
          customBorder: const CircleBorder(),
          child: const Icon(Icons.play_arrow, size: playArrowSize),
          onTap: () {
            if (routine.repetitionRoutine.children.isEmpty) {
              ScaffoldMessenger.of(context)
                ..removeCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    content: Text(AppLocalizations.of(context)!.routine_without_children_warning),
                    duration: const Duration(milliseconds: snackBarDuration),
                    behavior: SnackBarBehavior.floating,
                  ),
                );
              return;
            }
            Navigator.push(
              context,
              CustomPageRoute.build(
                builder: (BuildContext context) => TimerScreen(routine: routine),
              ),
            );
          },
        ),
        Text(
          AppLocalizations.of(context)!.start.toUpperCase(),
        ),
      ],
    );
  }

  Widget _getTrainingOverview({required StarterRoutine routine, required int index}) {
    routine.name;
    if (["", "timer_app_training_one_key", "timer_app_training_hiit_key"].contains(routine.name)) {
      routine.name = "${AppLocalizations.of(context)!.saved} "
          "${DateFormat(Constants.DATE_FORMAT).format(routine.dateTime)}";
    }

    return Flexible(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 12.0),
          Padding(
            padding: const EdgeInsets.only(left: smallPadding, right: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: Tooltip(
                    message: routine.name.replaceAll("", "\u{200B}"),
                    child: Text(
                      routine.name,
                      style: const TextStyle(fontWeight: FontWeight.bold, fontSize: routineTitleSize),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    ),
                  ),
                ),
                if (!widget.homeViewModel.isSearching())
                  ReorderableDragStartListener(
                    index: index,
                    child: const Icon(Icons.drag_handle),
                  ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20.0),
            child: Row(
              children: [
                Text(
                  "▶ ${AppLocalizations.of(context)!.time}:",
                  style: TextStyle(color: Theme.of(context).colorScheme.secondary),
                ),
                const SizedBox(width: 4.0),
                Text(routine.getTotalTimeString()),
              ],
            ),
          ),
          const SizedBox(height: 12.0),
          Flexible(child: _getTimeLine(routine)),
          _actionIcons(routine: routine),
        ],
      ),
    );
  }

  Widget _actionIcons({required StarterRoutine routine}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        IconButton(
          onPressed: () => _edit(routine: routine),
          icon: const Icon(Icons.edit),
          padding: const EdgeInsets.all(actionIconPadding),
          splashRadius: actionIconSplashRadius,
          iconSize: actionIconSize,
        ),
        IconButton(
          onPressed: () => _duplicate(context, routine: routine),
          icon: const Icon(Icons.copy),
          padding: const EdgeInsets.all(actionIconPadding),
          splashRadius: actionIconSplashRadius,
          iconSize: actionIconSize,
        ),
        IconButton(
          onPressed: () => delete(routine: routine),
          icon: const Icon(Icons.delete),
          padding: const EdgeInsets.all(actionIconPadding),
          splashRadius: actionIconSplashRadius,
          iconSize: actionIconSize,
        ),
        IconButton(
          onPressed: () => widget.homeViewModel.shareFile(routine),
          icon: const Icon(Icons.share),
          padding: const EdgeInsets.all(actionIconPadding),
          splashRadius: actionIconSplashRadius,
          iconSize: actionIconSize,
        ),
        const SizedBox(width: 8.0),
      ],
    );
  }

  Widget _getTimeLine(StarterRoutine routine) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Padding(
        padding: const EdgeInsets.only(left: smallPadding, right: Constants.STANDARD_PADDING),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            routine.getOverviewWidget(),
            const SizedBox(height: timeLinePadding),
            Flexible(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: routine.getOverviewText(context),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _edit({required StarterRoutine routine}) {
    Navigator.push(
      context,
      CustomPageRoute.build(
        builder: (BuildContext context) => EditorScreen(
          editViewModel: EditViewModel(
            advanced: routine is AdvancedRoutine,
            simpleRoutine: routine is SimpleRoutine ? routine : null,
            advancedRoutine: routine is AdvancedRoutine ? routine : null,
          ),
        ),
      ),
    );
  }

  void _duplicate(BuildContext context, {required StarterRoutine routine}) {
    dynamic newRoutine = widget.homeViewModel.duplicate(routine: routine);

    Navigator.push(
      context,
      CustomPageRoute.build(
        builder: (BuildContext context) => EditorScreen(
          editViewModel: EditViewModel(
            advanced: routine is AdvancedRoutine,
            simpleRoutine: routine is SimpleRoutine ? newRoutine : null,
            advancedRoutine: routine is AdvancedRoutine ? newRoutine : null,
          ),
        ),
      ),
    ).then((value) {
      if (value != null && value) widget.homeViewModel.scrollToTop();
    });
  }

  void delete({required StarterRoutine routine}) async {
    bool? result = await ConfirmDialog().deleteConfirm(context, name: routine.name);

    if (mounted) ScaffoldMessenger.of(context).removeCurrentSnackBar();

    if (result != null && result) {
      widget.homeViewModel.deleteRoutine(routine: routine);
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(AppLocalizations.of(context)!.deleted_entry),
            duration: const Duration(milliseconds: snackBarDuration),
            behavior: SnackBarBehavior.floating,
          ),
        );
      }
    } else {
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(AppLocalizations.of(context)!.delete_canceled),
            duration: const Duration(milliseconds: snackBarDuration),
            behavior: SnackBarBehavior.floating,
          ),
        );
      }
    }
  }
}
