import 'package:flex_color_picker/flex_color_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CustomColorPicker extends StatefulWidget {
  const CustomColorPicker({
    super.key,
    required this.currentColor,
    required this.onColorChanged,
    this.colors = const [],
  });

  final Color currentColor;
  final ValueChanged<Color> onColorChanged;
  final List<Color> colors;

  @override
  State<CustomColorPicker> createState() => _ColorPickerState(currentColor);
}

class _ColorPickerState extends State<CustomColorPicker> {
  Color selectedColor;

  _ColorPickerState(this.selectedColor);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: Column(
          children: [
            const SizedBox(height: 6),
            _topRow(),
            const Divider(thickness: 1),
            _colorPicker(),
          ],
        ),
      ),
    );
  }

  Widget _topRow() => Padding(
        padding: const EdgeInsets.only(left: 10.0, right: 20.0),
        child: Row(
          children: [
            IconButton(
              onPressed: () => Navigator.pop(context, false),
              icon: const Icon(Icons.close),
            ),
            Text(
              AppLocalizations.of(context)!.widget_color_picker_edit,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
            ),
            const Spacer(),
            TextButton(
              style: TextButton.styleFrom(
                foregroundColor: Colors.blue,
              ),
              onPressed: _onSave,
              child: Text(
                AppLocalizations.of(context)!.widget_color_picker_save,
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
              ),
            ),
          ],
        ),
      );

  Widget _colorPicker() => ColorPicker(
        color: selectedColor,
        showColorName: false,
        onColorChanged: _onColorChanged,
        pickersEnabled: const <ColorPickerType, bool>{
          ColorPickerType.both: true,
          ColorPickerType.primary: false,
          ColorPickerType.accent: false,
          ColorPickerType.bw: false,
          ColorPickerType.custom: false,
          ColorPickerType.wheel: true,
        },
        enableShadesSelection: false,
        recentColors: widget.colors,
        showRecentColors: true,
      );

  void _onColorChanged(Color color) {
    selectedColor = color;
    setState(() {});
  }

  void _onSave() {
    widget.onColorChanged(selectedColor);
    Navigator.pop(context, true);
  }
}
