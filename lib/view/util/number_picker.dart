import 'dart:math';

import 'package:flutter/material.dart';
import 'package:timer_app/resources/constants.dart';
import 'package:timer_app/view/editor/number_text_field.dart';
import 'package:timer_app/view/editor/plus_minus_button.dart';

class NumberPicker extends StatefulWidget {
  const NumberPicker({
    super.key,
    required this.initValue,
    required this.onChange,
    required this.padding,
    required this.numberTextFieldWidth,
    this.fontSize = 28,
    this.iconBoxSize = PlusMinusButton.ICON_BOX_SIZE,
    this.iconSize = PlusMinusButton.ICON_SIZE,
    this.maxValue = 99,
    this.minValue = 1,
    this.tailing = const SizedBox(),
    this.regex = Constants.REGEX_REPS,
  });

  final int initValue;
  final Function(int) onChange;
  final double padding;
  final double numberTextFieldWidth;
  final double iconBoxSize;
  final double iconSize;
  final double fontSize;

  final int maxValue;
  final int minValue;
  final Widget tailing;
  final String regex;

  @override
  _NumberPickerState createState() => _NumberPickerState(currentValue: this.initValue);
}

class _NumberPickerState extends State<NumberPicker> {
  _NumberPickerState({required this.currentValue});

  final TextEditingController _textEditingController = TextEditingController();

  int currentValue;

  @override
  void initState() {
    super.initState();
    this._textEditingController.text = this.currentValue.toString();
  }

  @override
  void dispose() {
    this._textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //update values
    this._textEditingController.text = this.currentValue.toString();
    this._textEditingController.selection = TextSelection.fromPosition(
      TextPosition(offset: _textEditingController.text.length),
    );

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        PlusMinusButton(
          iconBoxSize: widget.iconBoxSize,
          icon: Icons.remove,
          iconSize: widget.iconSize,
          changeValue: -1,
          onChangeValue: onTapPlusMinusButton,
        ),
        SizedBox(width: widget.padding),
        NumberTextField(
          controller: this._textEditingController,
          width: widget.numberTextFieldWidth,
          regex: widget.regex,
          fontSize: widget.fontSize,
          alignment: TextAlign.center,
          onChangePreCalc: (val) => {
            if (val != "") this.onChangedValue(int.parse(val)),
          },
        ),
        Container(width: widget.padding),
        PlusMinusButton(
          iconBoxSize: widget.iconBoxSize,
          icon: Icons.add,
          iconSize: widget.iconSize,
          changeValue: 1,
          onChangeValue: onTapPlusMinusButton,
        ),
        widget.tailing,
      ],
    );
  }

  void onTapPlusMinusButton(int changeValue) {
    this.onChangedValue(this.currentValue + changeValue);
  }

  void onChangedValue(int newVal) {
    setState(() {
      this.currentValue = min(max(widget.minValue, newVal), widget.maxValue);
      this._textEditingController.text = this.currentValue.toString();
      this._textEditingController.selection = TextSelection.fromPosition(
        TextPosition(offset: _textEditingController.text.length),
      );
    });
    widget.onChange(this.currentValue);
  }
}
