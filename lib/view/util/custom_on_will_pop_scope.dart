import 'dart:io';

import 'package:flutter/material.dart';

class CustomWillPopScope extends StatelessWidget {
  const CustomWillPopScope({super.key, required this.child, this.onWillPop});

  final Widget child;
  final Future<bool> Function()? onWillPop;

  @override
  Widget build(BuildContext context) {
    return Platform.isIOS
        ? GestureDetector(
            onPanUpdate: (details) async {
              if (details.delta.dx > 0 && onWillPop != null) {
                if (await onWillPop!()) {
                  if (context.mounted) Navigator.pop(context);
                }
              }
            },
            child: WillPopScope(
              onWillPop: () async {
                return false;
              },
              child: child,
            ),
          )
        : WillPopScope(
            onWillPop: onWillPop,
            child: child,
          );
  }
}
