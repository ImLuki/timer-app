import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:timer_app/resources/constants.dart';
import 'package:timer_app/view/editor/number_text_field.dart';
import 'package:timer_app/view/editor/plus_minus_button.dart';

class TimePicker extends StatefulWidget {
  const TimePicker({
    super.key,
    required this.initValue,
    required this.onChange,
    required this.padding,
    required this.numberTextFieldWidth,
    this.fontSize = 28,
    this.iconBoxSize = PlusMinusButton.ICON_BOX_SIZE,
    this.iconSize = PlusMinusButton.ICON_SIZE,
    this.timeMaxValue = Constants.TIME_MAX_VALUE,
    this.timeMinValue = Constants.TIME_MIN_VALUE,
    this.tailing = const SizedBox(),
  });

  final int initValue;
  final Function(int) onChange;
  final double padding;
  final double numberTextFieldWidth;
  final double iconBoxSize;
  final double iconSize;
  final double fontSize;

  final int timeMaxValue;
  final int timeMinValue;
  final Widget tailing;

  @override
  _TimePickerState createState() => _TimePickerState(currentValue: this.initValue);
}

class _TimePickerState extends State<TimePicker> {
  _TimePickerState({required this.currentValue});

  final TextEditingController _minutesEditingController = TextEditingController();
  final TextEditingController _secondsEditingController = TextEditingController();

  int currentValue;
  late StreamSubscription<bool> _keyboardSubscription;

  @override
  void initState() {
    super.initState();

    int trainingMinutes = this.currentValue ~/ Constants.SECONDS_PER_MINUTE;
    int trainingSeconds = this.currentValue % Constants.SECONDS_PER_MINUTE;
    _minutesEditingController.text = trainingMinutes.toString().padLeft(2, "0");
    _secondsEditingController.text = trainingSeconds.toString().padLeft(2, "0");

    KeyboardVisibilityController keyboardVisibilityController = KeyboardVisibilityController();
    _keyboardSubscription = keyboardVisibilityController.onChange.listen((bool visible) {
      if (!visible) this.onChangedValue(this.currentValue, true);
    });
  }

  @override
  void dispose() {
    this._minutesEditingController.dispose();
    this._secondsEditingController.dispose();

    _keyboardSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        PlusMinusButton(
          iconBoxSize: widget.iconBoxSize,
          iconSize: widget.iconSize,
          icon: Icons.remove,
          changeValue: -1,
          onChangeValue: onTapPlusMinusButton,
        ),
        SizedBox(width: widget.padding),
        NumberTextField(
          controller: this._minutesEditingController,
          width: widget.numberTextFieldWidth,
          regex: Constants.REGEX_ONE,
          fontSize: widget.fontSize,
          alignment: TextAlign.right,
          onChangePreCalc: (val) => {
            if (val != "")
              this.currentValue = int.parse(val) * Constants.SECONDS_PER_MINUTE +
                  int.parse(this._secondsEditingController.text == "" ? "0" : this._secondsEditingController.text),
          },
        ),
        Text(
          Constants.COLON_SIGN,
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: widget.fontSize,
          ),
        ),
        NumberTextField(
          controller: this._secondsEditingController,
          width: widget.numberTextFieldWidth,
          regex: Constants.REGEX_TWO,
          fontSize: widget.fontSize,
          alignment: TextAlign.left,
          onChangePreCalc: (val) => {
            if (val != "")
              this.currentValue = int.parse(val) +
                  int.parse(this._minutesEditingController.text == "" ? "0" : this._minutesEditingController.text) *
                      Constants.SECONDS_PER_MINUTE,
          },
        ),
        SizedBox(width: widget.padding),
        PlusMinusButton(
          iconBoxSize: widget.iconBoxSize,
          icon: Icons.add,
          iconSize: widget.iconSize,
          changeValue: 1,
          onChangeValue: onTapPlusMinusButton,
        ),
        widget.tailing,
      ],
    );
  }

  void onTapPlusMinusButton(int changeValue) {
    this.onChangedValue(this.currentValue + changeValue, true);
  }

  void onChangedValue(int newVal, bool pad) {
    setState(() {
      this.currentValue = min(max(widget.timeMinValue, newVal), widget.timeMaxValue);

      int minutes = this.currentValue ~/ Constants.SECONDS_PER_MINUTE;
      int seconds = this.currentValue % Constants.SECONDS_PER_MINUTE;

      this._minutesEditingController.text = minutes.toString();
      this._secondsEditingController.text = seconds.toString();

      if (pad) this.padValues();

      this._minutesEditingController.selection = TextSelection.fromPosition(
        TextPosition(offset: _minutesEditingController.text.length),
      );
      this._secondsEditingController.selection = TextSelection.fromPosition(
        TextPosition(offset: _secondsEditingController.text.length),
      );
    });
    widget.onChange(this.currentValue);
  }

  void padValues() {
    this._minutesEditingController.text = this._minutesEditingController.text.toString().padLeft(2, "0");
    this._secondsEditingController.text = this._secondsEditingController.text.toString().padLeft(2, "0");
  }
}
