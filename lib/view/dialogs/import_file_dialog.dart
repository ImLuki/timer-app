import 'dart:convert';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:timer_app/model/data_store.dart';
import 'package:timer_app/model/routines/util/routine_delegator.dart';
import 'package:timer_app/model/util/data_reader.dart';
import 'package:timer_app/resources/constants.dart';

//ignore_for_file: use_build_context_synchronously
class ImportFileDialog {
  Future<bool?> showLoadFileDialog(BuildContext context) async {

    return showDialog<bool?>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return SimpleDialog(
          contentPadding: const EdgeInsets.fromLTRB(24.0, 20.0, 24.0, 24.0),
          titleTextStyle: Theme.of(context).textTheme.titleMedium,
          title: Text("${AppLocalizations.of(context)!.import_backup_file}?"),
          children: [
            Text(
              AppLocalizations.of(context)!.import_backup_file_info,
              overflow: TextOverflow.clip,
            ),
            const SizedBox(height: 8.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(
                  onPressed: () => onCancel(context),
                  style: OutlinedButton.styleFrom(
                    foregroundColor: Theme.of(context).colorScheme.secondary,
                  ),
                  child: Text(AppLocalizations.of(context)!.cancel),
                ),
                TextButton(
                  onPressed: () => onConfirm(context),
                  style: OutlinedButton.styleFrom(
                    foregroundColor: Theme.of(context).colorScheme.secondary,
                  ),
                  child: Text(AppLocalizations.of(context)!.confirm),
                ),
              ],
            ),
          ],
        );
      },
    );
  }

  void onConfirm(BuildContext context) async {
    FocusScope.of(context).unfocus();

    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: [Constants.JSON_ENDING.replaceAll(".", "")],
    );
    if (result == null) {
      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            content: Text(AppLocalizations.of(context)!.could_not_import_backup),
            behavior: SnackBarBehavior.floating,
          ),
        );

      return;
    }

    _loadFile(result.files.single.path).then((value) => {
          ScaffoldMessenger.of(context)
            ..removeCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Text(
                  value
                      ? AppLocalizations.of(context)!.imported_backup
                      : AppLocalizations.of(context)!.could_not_import_backup,
                ),
                behavior: SnackBarBehavior.floating,
              ),
            ),
        });

    Navigator.pop(context);
  }

  void onCancel(BuildContext context) {
    FocusScope.of(context).unfocus();
    Navigator.pop(context);
  }

  Future<bool> _loadFile(String? value) async {
    if (value == null) {
      return false;
    }
    if (!value.endsWith(Constants.JSON_ENDING)) {
      return false;
    }

    String jsonString = await DataReader(path: value).getJsonFromFile();
    await DataStore().routineRepository.addRoutine(RoutineFactory.fromMap(jsonDecode(jsonString)));
    return true;
  }
}
