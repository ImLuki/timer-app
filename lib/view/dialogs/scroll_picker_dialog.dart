import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

abstract class ScrollPickerDialog<T> {
  final String title;
  final ValueChanged<T> onChanged;
  T currentValue;

  ScrollPickerDialog({
    required this.currentValue,
    required this.title,
    required this.onChanged,
  });

  /// shows picker dialog
  ///
  /// Navigator.pop on close
  Future<void> showScrollPickerDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: getTitle(),
          contentPadding: const EdgeInsets.all(16.0),
          children: [
            _embeddedPicker(context),
            const SizedBox(height: 14.0),
            _okButton(context),
          ],
        );
      },
    );
  }

  /// Return widget Title
  Widget getTitle();

  /// Returns body part with picker widget
  ///
  /// Needs [BuildContext] for strings
  Widget getPicker(BuildContext context);

  Widget _embeddedPicker(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.4,
      child: getPicker(context),
    );
  }

  Widget _okButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40.0),
      child: TextButton(
        style: TextButton.styleFrom(
          foregroundColor: Theme.of(context).colorScheme.secondary,
        ),
        child: Text(AppLocalizations.of(context)!.ok),
        onPressed: () => _onPressOkButton(context),
      ),
    );
  }

  void _onPressOkButton(BuildContext context) {
    onChanged(currentValue);
    Navigator.pop(context);
  }
}
