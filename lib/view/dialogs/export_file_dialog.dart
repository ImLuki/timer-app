import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:share_plus/share_plus.dart';
import 'package:sorted_list/sorted_list.dart';
import 'package:timer_app/model/data_access_object.dart';
import 'package:timer_app/model/data_store.dart';
import 'package:timer_app/model/routines/parent_routines/starter_routine.dart';
import 'package:timer_app/model/util/data_writer.dart';

//ignore_for_file: use_build_context_synchronously
class ExportFileDialog {
  final SortedList<StarterRoutine> routines = SortedList((a, b) => a.name.compareTo(b.name));
  int _selectedRadio = -1;

  Future<bool?> showExportDialog(BuildContext context) async {
    routines.addAll(DataStore().routineRepository.getAll());
    return showDialog<bool?>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
            return AlertDialog(
              contentPadding: const EdgeInsets.fromLTRB(24.0, 20.0, 24.0, 24.0),
              title: Column(
                children: [
                  Text("${AppLocalizations.of(context)!.export_training_routine}?"),
                  const SizedBox(height: 8.0),
                  Text(
                    AppLocalizations.of(context)!.export_training_routine_dialog_info,
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.normal),
                  ),
                ],
              ),
              content: _routineListWidget(context, setState),
              actions: [
                TextButton(
                  onPressed: () => onCancel(context),
                  style: OutlinedButton.styleFrom(
                    foregroundColor: Theme.of(context).colorScheme.secondary,
                  ),
                  child: Text(AppLocalizations.of(context)!.cancel),
                ),
                TextButton(
                  onPressed: _selectedRadio == -1 ? null : () => onConfirm(context),
                  style: OutlinedButton.styleFrom(
                    foregroundColor: _selectedRadio == -1 ? Colors.grey : Theme.of(context).colorScheme.secondary,
                  ),
                  child: Text(AppLocalizations.of(context)!.confirm),
                ),
              ],
            );
          },
        );
      },
    );
  }

  Widget _routineListWidget(BuildContext context, StateSetter setState) {
    return Scrollbar(
      child: SingleChildScrollView(
        child: Column(
          children: List<Widget>.generate(
            routines.length,
            (index) => RadioListTile<int>(
              value: index,
              title: Text(routines[index].name),
              groupValue: _selectedRadio,
              onChanged: (int? value) => setState(() => _selectedRadio = index),
            ),
          ),
        ),
      ),
    );
  }

  void onConfirm(BuildContext context) async {
    FocusScope.of(context).unfocus();
    if (_selectedRadio < 0) {
      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            content: Text(AppLocalizations.of(context)!.export_training_routine_warning),
            duration: const Duration(milliseconds: 1500),
            behavior: SnackBarBehavior.floating,
          ),
        );
    } else {
      await shareFile(this.routines[this._selectedRadio]);
    }
    Navigator.pop(context);
  }

  void onCancel(BuildContext context) {
    FocusScope.of(context).unfocus();
    Navigator.pop(context);
  }

  Future<void> shareFile(StarterRoutine starterRoutine) async {
    Map map = starterRoutine.toMap();
    map.remove(DataAccessObject.DB_ID_KEY);
    DataWriter dataWriter = DataWriter(
      content: jsonEncode(map),
      fileName: "${starterRoutine.name}-interval-timer-routine",
    );
    File file = await dataWriter.localFile;
    await dataWriter.write();
    await Share.shareXFiles([XFile(file.path)]);
    dataWriter.delete();
  }
}
