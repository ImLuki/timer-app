import 'package:flutter/material.dart';
import 'package:timer_app/view/dialogs/scroll_picker_dialog.dart';
import 'package:timer_app/view/util/scroll_picker.dart';

class SoundPickerDialog extends ScrollPickerDialog<String> {
  final List<String> items;
  final Function(String)? onPlay;

  SoundPickerDialog({
    required super.currentValue,
    required super.title,
    required this.items,
    required super.onChanged,
    required this.onPlay,
  });

  @override
  Widget getPicker(BuildContext context) {
    return ScrollPicker<String>(
      initialValue: currentValue,
      onChanged: (val) => this.currentValue = val,
      items: items,
    );
  }

  @override
  Widget getTitle() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(title),
        IconButton(
          icon: const Icon(Icons.volume_up),
          onPressed: () => onPlay?.call(currentValue),
        ),
      ],
    );
  }
}
