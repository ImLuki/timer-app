import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:timer_app/view/dialogs/scroll_picker_dialog.dart';
import 'package:timer_app/view/util/scroll_picker.dart';

/// Dialog to set timer
/// or deactivate / activate it
class TimerPickerDialog extends ScrollPickerDialog<int> {
  TimerPickerDialog({
    required super.currentValue,
    required super.title,
    required super.onChanged,
  });

  @override
  Widget getPicker(BuildContext context) {
    final List<int> timeStamps = _getValues();

    return ScrollPicker<int>(
      initialValue: currentValue,
      onChanged: (int val) => currentValue = val,
      items: timeStamps,
      itemMapping: _mapTimeStamps(timeStamps, context),
    );
  }

  @override
  Widget getTitle() {
    return Text(title);
  }

  List<String> _mapTimeStamps(List<int> timeStamps, BuildContext context) {
    return timeStamps.map<String>((e) {
      if (e == 0) {
        return AppLocalizations.of(context)!.widget_timer_dialog_off;
      } else {
        return "$e ${AppLocalizations.of(context)!.second(e)}";
      }
    }).toList();
  }

  List<int> _getValues() {
    final List<int> timeStamps = [];
    timeStamps.add(0);

    // values from 1 to 10 in steps of 1
    timeStamps.addAll(List.generate(14, (index) => index + 1));

    // values from 20 to 60 in steps of 5
    timeStamps.addAll(List.generate(10, (index) => index * 5 + 15));
    return timeStamps;
  }
}
