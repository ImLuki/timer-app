import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_foreground_task/flutter_foreground_task.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:timer_app/model/data_store.dart';
import 'package:timer_app/theme/app_theme.dart';
import 'package:timer_app/timer_app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  FlutterForegroundTask.initCommunicationPort();
  await DataStore().init();

  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      onGenerateTitle: (context) {
        DataStore().routineRepository.addStandardRoutinesIfFirstStart(context);
        return AppLocalizations.of(context)!.title;
      },
      theme: AppThemeDataFactory.prepareThemeData(),
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      localeListResolutionCallback: (locales, supportedLocales) {
        if (locales != null) {
          for (Locale locale in locales) {
            if (supportedLocales.any((element) => element.languageCode == locale.languageCode)) {
              return locale;
            }
          }
        }
        return const Locale('en');
      },
      home: const TimerApp(),
      initialRoute: '/',
    );
  }
}
