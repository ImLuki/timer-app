import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_foreground_task/flutter_foreground_task.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:fluttericon/font_awesome5_icons.dart';
import 'package:timer_app/view/dialogs/import_file_dialog.dart';
import 'package:timer_app/view/editor/editor_screen.dart';
import 'package:timer_app/view/routine_view/routine_view.dart';
import 'package:timer_app/view/settings/settings_screen.dart';
import 'package:timer_app/view/util/custom_page_route.dart';
import 'package:timer_app/view_model/edit_view_model.dart';
import 'package:timer_app/view_model/home_view_model.dart';

class TimerApp extends StatefulWidget {
  const TimerApp({super.key});

  @override
  _TimerAppState createState() => _TimerAppState();
}

class _TimerAppState extends State<TimerApp> {
  static const double iconSize = 32.0;
  static const double iconPadding = 10.0;

  bool hideFloatingButton = false;

  final HomeViewModel _homeViewModel = HomeViewModel();
  late StreamSubscription<bool> _keyboardSubscription;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      // Request permissions and initialize the service.
      _requestPermissions();
    });

    KeyboardVisibilityController keyboardVisibilityController = KeyboardVisibilityController();
    // Subscribe
    _keyboardSubscription = keyboardVisibilityController.onChange.listen((bool visible) {
      if (!visible && mounted) FocusScope.of(context).unfocus();
      setState(() {
        this.hideFloatingButton = visible;
      });
    });
  }

  /// Request permission for foreground service
  Future<void> _requestPermissions() async {
    // Android 13+, you need to allow notification permission to display foreground service notification.
    //
    // iOS: If you need notification, ask for permission.
    final NotificationPermission notificationPermission = await FlutterForegroundTask.checkNotificationPermission();
    if (notificationPermission != NotificationPermission.granted) {
      await FlutterForegroundTask.requestNotificationPermission();
    }

    if (Platform.isAndroid) {
      // Android 12+, there are restrictions on starting a foreground service.
      //
      // To restart the service on device reboot or unexpected problem, you need to allow below permission.
      if (!await FlutterForegroundTask.isIgnoringBatteryOptimizations) {
        // This function requires `android.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS` permission.
        await FlutterForegroundTask.requestIgnoreBatteryOptimization();
      }
    }
  }

  @override
  void dispose() {
    _keyboardSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: this.hideFloatingButton
            ? null
            : FloatingActionButton(
                heroTag: null,
                backgroundColor: Theme.of(context).colorScheme.secondary,
                onPressed: _newRoutine,
                child: const Icon(
                  Icons.add,
                  color: Colors.black,
                  size: iconSize,
                ),
              ),
        bottomNavigationBar: BottomAppBar(
          elevation: 8.0,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: iconPadding, vertical: 1.5),
                child: IconButton(
                  tooltip: AppLocalizations.of(context)!.import_backup_tool_tip,
                  icon: const Icon(
                    FontAwesome5.file_import,
                    color: Colors.white,
                    size: 24,
                  ),
                  onPressed: () => ImportFileDialog().showLoadFileDialog(context),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: iconPadding),
                child: IconButton(
                  icon: const Icon(
                    Icons.settings,
                    color: Colors.white,
                    size: 30,
                  ),
                  onPressed: this._openSettings,
                ),
              ),
            ],
          ),
        ),
        body: SafeArea(
          child: Stack(
            children: [
              RoutineView(homeViewModel: this._homeViewModel),
              _floatingSearchBar(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _floatingSearchBar() {
    return Container(
      height: 46.0,
      margin: const EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
      child: TextFormField(
        autofocus: false,
        controller: _homeViewModel.textController,
        onChanged: (_) => setState(() {
          _homeViewModel.onSearchTextChanged();
        }),
        textAlign: TextAlign.left,
        textAlignVertical: TextAlignVertical.center,
        textCapitalization: TextCapitalization.sentences,
        maxLines: 1,
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.only(left: 20.0),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(6.0),
            borderSide: BorderSide.none,
          ),
          filled: true,
          fillColor: const Color(0xFF494949),
          suffixIcon: InkWell(
            borderRadius: BorderRadius.circular(12.0),
            onTap: () => setState(
              () {
                _homeViewModel.textController.clear();
                _homeViewModel.onSearchTextChanged();
                FocusScope.of(context).unfocus();
              },
            ),
            child: Icon(
              _homeViewModel.textController.value.text == "" ? Icons.search : Icons.clear,
              color: Colors.white,
            ),
          ),
          hintStyle: Theme.of(context).textTheme.bodyMedium,
          hintText: '${AppLocalizations.of(context)!.search}...',
        ),
      ),
    );
  }

  void _openSettings() {
    Navigator.push(
      context,
      CustomPageRoute.build(
        builder: (BuildContext context) => const SettingsPage(),
      ),
    ).then((value) {
      setState(() {});
      if (mounted) FocusScope.of(context).unfocus();
    });
  }

  void _newRoutine() {
    Navigator.push(
      context,
      CustomPageRoute.build(
        builder: (BuildContext context) => EditorScreen(editViewModel: EditViewModel(advanced: false)),
      ),
    ).then((value) {
      if (value != null && value) _homeViewModel.scrollToTop();
      if (mounted) FocusScope.of(context).unfocus();
    });
  }
}
